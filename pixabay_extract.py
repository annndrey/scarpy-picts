#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pyvirtualdisplay import Display
from selenium import webdriver

import time
import json
import os
import sys

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from models import Image, Tag
from connect import session

from sqlalchemy.exc import IntegrityError


URL = "https://pixabay.com/en/photos/?pagi={}"
PROXY = os.environ.get('PROXYCONF')

def setup_profile():
    display = Display(visible=0, size=(1600, 900))
    display.start()
    options = Options()
    options.set_headless(headless=True)

    # add proxy conf here
    #webdriver.DesiredCapabilities.FIREFOX['proxy']={
    #    "httpProxy":PROXY,
    #    "ftpProxy":PROXY,
    #    "sslProxy":PROXY,
    #    "proxyType":"MANUAL"
    #}

    browser = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver', 
                                firefox_binary='../../../firefox/firefox-bin'
    )

    
    return browser

def get_images_urls(startpage=1):
    
    browser = setup_profile()
    browser.implicitly_wait(3)

    for page in range(startpage, startpage+500):
        
        browser.get(URL.format(page))
        #all_cookies = browser.get_cookies()
        ## print(all_cookies)
        ## browser.execute_script("window.scrollTo(0, 1080)")
        ## browser.implicitly_wait(2)
        print('parsing page {}'.format(page))
        media = browser.find_element_by_xpath('//div[@class="media_list"]')
        for d in media.find_elements_by_xpath('//div[@class="item"]'):
            l =  d.find_elements_by_tag_name('a')[0]
            # print(l.get_attribute("href"))
            # print(l.get_attribute('innerHTML'))
            for img in l.find_elements_by_tag_name('img'):
                imgtags = []
                tags = img.get_attribute("alt").split(", ")
                # print('TAGS', tags)
                for tag in tags:
                    tagtxt = tag.strip().lower()
                    existing = session.query(Tag).filter(Tag.txt == tagtxt).first()
                    if not existing:
                        newtag = Tag(txt=tag.strip().lower())
                        session.add(newtag)
                        session.commit()
                        imgtags.append(newtag)
                        
                    else:
                        imgtags.append(existing)
                        
                imgsrc = img.get_attribute("src")
                if imgsrc.endswith('blank.gif'):
                    # not loaded
                    attr = 'data-lazy'
                else:
                    # loaded
                    attr = 'src'

                # print('ATTR', attr)
                src = img.get_attribute(attr).replace('__340.jpg', '_960_720.jpg')
                # print('SRC', src)
                existing = session.query(Image).filter(Image.url == src).first()
                if not existing:
                    newimage = Image(url=src, source='pixabay')
                    session.add(newimage)
                    session.commit()
                    for tg in imgtags:
                        #if tg not in newimage.tags:
                        newimage.tags.append(tg)
                        
                        session.add(newimage)
                        try:
                            session.commit()
                        except IntegrityError:
                            session.rollback()
        print("going to the next page")
        #browser.close()
        # save urls to DB
        # save tags
        # go to the next page
        # keep page number to continue from
    

if __name__ == '__main__':
    page = int(sys.argv[1])
    get_images_urls(startpage=page)
