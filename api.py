#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, g, make_response, request, current_app, Response, send_file, copy_current_request_context
from flask_migrate import Migrate
from flask import abort as fabort
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, Api, reqparse, abort
from flask.json import jsonify
from flasgger import Swagger, LazyString, LazyJSONEncoder
from flask_marshmallow import Marshmallow
from flask_httpauth import HTTPBasicAuth
from flask_cors import CORS, cross_origin
from flask_restful.utils import cors
from functools import wraps
from sqlalchemy.pool import NullPool
from werkzeug.datastructures import FileStorage

from werkzeug.routing import RequestRedirect
from werkzeug.wsgi import FileWrapper

from marshmallow_enum import EnumField
from sqlalchemy import or_, and_, func, cast, String, event, exc
from sqlalchemy.exc import IntegrityError
from models import db, Image, ImageVersion, Tag, OrientationType, Location, User, AuthorBlacklist, SourceBlacklist, Article, Counter, img_tags
from urllib.parse import urlparse, parse_qs, urlencode, urljoin, urlunparse
from collections import namedtuple

from pdf2image import convert_from_path, convert_from_bytes
from wand.image import Image as WImage

import unidecode

import requests
from requests import adapters
import ssl
from urllib3 import poolmanager
from urllib3.response import HTTPResponse
from shutil import copyfile
import threading, queue

from minio import Minio
#from minio.error import ResponseError, NoSuchKey
import json
import click
import os
import warnings
import logging
import uuid
import pickle
import math
import imghdr
import pprint
import cloudinary
import xxhash
import copy
import time
import datetime
import jwt
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, fresh_jwt_required, decode_token
)
import io
import tempfile
import mimetypes
import magic
import traceback

# for web screenshots
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import re
from flask import redirect
from PIL import Image as Img
from PIL import ImageOps, ImageFilter, ImageEnhance, ImageDraw, ImageFont
from bs4 import BeautifulSoup

from redis import Redis

import click
from multiprocessing import Pool
#from multiprocessing.pool import ThreadPool as Pool

from selenium.webdriver.remote.remote_connection import LOGGER as serverLogger
from urllib3.connectionpool import log as urllibLogger

from io import BytesIO

# caching
from flask_caching import Cache
import urllib.parse

from celery import Celery
from celery.utils.log import get_task_logger
import base64

# ...
# profiling
#from pycrunch_trace.client.api import trace



urllibLogger.setLevel(logging.WARNING)
logging.basicConfig(format='%(levelname)s: %(asctime)s.%(msecs)03d - %(message)s',
                    level=logging.DEBUG, datefmt='%d.%m.%Y %I:%M:%S %p')
applogger = logging.getLogger(__name__)
celery_logger = get_task_logger(__name__)
serverLogger.setLevel(logging.WARNING)


custom_errors = {
    'MethodNotAllowed': {
        'status': 400,
        'message': 'Wrong parameter value'
    }
}


pp = pprint.PrettyPrinter(indent=4)

app = Flask(__name__)

app.config['JWT_SECRET_KEY'] = app.config['SECRET_KEY']
jwtmng = JWTManager(app)

cors = CORS(app, resources={r"/*": {"origins": "*"}}, methods=['GET', 'POST', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'])
api = Api(app, errors=custom_errors)
auth = HTTPBasicAuth()
app.config.from_envvar('APPSETTINGS')
DEVELOP = app.config.get('DEVELOPMENT', False)


if not app.config.get('LOCAL', False):
    applogger.debug("REMOTE SERVER")
    import graypy
    GRAYLOG_HOST = app.config.get('GRAYLOG_HOST', 'localhost')
    GRAYLOG_PORT = app.config.get('GRAYLOG_PORT', 12201)
    GRAYLOG_PORT = int(GRAYLOG_PORT)
    applogger.debug(["GRAYLOG HOST", GRAYLOG_HOST])
    applogger.debug(["GRAYLOG PORT", GRAYLOG_PORT])
    graylog_handler = graypy.GELFUDPHandler(GRAYLOG_HOST, GRAYLOG_PORT)
    applogger.addHandler(graylog_handler)
    DEVELOP=False
else:
    applogger.debug("LOCAL SERVER")
    
db.init_app(app)
migrate = Migrate(app, db)
ma = Marshmallow(app)

REDIS_HOST = app.config.get('REDIS_HOST', 'localhost')
REDIS_PORT = app.config.get('REDIS_PORT', 6379)
REDIS_DB = app.config.get('REDIS_DB', 0)

cache = Cache(app, config={
    'CACHE_TYPE': 'redis',
    'CACHE_KEY_PREFIX': 'fcache',
    'CACHE_REDIS_HOST': REDIS_HOST,
    'CACHE_REDIS_PORT': REDIS_PORT,
    'CACHE_REDIS_DB'  : REDIS_DB,
    'CACHE_REDIS_URL': 'redis://{}:{}/{}'.format(REDIS_HOST, REDIS_PORT, REDIS_DB)
})

app.config['CELERY_BROKER_URL'] = 'redis://{}:{}/{}'.format(REDIS_HOST, REDIS_PORT, 1)
app.config['CELERY_RESULT_BACKEND'] = 'redis://{}:{}/{}'.format(REDIS_HOST, REDIS_PORT, 1)


app.config['SWAGGER'] = {
    'uiversion': 3
}
swtemplate = {
    "info": {
        "title": "Image Finder API",
        "description": "API for image search across multiple "
        "sources based on keywords, resolution and other parameters.",
        "version": "2",
    },
    "schemes": [
        "https"
    ]
}

swagger_config = {
    "headers": [
    ],
    "specs": [
        {
            "endpoint": 'apidescr',
            "route": '/apidescr.json',
            "rule_filter": lambda rule: True, 
            "model_filter": lambda tag: True,
        }
    ],
    "static_url_path": "/flasgger_static",
    "swagger_ui": True,
    "specs_route": "/docs/",
    'uiversion': 3
}


swagger = Swagger(app, config=swagger_config, template=swtemplate)
YKEY = app.config.get('YANDEX_API_KEY')
DPL_KEY = app.config.get('DEEPL_API_KEY')
PEXELS_KEY = app.config.get('PEXELS_KEY')
PIXABAY_KEY = app.config.get('PIXABAY_KEY')
PIXABAY_LOGIN = app.config.get('PIXABAY_LOGIN')
PIXABAY_PASSWORD = app.config.get('PIXABAY_PASSWORD')
IMGPATH = app.config.get('IMGPATH')
DEFAULT_LIMIT = app.config.get('DEFAULT_LIMIT')
MAX_LIMIT = app.config.get('MAX_LIMIT')
DEFAULT_WIDTH = app.config.get('DEFAULT_WIDTH')
YANDEX = "https://translate.yandex.net/api/v1.5/tr.json/translate?key={}&text={}&lang={}"
GOOGLE = 'https://translate.googleapis.com/translate_a/single?client=gtx&sl={}&dt=t&dt=bd&dj=1&text={}&tl={}'
DEEPL = 'https://api.deepl.com/v2/translate?source_lang={}&target_lang={}&text={}&auth_key={}'
PIXABAY_LOGINPAGE = 'https://pixabay.com/accounts/login/?source=main_nav&next=/'
PIXABAY_COOKIES = 'pixabay_cookies.txt'
PER_PAGE = app.config.get('PER_PAGE')
FLICKR_SEARCH_RADIUS = app.config.get('FLICKR_SEARCH_RADIUS')
FLICKR_KEY = app.config.get('FLICKR_KEY')
FLICKR_API = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key={FLICKR_KEY}&lat={lat}&lon={lon}&radius={radius}&format=json&per_page={limit}&nojsoncallback=1&has_geo=1&accuracy=8&sort=interestingness-desc"
FLICKR_TAG_API = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key={FLICKR_KEY}&tags={tags}&tag_mode={tagmode}&format=json&per_page={limit}&&nojsoncallback=1"
FLICKR_AUTHOR_API = "https://api.flickr.com/services/rest/?method=flickr.people.getInfo&api_key={FLICKR_KEY}&user_id={USER_ID}&format=json&nojsoncallback=1"
FLICKR_PHOTO_URL = "https://farm{farmid}.staticflickr.com/{serverid}/{photoid}_{secret}_b.jpg"
IMGUR_KEY = app.config.get('IMGUR_KEY')
IMGUR_SECRET = app.config.get('IMGUR_SECRET')
IMGUR_API = 'https://api.imgur.com/3/gallery/search/top?q_any={tags}&q_type=jpg'
UNSPLASH_KEY = app.config.get('UNSPLASH_KEY')
UNSPLASH_SECRET = app.config.get('UNSPLASH_SECRET')
UNSPLASH_API = 'https://api.unsplash.com/search/photos?query={tags}&client_id={key}&page={page}&per_page={per_page}'
GOOGLE_PLACES_API = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?key={key}&input={address}&inputtype=textquery&fields=formatted_address,geometry,id,name,photos,types,place_id"
GOOGLE_PLACES_DETAILS = "https://maps.googleapis.com/maps/api/place/details/json?key={key}&placeid={placeid}&fields=formatted_address,id,name,photos,place_id"
GOOGLE_PHOTOS_API = "https://maps.googleapis.com/maps/api/place/photo?key={key}&photoreference={photoid}&maxwidth={width}"
STREET_VIEW_API = "https://maps.googleapis.com/maps/api/streetview?location={lat},{lon}&size={width}x{height}&key={API_KEY}"
STREET_VIEW_METADATA_API = "https://maps.googleapis.com/maps/api/streetview/metadata?location={lat},{lon}size={width}x{height}&key={API_KEY}"
OSM_COORDS_TO_ADDRESS = "https://nominatim.openstreetmap.org/reverse?format=json&lat={lat}&lon={lon}&addressdetails=1&accept-language=en"
OSM_ADDRESS_TO_COORDS = "https://nominatim.openstreetmap.org/search?format=json&street={street}&city={city}&postalcode={zipcode}&addressdetails=1&accept-language=en"
GOOGLE_KEY = app.config.get('GOOGLE_KEY')
G_ADDRESS_TO_COORDS = "https://maps.googleapis.com/maps/api/geocode/json?address={address}&key={key}"
G_COORDS_TO_ADDRESS = "https://maps.googleapis.com/maps/api/geocode/json?latlng={latlon}&key={key}"
CLOUDINARY_KEY = app.config.get('CLOUDINARY_KEY')
CLOUDINARY_SECRET = app.config.get('CLOUDINARY_SECRET')
CLOUDINARY_CLOUD = app.config.get('CLOUDINARY_CLOUD')

MAX_RESIZE = app.config.get('MAX_RESIZE', 10000)
MAX_TEXT = app.config.get('MAX_TEXT', 300)
TEXT_FONT = app.config.get('TEXT_FONT', "/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf")
SUPPORTED_FORMATS = ['bmp', 'jpg', 'jpeg', 'png', 'gif', 'webp', 'j2k']
CHROME_PATH = app.config.get('CHROME_PATH', '/usr/bin/google-chrome')
CHROMEDRIVER_PATH = app.config.get('CHROMEDRIVER_PATH', '/usr/bin/chromedriver')
BREAKPOINTS = [544, 768, 992, 1200]
GRAVITYVALUES = ['north_east', 'north', 'north_west', 'west',
                 'south_west', 'south','south_east', 'east',
                 'center', 'face', 'faces']
FACE_DETECT_API = app.config.get('FACE_DETECT_API')
FACE_DETECT_USERNAME = app.config.get('FACE_DETECT_USERNAME')
FACE_DETECT_PASSWORD = app.config.get('FACE_DETECT_PASSWORD')
FACE_DETECT_UPDATE = app.config.get('FACE_DETECT_UPDATE', 'leave')

ARTICLES_API = app.config.get('ARTICLES_API')
ARTICLES_USERNAME = app.config.get('ARTICLES_USERNAME')
ARTICLES_PASSWORD = app.config.get('ARTICLES_PASSWORD')

MINIO_SERVER_URL = app.config.get('MINIO_SERVER_URL')
MINIO_ACCESS_KEY = app.config.get('MINIO_ACCESS_KEY')
MINIO_SECRET_KEY = app.config.get('MINIO_SECRET_KEY')
MINIO_BUCKET_NAME = app.config.get('MINIO_BUCKET_NAME')

NUMPROCESS = 20

JWT_ACCESS_TOKEN_EXPIRES=False

cloudinary.config(
    cloud_name = CLOUDINARY_CLOUD, 
    api_key = CLOUDINARY_KEY, 
    api_secret = CLOUDINARY_SECRET
)

#driver = None


def make_celery(app):
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery


celery = make_celery(app)



#@app.before_first_request
def create_chrome_instance():
    #global driver
    applogger.debug("Starting chrome")
    chrome_options = Options()
    chrome_options.add_argument("--disable-logging")
    chrome_options.add_argument('--log-level=3')
    chrome_options.add_experimental_option('excludeSwitches', ['enable-logging'])
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument("--service_log_path=/dev/null")
    chrome_options.add_argument("--hide-scrollbars")
    #chrome_options.add_argument("--window-size={},{}".format(int(dim['w']), int(dim['h'])))
    chrome_options.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.37 (KHTML, like Gecko) Chrome/73.0.3783.86 Safari/538.36")
    desired_capabilities = DesiredCapabilities().CHROME
    desired_capabilities['pageLoadStrategy'] = 'none'
    desired_capabilities['Cookie-Installing-Permission'] = 'all'
    chrome_options.binary_location = CHROME_PATH
    driver = webdriver.Chrome(
        executable_path=CHROMEDRIVER_PATH,
        chrome_options=chrome_options,
        desired_capabilities=desired_capabilities
    )
    applogger.debug(["Crome started", driver.service.process.pid])
    return driver

def countresults(query):
    count_q = query.statement.with_only_columns([func.count()]).order_by(None)
    count = query.session.execute(count_q).scalar()
    return count

@celery.task
def trigger_unsplash_download(url):
    print(url)
    res = requests.get(url)
    print(res)
    
@celery.task
def article_update(story_id, tags, filters, request_host, request_path):
    # search tags
    # update arrticle
    # imagetagsearch

    print(["REQ", story_id, tags, filters, request_host, request_path])
    # resp = ImageV2API().get()
    # print(["RESP", resp])
    images_query, cache_to_be_cleared = basic_image_search(tags=tags, limit=1, filters=filters)
    db.session.commit()
    db_response = images_query.first()
        
    if db_response:
        local_url = create_local_url(request_host, db_response.path, request_path)
        if filters:
            fltrs = [f.strip() for f in filters.split(",")]
            for fltr in fltrs:
                apply_single_chain(fltr)
            local_url = add_filters_to_url(local_url, filters)
            
            article_api_url = ARTICLES_API.format(story_id)
            req_payload = {"thumb":local_url}
            print(["ARTICLE UPDATE", article_api_url, req_payload, ARTICLES_USERNAME, ARTICLES_PASSWORD])
            article_resp = requests.put(article_api_url, auth=(ARTICLES_USERNAME, ARTICLES_PASSWORD), json=req_payload)
            # Getting Server Error (500) if URL is longer than 100 chars
            print(["ARTICLE API RESP", article_resp.status_code, article_resp.text])
        print(["RESP", local_url])
        

@celery.task
def minio_upload(fpath, buf_str, size, content_type, imgpath):
    buf = BytesIO(base64.b64decode(buf_str))
    print("CELERY MINIO START")
    celery_logger.debug("MINIO start")
    celery_logger.debug(["MINIO upload", fpath])
    minioClient = create_minio_client()
    #mthread = threading.Thread(target=minioClient.put_object, args=[MINIO_BUCKET_NAME, fpath, buf, size], kwargs={'content_type':content_type})
    #mthread.start()
    #mthread.join()
    minioClient.put_object(MINIO_BUCKET_NAME, fpath, buf, size, content_type=content_type)
    #applogger.debug("MINIO end")
    response = Response(buf.getvalue(), mimetype=content_type)
    cache.set("/api/v2/{}?".format(imgpath), response, timeout=300)
    cache.set("/api/v1/{}?".format(imgpath), response, timeout=300)
    celery_logger.debug(["CACHE", imgpath, content_type])
    celery_logger.debug("MINIO end")
    print("CELERY MINIO END")

def count_tags(tags, limit=None):
    images = db.session.query(Image).filter(Image.downloaded == 1).filter(Image.url is not None)
    taglist = []
    if not limit:
        limit = DEFAULT_LIMIT
    for tg in tags:
        tg = tg.lower()
        tagobj = db.session.query(Tag).filter(Tag.txt == tg).first()
        if tagobj:
            taglist.append(tagobj)
        
    images = images.filter(*[Image.tags.contains(t) for t in taglist]).limit(limit)
    for tg in taglist:
        for img in images:
            tagcounter = db.session.query(Counter).filter(Counter.image_id == img.id).filter(Counter.tag_id==tg.id).first()
            if not tagcounter:
                tagcounter = Counter(image_id=img.id, tag_id=tg.id, counter=1)
            else:
                tagcounter.counter = tagcounter.counter + 1
                    
            db.session.add(tagcounter)
            db.session.commit()
            applogger.debug(["TAGS COUNTER", tg, tagcounter.counter])
    
    
def update_tag_counter():
    applogger.debug(["UPDATE CALL", request.args.getlist("tags")])
    tags = request.args.getlist("tags", None)
    limit = request.args.get("limit", None)
    if tags:
        count_tags(tags, limit)

def update_img_counter():
    version_regex = r"/v\d/"
    VERSION = re.findall(version_regex, request.path)
    if VERSION:
        VERSION = VERSION[-1].replace("/", '')

    commonpath = request.path.replace(f"api/{VERSION}/", "")[1:]

    if "/" in commonpath:
        imgsource, otherpath = commonpath.split("/", 1)
    else:
        imgsource = ""
        otherpath = ""
        
    filterlist = []
    spliturl(otherpath, filterlist)
    filterstring = "/".join(filterlist)
    parentpath = otherpath.replace(filterstring, '')
    parentpath = "/".join([imgsource, parentpath.lstrip("/")])
    img = db.session.query(Image).filter(Image.path==parentpath).first()
    if img:
        imgcounter = db.session.query(Counter).filter(Counter.image_id == img.id).first()
        if not imgcounter:
            imgcounter = Counter(image_id=img.id, counter=1)
        else:
            if not imgcounter.counter:
                imgcounter.counter = 1
            else:
                imgcounter.counter = imgcounter.counter + 1
                
        db.session.add(imgcounter)
        db.session.commit()
        applogger.debug(["UPDATE CALL", imgcounter.counter])
    
def cache_key():
    applogger.debug(["CACHE CALL", request.args.getlist("tags")])
    args = request.args
    key = request.path + '?' + urllib.parse.urlencode([
        (k, v) for k in sorted(args) for v in sorted(args.getlist(k))
    ])
    return key



def clear_cache(key_prefix, exact_match=False):
    redis_client = Redis(REDIS_HOST, REDIS_PORT, REDIS_DB)
    applogger.debug("REDIS KEYS")
    if not exact_match:
        path_regexp = f"fcache\/.+\/{key_prefix}\?"
        matched_keys = [k for k in redis_client.keys() if re.match(path_regexp, k.decode('utf-8'))]
    else:
        path_key = f"fcache{key_prefix}"
        matched_keys = [k for k in redis_client.keys() if path_key == k.decode('utf-8')]
    applogger.debug(matched_keys)
    for key in matched_keys:
        redis_client.delete(key)

def create_minio_url(imgpath, imgsource=False):
    print(["DEVELOP", DEVELOP])
    if DEVELOP:
        PROTO = "http"
    else:
        PROTO = "https"
    if imgsource:
        img_url = f"{PROTO}://{MINIO_SERVER_URL}/{MINIO_BUCKET_NAME}/{imgpath}"
    else:
        img_url = f"{PROTO}://{MINIO_SERVER_URL}/{MINIO_BUCKET_NAME}/{IMGPATH}/{imgpath}"
    return img_url

def find_version(path):
    version_regex = r"/v\d/"
    VERSION = re.findall(version_regex, path)
    if VERSION:
        VERSION = VERSION[-1].replace("/", '')
    else:
        VERSION = 'v2'
    return VERSION

def create_local_url(host, imgpath, request_path):
    VERSION = find_version(request_path)
    local_host = host.replace('http://', 'https://', 1)
    img_url = f"https://{local_host}/api/{VERSION}/{imgpath}"
    return img_url

#@cache.cached(timeout=300, key_prefix=cache_key)
def serve_url(url, path=None):
    try:
        r = requests.get(url, timeout=2)
        #r.raise_for_status()
    except requests.exceptions.ReadTimeout as e:
        return make_response(jsonify(message="Minio Response Timeout"), 409)
        
    if r.status_code == 404:
        img_to_remove = db.session.query(Image).filter(Image.path==path).first()
        if img_to_remove:
            applogger.debug(["MINIO STATUS", r.status_code, path, img_to_remove])        
            db.session.delete(img_to_remove)
            db.session.commit()
        
    contenttype = mimetypes.guess_type(url)[0]
    return Response(
        r.content,
        status=r.status_code,
        content_type=contenttype,
    )


def create_minio_client():
    if DEVELOP:
        secure=False
    else:
        secure=True
        
    minioClient = Minio(MINIO_SERVER_URL,
                        access_key=MINIO_ACCESS_KEY,
                        secret_key=MINIO_SECRET_KEY,
                        secure=secure)
    
    return minioClient

        
class TLSAdapter(adapters.HTTPAdapter):

    def init_poolmanager(self, connections, maxsize, block=False):
        """Create and initialize the urllib3 PoolManager."""
        ctx = ssl.create_default_context()
        ctx.set_ciphers('DEFAULT@SECLEVEL=1')
        self.poolmanager = poolmanager.PoolManager(
                num_pools=connections,
                maxsize=maxsize,
                block=block,
                ssl_version=ssl.PROTOCOL_TLS,
                ssl_context=ctx)

        
class SQLAlchemyNoPool(SQLAlchemy):
    def apply_driver_hacks(self, app, info, options):
        options.update({
            'poolclass': NullPool
        })
        super(SQLAlchemy, self).apply_driver_hacks(app, info, options)

        
class ImageLocation:
    def __init__(self, lat=None, lon=None, address=None, zipcode=None, city=None):
        self.lat = lat
        self.lon = lon
        self.address = address
        self.zipcode = zipcode
        self.city = city

        
@app.errorhandler(405)
def method_not_allowed(e):
    return jsonify({'error': 400, 'message': "Wrong parameter value"}), 400


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 401)


@app.route('/api/v1/protected-fresh', methods=['GET'])
@app.route('/api/v2/protected-fresh', methods=['GET'])
@fresh_jwt_required
def protected_fresh():
    username = get_jwt_identity()
    return jsonify(fresh_logged_in_as=username), 200


@app.route('/api/v1/register', methods=['POST'])
@app.route('/api/v2/register', methods=['POST'])
@cross_origin(supports_credentials=True)
def register_user():
    """Register User API. After registration every user should be activated manually
    ---
    tags: [Authentication,]
    parameters:
      - name: username
        in: body
        required: true
      - name: password
        in: body
        required: true
    responses:
      201:
        description: User created
        schema:
          id: User
          properties:
            username:
              type: string
              description: User's login, in email format.
              default: "newuser@site.com"
            user_id:
              type: integer
              description: User ID.
              default: 1
            registered_on:
              type: string
              format: date-time
              description: Registration time
              default: Mon, 09 Dec 2019 21:26:52 GMT
            token:
              type: string
              description: Auth token
            is_confirmed:
              type: boolean
              description: If user is confirmed.
              default: "false"
      400:
        description: Wrong parameter value
      409: 
        description: User with same login exists
    """
    if len(request.data) == 0:
        return make_response(jsonify(message="No data provided"), 400)
        
    username = request.json.get('username', None)
    password = request.json.get('password', None)
    reset_password = request.json.get('reset_password', None)
    
    if not username:
        return make_response(jsonify(message="No username provided"), 400)
    if not password:
        return make_response(jsonify(message="No password provided"), 400)
        
    if len(username) < 3:
        return make_response(jsonify(message="username too short"), 400)
    if len(password) < 3:
        return make_response(jsonify(message="Password too short"), 400)
    if not "@" in username:
        return make_response(jsonify(message="Wrong username format"), 400)

    existing = db.session.query(User).filter(User.login == username).first()
    if not existing:
        newuser = User(login=username)
        newuser.hash_password(password)
        newuser.is_confirmed = True
        db.session.add(newuser)
        db.session.commit()
        access_token = create_access_token(identity=username, fresh=True, expires_delta=False)
        refresh_token = create_refresh_token(identity=username)

        res_dict = {'user_id': newuser.id,
                    'username': newuser.login,
                    #'password': password,
                    'is_confirmed': newuser.is_confirmed,
                    'registered_on': newuser.registered_on,
                    'token': access_token,
                    'refresh_token': refresh_token
        }
        return make_response(jsonify(res_dict), 201)
    #else:
    #    if reset_password:
    #        existing.hash_password(password)
    #        db.session.add(existing)
    #        db.session.commit()
            
    return make_response(jsonify({'Error': 'User already exists'}), 409)


@app.route('/api/v1/refresh', methods=['POST'])
@app.route('/api/v2/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    """Refresh Token API
    Provide a Refresh token to get a fresh access token
    ---
    tags: [Authentication,]
    responses:
      200:
        description: Returns a new access token
        schema:
          id: Token
          properties:
            username:
              type: string
              description: User's login, in email format.
              default: "newuser@site.com"
            user_id:
              type: integer
              description: User ID.
              default: 1
            access_token:
              type: string
              description: JWT Auth token
            refresh_token:
              type: string
              description: JWT Refresh token
      401:
        description: UNAUTHORIZED
    """

    current_user = get_jwt_identity()
    user = User.query.filter_by(login=current_user).first()
    if not user:
        fabort(401, "Not authorized")
    access_token = create_access_token(identity=current_user, fresh=True, expires_delta=False)
        
    ret = {
        'access_token': access_token,
    }
    return jsonify(ret), 200


@app.route('/api/v1/token', methods=['POST'])
@app.route('/api/v2/token', methods=['POST'])
@cross_origin(supports_credentials=True)
def get_auth_token_post():
    """Access Token API
    ---
    tags: [Authentication,]
    parameters:
      - name: username
        in: body
        required: true
      - name: password
        in: body
        required: true
    responses:
      200:
        description: User is authorised
        schema:
          id: Token
          properties:
            username:
              type: string
              description: User's login, in email format.
              default: "newuser@site.com"
            user_id:
              type: integer
              description: User ID.
              default: 1
            access_token:
              type: string
              description: JWT Auth token
            refresh_token:
              type: string
              description: JWT Refresh token
      401:
        description: UNAUTHORIZED
    """
    
    applogger.debug("LOGIN")
    username = request.json.get('username')
    password = request.json.get('password')
    user = User.query.filter_by(login = username).first()
    if user and user.is_confirmed:
        if user.verify_password(password):
            #token = user.generate_auth_token()
            #expires = datetime.timedelta(minutes=3)
            access_token = create_access_token(identity=username, fresh=True, expires_delta=False)
            refresh_token = create_refresh_token(identity=username)
            response = jsonify({ 'token': "%s" % access_token,
                                 'refresh_token': "%s" % refresh_token,
                                 "user_id":user.id,
                                 "username": user.login })
            return response
    return make_response(jsonify(message="Not authorized"), 401)


def spliturl(url, filterlist):
    if "/" in url:
        imgfilters, urlstring = url.split('/', 1)
        bd, fext = os.path.splitext(imgfilters)
        fext = fext.strip('.')
        if not "url" in imgfilters and not fext in SUPPORTED_FORMATS:
            filterlist.append(imgfilters)
            spliturl(urlstring, filterlist)
        else:
            return filterlist, urlstring

def non_empty_string(s):
    if not s:
        raise ValueError("Must not be empty string")
    return s
        
def add_filters_to_url(url, filterstring):
    """
    A function to modify an original image URL
    by adding a given filters string
    """
    parsedurl = urlparse(url)
    path = parsedurl.path.split("/")
    path.insert(len(path)-1, filterstring)
    newpath = "/".join(path)
    parsedurl = parsedurl._replace(path=newpath)
    newurl = urlunparse(parsedurl)
    return newurl

def add_srcset_to_url(srcsizes, url):
    sizes = [[f"f_scale-w_{s}", s] for s in BREAKPOINTS]
    srcsets = [" ".join([add_filters_to_url(url, s[0]), "{}w".format(s[1])]) for s in sizes]
    newurl = srcsets[-1].split(" ")[0]
    srcsets = ", ".join(srcsets)
    newsrcset = f'<img srcset="{srcsets}" sizes="{srcsizes}" src="{newurl}"'
    return newsrcset

@celery.task
def face_detect(image_id, buf_str, mode):
    #applogger.debug(["BUF", buf_str])
    buf = BytesIO(base64.b64decode(buf_str))
    with app.app_context():
    #if True:
        #db = SQLAlchemyNoPool()
        db.session.commit()
        image = db.session.query(Image).filter(Image.id == image_id).first()
        #applogger.debug(["FD", image_id, image, buf])
        files = {'imagefile': ("imagefile.jpg", buf.getvalue())}
        #applogger.debug([FACE_DETECT_API, files])
        faces = []

        try:
            if mode == "tags":
                payload = {'returndata': 'match'}
                response = requests.post(FACE_DETECT_API, files=files, data=payload, auth=(FACE_DETECT_USERNAME, FACE_DETECT_PASSWORD))
            else:
                response = requests.post(FACE_DETECT_API, files=files, auth=(FACE_DETECT_USERNAME, FACE_DETECT_PASSWORD))
        except requests.exceptions.ConnectionError:
            applogger.debug(["No response from FaceDetect API"])
            response = None
            return faces
        #except Exception as e:
        ##    applogger.debug("type error: " + str(e))
        #    applogger.debug(traceback.format_exc())
        #    return faces
    
        applogger.debug(["FD_RESP", response.status_code])
        if response.status_code == 200:
            applogger.debug(["FACE DETECT API RESPONSE"])
            resp_json = json.loads(response.text)
            if mode == "tags":
                faces = resp_json.get('faces_tag')
            else:
                faces = resp_json.get('faces_locations')
            
            applogger.debug(["FACE DETECT API RESPONSE", faces])
        else:
            applogger.debug(["NO RESPONSE FROM FACE DETECT API", faces])

        if image:
            image.faces_json = json.dumps(faces)
            db.session.add(image)
            db.session.commit()
            applogger.debug(["FDETECT DB IMAGE", image.faces_json])
        return faces
    #return faces

        
def recursive_search(tags, limit, width, orientation, funcs, source):
    applogger.debug(f"RECURSIVE SEARCH  {source}")
    if funcs:
        found = funcs[source](tags, limit, width, orientation)
        funcs.pop(source)
        applogger.debug("{} found {}".format(source, found))
        applogger.debug("REQUESTED {} FOUND {}".format(limit, found))
        if found < limit:
            limit = limit - found
            if len(list(funcs)) > 0:
                source = list(funcs)[0]
                applogger.debug("Moving for the next source, {}".format(source))
                recursive_search(tags, limit, width, orientation, funcs, source)
            
def getlocation(lat=None, lon=None, address=None, city=None, zipcode=None):
    applogger.debug("GET LOCATION CALL")
    location = None
    if any((address, city, zipcode)):
        location = osmgeolookup(address=address, zipcode=zipcode, city=city)
        applogger.debug(f"OSM GEO SEARCH {location}")
        if not location.lat and not location.lon:
            location = googlegeolookup(address=address, zipcode=zipcode, city=city)
            applogger.debug(f"GOOGLE GEO SEARCH {location}")
    elif all((lat, lon)):
        location = osmreverselookup(lat, lon)
        if not location.address:
            location = googlereverselookup(lat, lon, loc=location)
    return location


def pexelssearch(tags, limit, width, orientation):
    totalcount = 0
    source = 'pexels'
    tagstr = "+".join(tags)
    r = requests.get(f"https://api.pexels.com/v1/search?query={tagstr}&per_page={limit}&min_width={width}", headers={'Authorization': f'{PEXELS_KEY}'})
    applogger.debug("PEXELS search")
    applogger.debug(f"https://api.pexels.com/v1/search?query={tagstr}&per_page={limit}&min_width={width}")
    if r.status_code == 200:
        res = json.loads(r.content)
        photos = res.get("photos")
        if photos:
            argslist = []
            for p in photos:
                img_src = p['src']['original']
                image_source = p['src']['original']
                imgwidth = p['width']
                imgheight = p['height']
                pexels_url = p['url']
                applogger.debug(["PEXELS", p])
                # TODO Pexels author
                # "photographer": "Joey Farina"
                # "photographer_url": "https://www.pexels.com/@joey"
                img_author = p['photographer']
                resp = requests.get(pexels_url)
                
                if resp.status_code == 200:
                    soup = BeautifulSoup(resp.content, features="html.parser")
                    imgblock = soup.find_all("a", class_='js-photo-page-image-download-link', href=True)
                    if len(imgblock) > 0:
                        imgblock = imgblock[0]
                    imgurl = urlparse(imgblock['href']).query
                    imgurl = parse_qs(imgurl)
                    imgdescr = imgurl['dl']
                    if imgdescr:
                        imgdescr = imgdescr[-1]
                        imgtags = imgdescr.split("-")[:-1]
                        imgtags = [t for t in imgtags if t not in ['and', 'or', 'not']]
                        tags = tags + imgtags
                if orientation:
                    imgscale = imgwidth / imgheight
                    if orientation == 'landscape':
                        height = int(width * 1/imgscale)
                    elif orientation == 'portrait':
                        height = int(width * imgscale)
                        
                    getparams = f"?auto=compress&cs=tinysrgb&fit=crop&h={height}&w={width}"
                    img_src = img_src + getparams
                    applogger.debug(f'SCALE: {imgscale}, width: {width}, height: {height}')
                    # there are additional params: ?fit=crop&w=2000&h=3000
                    # Now we can get portrait or landscape results as we want
                    # image = getfile(img_src, source, width, orientation, tags=tags)
                dbtags = []
                if tags:
                    tags = list(set(tags))
                    for tag in tags:
                        prevtag = db.session.query(Tag).filter(Tag.txt == tag).first()
                        if not prevtag:
                            newtag = Tag(txt=tag)
                            db.session.add(newtag)
                            db.session.commit()
                            dbtags.append(newtag)
                        else:
                            dbtags.append(prevtag)
                dbtags = [t.id for t in dbtags]
                argslist.append([img_src, source, width, orientation, dbtags, None, image_source, None, img_author, True])
            # Paralleled requests 
            p = Pool(processes=NUMPROCESS)
            data = p.starmap(processfile, argslist)
            p.close()
            totalcount = data.count(True)

    return totalcount


def pixabaysearch(tags, limit, width, orientation):
    totalcount = 0
    source = 'pixabay'
    tagstr = "+".join(tags)
    orient = 'all'
    orient_dict = {'landscape': 'horizontal', 'portrait': 'vertical'}
    if orientation:
        orient = orient_dict[orientation]
    if not width:
        width = 0
    # Pixabay accepts per_page values from 3 to 200
    if limit < 3:
        pxlimit = 3
    else:
        pxlimit = limit
    r = requests.get(f"https://pixabay.com/api/?key={PIXABAY_KEY}&q={tagstr}&image_type=photo&&per_page={pxlimit}&min_width={width}&orientation={orient}")
    if r.status_code == 200:
        res = json.loads(r.content)
        if res['totalHits'] > 0:
            reslist = res['hits']
            if reslist:
                argslist = []
                for i in range(limit):
                    applogger.debug("DOWNLOADING RESULTS")
                    if reslist:
                        p = reslist.pop(0)
                        imgtags = p['tags'].split(", ")
                        tags = tags + imgtags
                        # TODO Pixabay author
                        # "user": "Josch13"
                        img_src = p['imageURL']
                        img_author = p['user']
                        dbtags = []
                        
                        if tags:
                            tags = list(set(tags))
                            for tag in tags:
                                prevtag = db.session.query(Tag).filter(Tag.txt == tag).first()
                                if not prevtag:
                                    newtag = Tag(txt=tag)
                                    db.session.add(newtag)
                                    db.session.commit()
                                    dbtags.append(newtag)
                                else:
                                    dbtags.append(prevtag)

                        # New format:
                        # img_src, source, imgwidth, imgorientation, tags, location, source_url, imgcredits
                        dbtags = [t.id for t in dbtags]
                        argslist.append([img_src, source, width, orientation, dbtags, None, img_src, None, img_author, True])
                # Paralleled requests 
                p = Pool(processes=NUMPROCESS)
                data = p.starmap(processfile, argslist)
                p.close()
                totalcount = data.count(True)
                
    return totalcount

def unsplashsearch(tags, limit, width, orientation):
    applogger.debug("UNSPLASH SEARCH")
    totalcount = 0
    source = 'unsplash'
    tagstr = " ".join(tags)
    per_page = 30
    pagenum = 1
    if not tags:
        return make_response(jsonify(message="Not enoung data for Unsplash search"), 400)

        
    photo_list = []
    page = 0
    applogger.debug(["REQUESTED LIMIT", limit])
    while len(photo_list) < limit:
        applogger.debug(["LENGTH", len(photo_list)])
        # >>>> If no results, break
        apistr = UNSPLASH_API.format(tags=tagstr, key=UNSPLASH_KEY, page=page+1, per_page=per_page, orientation=orientation)
        if orientation:
            apistr = apistr + f"&orientation={orientation}"
        resp = requests.get(apistr)
        applogger.debug(["UNSPLASH RESPONSE", resp.status_code])
        if resp.status_code == 200:
            res = json.loads(resp.content.decode("utf-8"))
            if res['total'] == 0:
                break
            data = res['results']
            for r in data:
                if r['width'] >= width & len(photo_list) < limit:
                    applogger.debug(["ADD PHOTO TO LIST", limit])
                    photo_list.append(r)
                    limit = limit - 1
        else:
            break
    # data collected
    argslist = []
    for p in photo_list:
        download_link = p['links']['download_location']
        download_link = download_link + f"&client_id={UNSPLASH_KEY}"
        trigger_unsplash_download.delay(download_link)
        image_url = p['urls']['full']
        img_author = p["user"]['username']
        applogger.debug(f"Fetching image {image_url}")
        image_tags = [t['title'] for t in p['tags']]
        if len(image_tags) > 0:
            tags = tags + image_tags
        if p["user"]["first_name"]:
            fname = unidecode.unidecode(p["user"]["first_name"])
        else:
            fname = ""
        if p["user"]["last_name"]:
            lname = unidecode.unidecode(p["user"]["last_name"])
        else:
            lname = ""
        imgcredits = """Photo by <a href="https://unsplash.com/@{username}?utm_source=imgfinder&utm_medium=referral">{fname} {lname}</a> on <a href="https://unsplash.com/?utm_source=imgfinder&utm_medium=referral">Unsplash</a>""".format(username=p["user"]["username"], fname=fname, lname=lname)
        source_url = image_url
        dbtags = []
        if tags:
            tags = list(set(tags))
            for tag in tags:
                prevtag = db.session.query(Tag).filter(Tag.txt == tag).first()
                if not prevtag:
                    newtag = Tag(txt=tag)
                    db.session.add(newtag)
                    db.session.commit()
                    dbtags.append(newtag)
                else:
                    dbtags.append(prevtag)
        dbtags = [t.id for t in dbtags]
        argslist.append([image_url, source, width, orientation, dbtags, None, source_url, imgcredits, img_author, True])
    p = Pool(processes=NUMPROCESS)
    data = p.starmap(processfile, argslist)
    p.close()
    totalcount = data.count(True)
    return totalcount
    
    
def flickrtagssearch(tags, limit, width, orientation):
    totalcount = 0
    source = 'flickrtags'
    tagstr = ",".join(tags)
    tagmode = 'all'
    if not tags:
        return make_response(jsonify(message="Not enoung data for Flickr tags search"), 400)
    total = 0
    apistr = FLICKR_TAG_API.format(FLICKR_KEY=FLICKR_KEY, tags=tagstr, tagmode=tagmode, limit=limit)
    applogger.debug(f"Performing Flickr tagssearch, {apistr}, requested {limit} images")
    r = requests.get(apistr)
    if r.status_code == 200:
        applogger.debug(r.content.decode("utf-8"))
        res = json.loads(r.content.decode("utf-8"))
        stat = res['stat']
        try:
            total = int(res["photos"]["total"])
        except:
            pass
        if total > 0 and stat == 'ok':
            argslist = []
            for crd in res["photos"]["photo"]:
                # Transform response to image URL
                dbtags = []
                if tags:
                    tags = list(set(tags))
                    for tag in tags:
                        prevtag = db.session.query(Tag).filter(Tag.txt == tag).first()
                        if not prevtag:
                            newtag = Tag(txt=tag)
                            db.session.add(newtag)
                            db.session.commit()
                            dbtags.append(newtag)
                        else:
                            dbtags.append(prevtag)
                applogger.debug(["FLICKR", crd])
                author_id = crd['owner']
                # TODO Flickr author
                author_api_str = FLICKR_AUTHOR_API.format(FLICKR_KEY=FLICKR_KEY, USER_ID=author_id)
                author_res = requests.get(author_api_str)
                if author_res.status_code == 200:
                    author_json = json.loads(author_res.content.decode("utf-8"))
                    img_author = author_json['person']['username']['_content']
                else:
                    img_author = None
                    
                image_url = FLICKR_PHOTO_URL.format(farmid=crd["farm"],
                                                    serverid=crd["server"],
                                                    photoid=crd["id"],
                                                    secret=crd["secret"])
                dbtags = [t.id for t in dbtags]
                argslist.append([image_url, source, width, orientation, dbtags, None, image_url, None, img_author, True])
                applogger.debug(argslist)
            p = Pool(processes=NUMPROCESS)
            data = p.starmap(processfile, argslist)
            p.close()
            totalcount = data.count(True)

    return totalcount


def imgursearch(tags, limit, width, orientation):
    applogger.debug("IMGUR SEARCH")
    totalcount = 0
    cnt = 0
    source = 'imgur'
    page = 1
    tagstr = " ".join(tags)
    if not tags:
        return make_response(jsonify(message="Not enoung data for Imgur search"), 400)

    total = 0
    apistr = IMGUR_API.format(tags=tagstr)
    headers = {'Authorization': f'Client-ID {IMGUR_KEY}'}
    applogger.debug(apistr)
    r = requests.get(apistr, headers=headers)
    if r.status_code == 200:
        res = json.loads(r.text)
        if res['success'] and res['status'] == 200:
            for gallery in res['data']:
                imgtags = [t['name'] for t in gallery['tags']]
                if len(imgtags) > 0:
                    tags = tags + imgtags
                if 'images' in gallery:
                    argslist = []
                    img_author = gallery['account_url']
                    for img in gallery['images']:
                        if cnt <= limit:
                            imgwidth = img['width']
                            imgheight = img['height']
                            image_url = img['link']
                            if width and imgwidth >= width:
                                dbtags = []
                                if tags:
                                    tags = list(set(tags))
                                    for tag in tags:
                                        prevtag = db.session.query(Tag).filter(Tag.txt == tag).first()
                                        if not prevtag:
                                            newtag = Tag(txt=tag)
                                            db.session.add(newtag)
                                            db.session.commit()
                                            dbtags.append(newtag)
                                        else:
                                            dbtags.append(prevtag)
                                dbtags = [t.id for t in dbtags]
                                argslist.append([image_url, source, width, orientation, dbtags, None, image_url, None, img_author, True])
                                cnt = cnt + 1
                    
                    p = Pool(processes=NUMPROCESS)
                    data = p.starmap(processfile, argslist)
                    p.close()
                    totalcount = data.count(True)

    return totalcount


def cloudinarysearch(tags, limit, width, orientation):
    totalcount = 0
    source = 'cloudinary'
    tagstr = " ".join(tags)
    cl_res = cloudinary.Search().expression(f'resource_type:image AND tags={tagstr}').max_results(str(limit)).execute()
    if cl_res['total_count'] > 0:
        argslist = []
        for r in cl_res['resources']:
            img_src = r['secure_url']
            # TODO cloudinary author
            dbtags = []
            if tags:
                tags = list(set(tags))
                for tag in tags:
                    prevtag = db.session.query(Tag).filter(Tag.txt == tag).first()
                    if not prevtag:
                        newtag = Tag(txt=tag)
                        db.session.add(newtag)
                        db.session.commit()
                        dbtags.append(newtag)
                    else:
                        dbtags.append(prevtag)
            dbtags = [t.id for t in dbtags]
            argslist.append([img_src, source, width, orientation, dbtags, None, None, None, None, True])
        p = Pool(processes=NUMPROCESS)
        data = p.starmap(processfile, argslist)
        p.close()
        totalcount = data.count(True)

    return totalcount

def saveurl(imgtags, source, image_url, findface=True, location_id=None, license=None, publisher=None, descr=None, filters=None):
    """
    Save an image by url. Works for both external & internal image URLs
    """

    # 0. check for db records
    # if no db records: 
    

    minioClient = create_minio_client()
    parsedurl = urlparse(image_url)
    imgpath = parsedurl.path
    img_mime_type = mimetypes.guess_type(imgpath)[0]
    minio_path = imgpath.replace(MINIO_BUCKET_NAME, "")[2:]
    other_path = imgpath[1:]
    
    with app.app_context():
        existing_minio_db = db.session.query(Image).filter(Image.path==minio_path).first()
        existing_other_db = db.session.query(Image).filter(Image.url==image_url).first()
        if not existing_minio_db and not existing_other_db:
            applogger.debug(["UPLOAD URL NOT EXISTING"])
            try:
                minio_obj = minioClient.stat_object(MINIO_BUCKET_NAME, minio_path)
                minio_url = "{}://{}{}".format(parsedurl.scheme, parsedurl.netloc, parsedurl.path)
                source="minio"
                applogger.debug(["UPLOAD URL MINIO", minio_obj.object_name, imgpath, source])
                
                downloaded = processfile(minio_url, source, None, None, imgtags, location_id, minio_url, None, False)
                
                db.session.commit()

                image = db.session.query(Image).filter(Image.url == minio_url).filter(Image.downloaded == 1).first()
                applogger.debug(["MINIO URL", "NEW IMAGE RECORD", downloaded, image.id]
                )
                
                return image.id, False
                # get minio image
                # get image properties
                # create db record
                # return image
                
            except:# NoSuchKey:
                applogger.debug(["UPLOAD URL", "NEW IMAGE", imgpath, minio_path, other_path, img_mime_type])
                # img_src, source, imgwidth, imgorientation, imgtags, location, source_url, imgcredits)
                downloaded = processfile(image_url, source, None, None, imgtags, location_id, image_url, None, None, True)
                db.session.commit()
                image = db.session.query(Image).filter(Image.url == image_url).filter(Image.downloaded == 1).first()
                if not image:
                    #message = f'Wrong URL'
                    #return make_response(jsonify(message=message), 400)
                    fabort(400, "Wrong URL")

                applogger.debug(["UPLOAD URL", "NEW IMAGE", downloaded, image])
                return image.id, False
                # upload to minio
                # get the image
                # get image properties
                # create db record
                # put to minio
                # return image
        
        elif existing_minio_db:
            # return image
            return existing_minio_db.id, True

        elif existing_other_db:
            # return image            
            return existing_other_db.id, True
    
    return

    ### >>> Modify
    existing = False
    fileext = os.path.splitext(uploaded_file.filename)[-1]
    if not fileext:
        fileext = '.jpg'
    
    applogger.debug(["FILEEXT", fileext])
    img_mime_type = mimetypes.guess_type(uploaded_file.filename)[0]
    newfilename = str(uuid.uuid4())+fileext
    applogger.debug(["NEWFILENAME", newfilename])
    directory = os.path.join(IMGPATH, source)
    fpath = os.path.join(directory, newfilename)
    
    filehash = xxhash.xxh64()


    ####
    
    with tempfile.NamedTemporaryFile() as f:
        chunk_size = 1024
        while True:
            chunk = uploaded_file.read(chunk_size)
            if len(chunk) == 0:
                break
            f.write(chunk)
            filehash.update(chunk)
        hashstr = filehash.hexdigest()
        applogger.debug(f"Uploaded file hash string is {hashstr}")
        f.seek(0)
        existing_img = None
        buf = None
        with app.app_context():
            existing_img = db.session.query(Image).filter(Image.hashstr==hashstr).filter(Image.source==source).first()
            if not existing_img:
                applogger.debug("Saving new image")
                applogger.debug("Create DB record")

                newimage = Image(source=source,
                                 hashstr=hashstr,
                                 path=source + os.sep + newfilename,
                )
            
                width, height = Img.open(f.name).size
                orientation = "landscape"
                if height >= width:
                    orientation = 'portrait'
                newimage.width = width
                newimage.height = height
                newimage.orientation = orientation
                newimage.downloaded = True
                newimage.score = 1
                
                applogger.debug("MINIO")
                applogger.debug("Thread start")
                buf = BytesIO(f.read())
                # Move to background
                buf_str = base64.b64encode(buf.read()).decode('ascii')
                minio_upload.delay(fpath, buf_str, os.stat(f.name).st_size, img_mime_type, newimage.path)
                
            else:
                applogger.debug("Hashstring match, existing record {}".format(existing_img.id))
                newimage = existing_img
                existing = True
            
            # ADDING TAGS
            if imgtags:
                tags = db.session.query(Tag).filter(Tag.id.in_(imgtags)).all()
                newimage.tags = tags

            if not newimage.faces_json and findface:
                buf = BytesIO(f.read())
                # Move to background
                buf_str = base64.b64encode(buf.read()).decode('ascii')
                #print(["BUF_STR", buf_str])
                faces = face_detect.run(None, buf_str,  "faces")
                #faces = face_detect(f.name, "faces")
                newimage.faces_json = json.dumps(faces)
            if location_id:
                db_location = db.session.query(Location).filter(Location.id==location_id).first()
                if db_location:
                    newimage.location = db_location
            if license:
                newimage.license = license
            if publisher:
                newimage.publisher = publisher
            if descr:
                newimage.descr = descr
            db.session.add(newimage)
            db.session.commit()
            applogger.debug("New image added {}".format(newimage.path))
            return newimage.id, existing
    ### >>> Modify
    

def savefile(imgtags, source, uploaded_file, findface=True, location_id=None, outqueue=None, license=None, publisher=None, descr=None, source_url=None):
    """
    Save an uploaded file
    """
    
    existing = False
    fileext = os.path.splitext(uploaded_file.filename)[-1]
    if not fileext:
        fileext = '.jpg'
    
    applogger.debug(["FILEEXT", fileext])
    img_mime_type = mimetypes.guess_type(uploaded_file.filename)[0]
    newfilename = str(uuid.uuid4())+fileext
    applogger.debug(["NEWFILENAME", newfilename])
    directory = os.path.join(IMGPATH, source)
    fpath = os.path.join(directory, newfilename)
    
    filehash = xxhash.xxh64()
    
    with tempfile.NamedTemporaryFile() as f:
        chunk_size = 1024
        while True:
            chunk = uploaded_file.read(chunk_size)
            if len(chunk) == 0:
                break
            f.write(chunk)
            filehash.update(chunk)
        hashstr = filehash.hexdigest()
        applogger.debug(f"Uploaded file hash string is {hashstr}")
        f.seek(0)
        existing_img = None
        buf = None
        with app.app_context():
            existing_img = db.session.query(Image).filter(Image.hashstr==hashstr).filter(Image.source==source).first()
            if not existing_img:
                applogger.debug("Saving new image")
                applogger.debug("Create DB record")

                newimage = Image(source=source,
                                 hashstr=hashstr,
                                 path=source + os.sep + newfilename,
                )
            
                width, height = Img.open(f.name).size
                orientation = "landscape"
                if height >= width:
                    orientation = 'portrait'
                newimage.width = width
                newimage.height = height
                newimage.orientation = orientation
                newimage.downloaded = True
                newimage.score = 1
                if source_url:
                    newimage.source_url = source_url
                applogger.debug("MINIO")
                applogger.debug("Thread start")
                buf = BytesIO(f.read())
                # Move to background
                buf_str = base64.b64encode(buf.read()).decode('ascii')
                minio_upload.delay(fpath, buf_str, os.stat(f.name).st_size, img_mime_type, newimage.path)
                
            else:
                applogger.debug("Hashstring match, existing record {}".format(existing_img.id))
                newimage = existing_img
                existing = True
            
            # ADDING TAGS
            if imgtags:
                tags = db.session.query(Tag).filter(Tag.id.in_(imgtags)).all()
                newimage.tags = tags

            if location_id:
                db_location = db.session.query(Location).filter(Location.id==location_id).first()
                if db_location:
                    newimage.location = db_location
            if license:
                newimage.license = license
            if publisher:
                newimage.publisher = publisher
            if descr:
                newimage.descr = descr
            db.session.add(newimage)
            db.session.commit()
            if not newimage.faces_json and findface:
                buf = BytesIO(f.read())
                # Move to background
                buf_str = base64.b64encode(buf.read()).decode('ascii')
                #print(["BUF_STR", buf_str])
                face_detect.delay(newimage.id, buf_str,  "faces")

            applogger.debug("New image added {}".format(newimage.path))
            return newimage.id, existing
 

def processfile(img_src, source, imgwidth, imgorientation, imgtags, location, source_url, imgcredits, img_author, upload_to_minio):
    #
    #with app.app_context():
    if True:
        db = SQLAlchemyNoPool()
        minioClient = create_minio_client()
        downloaded = False
        image = None
        if imgtags:
            tags = db.session.query(Tag).filter(Tag.id.in_(imgtags)).all()
        else:
            tags = None
        
        applogger.debug(f"IMG SRC {img_src}")
        image = db.session.query(Image).filter(Image.url == img_src).filter(Image.downloaded == 1).first()
        if image:
            applogger.debug("File already downloaded {} {}".format(image.id, [tag.txt for tag in image.tags]))
            # If the file was already downloaded with a different set of tags
            # Here we should update the image tags, by adding imgtags to the existing
            # image tags list
            # >>> Issue 145
            #for tag in
            if tags:
                #tags = db.session.query(Tag).filter(Tag.id.in_(imgtags)).all()
                for tag in tags:
                    if tag not in image.tags:
                        image.tags.append(tag)
                        db.session.add(image)
                        db.session.commit()
            return True
        
        else:
            applogger.debug("Nothing found for {}, performing online search".format(img_src))
            file_url = urlparse(img_src).path.rsplit("/")[-1]
            fileext = os.path.splitext(file_url)[-1]
            if not fileext:
                fileext = '.jpg'
            # img_mime_type = mimetypes.guess_type(file_url)[0]
            if not upload_to_minio and source == 'minio':
                parsedurl = urlparse(img_src)
                fpath = parsedurl.path[1:].replace(MINIO_BUCKET_NAME, "")[1:]
            else:
                newfilename = str(uuid.uuid4())+fileext
                directory = os.path.join(IMGPATH, source)
                fpath = os.path.join(directory, newfilename)
            if source:
                applogger.debug("GET FROM {} {}".format(source, img_src))
                try:
                    response = requests.get(img_src, stream=True)
                except requests.exceptions.ConnectionError:
                    return False
            
            if response.status_code == 200:
                applogger.debug("FILE DOWNLOADED")
                filehash = xxhash.xxh64()
                with tempfile.NamedTemporaryFile() as f:
                    for chunk in response.iter_content(1024):
                        f.write(chunk)
                        filehash.update(chunk)
                    hashstr = filehash.hexdigest()

                    applogger.debug(f"Downloaded file hash string is {hashstr}")
                    img_mime_type = magic.from_file(f.name, mime=True)
                    f.seek(0)
                    buf = BytesIO(f.read())
                    existing_img = db.session.query(Image).filter(Image.hashstr==hashstr).filter(Image.source==source).first()
                    if not existing_img:
                        applogger.debug("Saving new image")
                        # Put file to Minio storage
                        newimage = Image(url=img_src, source=source, hashstr=hashstr)
                        pil_img = Img.open(f.name)
                        width, height = pil_img.size
                    
                        orientation = "landscape"
                        if height >= width:
                            orientation = 'portrait'
                        applogger.debug(f'Min_width: {imgwidth}, orig_width: {width}')
                        applogger.debug(f'Req_ orientation: {imgorientation}, orig_width: {orientation}')
                        if imgwidth:
                            if width < imgwidth:
                                applogger.debug(f"Image width {imgwidth} is less than required width {width}, returning None")
                                return None
                        if imgorientation:
                            if imgorientation != orientation:
                                applogger.debug(f"Image orientation {imgorientation} doesn't match required orientation {orientation}, returning None")
                                return None
                        newimage.width = width
                        newimage.height = height
                        newimage.orientation = orientation
                        if not upload_to_minio and source == 'minio':
                            newimage.path = fpath
                        else:
                            newimage.path = source + os.sep + newfilename
                        newimage.downloaded = True


                        applogger.debug(["PROCESSFILE", newimage.path, img_mime_type])
                        if upload_to_minio:
                            # Move to background
                            buf_str = base64.b64encode(buf.read()).decode('ascii')
                            minio_upload.delay(fpath, buf_str, os.stat(f.name).st_size, img_mime_type, newimage.path)

                        if imgcredits:
                            newimage.imgcredits = imgcredits
                        if source_url:
                            newimage.source_url = source_url
                        if img_author:
                            newimage.author = img_author
                    
                    else:
                        applogger.debug("Hashstring match, existing record {}".format(existing_img.id))
                        downloaded = True
                        newimage = existing_img
                
                    # ADDING TAGS
                    if imgtags:
                        applogger.debug("ADDING TAGS")
                        if tags:
                            #tags = db.session.query(Tag).filter(Tag.id.in_(imgtags)).all()
                            newimage.tags = tags
                    if location:
                        location_query = db.session.query(Location)
                        if location.lat:
                            location_query = location_query.filter(Location.lat == str(location.lat))
                        if location.lon:
                            location_query = location_query.filter(Location.lon == str(location.lon))
                        if location.address:
                            location_query = location_query.filter(Location.addr == location.address)
                        if location.zipcode:
                            location_query = location_query.filter(Location.zipcode == location.zipcode)
                        if location.city:
                            location_query = location_query.filter(Location.city == location.city)
                        db_location = location_query.first()

                        if not db_location:
                            db_location = Location(lat=str(location.lat),
                                                   lon=str(location.lon),
                                                   addr=location.address,
                                                   zipcode=location.zipcode,
                                                   city=location.city
                            )
                            db.session.add(db_location)
                            db.session.commit()
                            applogger.debug("Adding new location {}".format(db_location))
                        else:
                            applogger.debug("Using existing location {}".format(db_location))
                        db.session.add(newimage)
                        db.session.commit()
                        newimage.location = db_location
                        applogger.debug("Image location added {}".format(db_location))
                        # check if pixabay has returned an image
                        # imgtype = imghdr.what(fpath)
                        #applogger.debug("Image type by imghdr {}".format(imgtype))
                        #if not imgtype and source == 'pixabay':
                        #    os.remove(fpath)
                        #    applogger.debug(f"Too many requests for {source} API. {newimage}")
                        #    #
                        #    message = f"Too many requests for {source} API"
                        #    return make_response(jsonify(message=message), 400)
                        
                        #del newimage
                    
                                     
                    db.session.add(newimage)
                    db.session.commit()
                    # Face Detection call
                    # Move to background
                    #if not newimage.faces_json:
                    #face_detect(newimage.id, buf, "faces")
                    #newimage.faces_json = json.dumps(faces)
                    buf.seek(0)
                    buf_str = base64.b64encode(buf.read()).decode('ascii')
                    #print(["BUF_STR", buf_str])
                    #applogger.debug(["DETECT FACES", buf_str])
                    face_detect.delay(newimage.id, buf_str, "faces")
                    #fd_thread = threading.Thread(target=face_detect, args=[newimage.id, buf, "faces"])
                    #fd_thread.start()
                    applogger.debug("New image added {}".format(newimage))
                    downloaded = True
        db.session.remove()
        db.session.close()
        db.engine.dispose()
        applogger.debug("Final return from getfile")

        return downloaded


def googlegeolookup(address=None, zipcode=None, city=None):
    """
    Google Geocode API Lookup (address to coordinates)
    """
    lat = None
    lon = None
    addr_str = None
    addr_list = []
    if address:
        addr_list.append(address.replace(", ", "+").replace(",", "+").replace(" ", "+"))
    if city:
        addr_list.append(city.replace(" ", "+"))
    if zipcode:
        addr_list.append(str(zipcode))
        
    addr_str = ",+".join(addr_list)
    res = requests.get(G_ADDRESS_TO_COORDS.format(address=addr_str, key=GOOGLE_KEY))
    if res.status_code == 200:
        res_json = json.loads(res.content)
        if len(res_json['results']) > 0:
            applogger.debug(res_json['results'])
            glocation = res_json['results'][0]['geometry']['location']
            applogger.debug("Google geo lookup {}".format(glocation))
            lat = glocation.get('lat', None)
            lon = glocation.get('lng', None)
    location = ImageLocation(lat=lat, lon=lon, zipcode=zipcode, city=city, address=address)
    return location

def googlereverselookup(lat, lon, loc=None):
    """
    Google Geocode API Reverse Lookup (coordinates to address)
    """
    address = None
    city = None
    zipcode = None
    res = requests.get(G_COORDS_TO_ADDRESS.format(latlon=",".join(map(str, [lat,lon])), key=GOOGLE_KEY))
    if res.status_code == 200:
        res_json = json.loads(res.content)
        if len(res_json['results']) > 0:
            res_json = res_json['results'][0]
            addr_components = res_json['address_components']
            address = ", ".join([v['long_name'] for v in addr_components if 'street_number' in v['types'] or 'route' in v['types']])
            zipcode = [v['long_name'] for v in addr_components if 'postal_code' in v['types']]
            city = [v['long_name'] for v in addr_components if 'locality' in v['types']]
            if len(city) == 0:
                city = [v['long_name'] for v in addr_components if 'postal_town' in v['types']]
            if len(zipcode) == 0 and len(city) == 0:
                address = res_json['formatted_address']
                zipcode = None
                city = None
            else:
                if len(zipcode) > 0:
                    zipcode = zipcode[0]
                else:
                    zipcode = None
                if city:
                    city = city[0]
    if not loc:
        location = ImageLocation(lat=lat, lon=lon, zipcode=zipcode, city=city, address=address)
    else:
        location = ImageLocation(lat=loc.lat, lon=loc.lon, zipcode=loc.zipcode, city=loc.city, address=address)
    return location
            
def osmgeolookup(address=None, zipcode=None, city=None):
    """
    OpenStreetMap Geo Lookup API (Address to Coords)
    """
    lat = None
    lon = None
    if address is None:
        address = ""
    if zipcode is None:
        zipcode = ""
    if city is None:
        city = ""
        
    coords_res = requests.get(OSM_ADDRESS_TO_COORDS.format(street=address,
                                                       zipcode=zipcode,
                                                       city=city))
    applogger.debug(OSM_ADDRESS_TO_COORDS.format(street=address,
                                           zipcode=zipcode,
                                           city=city))
    if coords_res.status_code == 200:
        coords_json = json.loads(coords_res.content)
        if not "error" in coords_json:
            if len(coords_json) > 0:
                coords_json = coords_json[0]
                applogger.debug(coords_json)
                lat = coords_json.get('lat', None)
                lon = coords_json.get('lon', None)
                if address == "":
                    address = coords_json['address'].get('road', None)
                if zipcode == "":
                    zipcode = coords_json['address'].get('postcode', None)
                if city == "":
                    city = coords_json['address'].get('city', None)
    
    location = ImageLocation(lat=lat, lon=lon, zipcode=zipcode, city=city, address=address)
    return location

def osmreverselookup(lat, lon):
    """
    OpenStreetMap Reverse Geo Lookup API (Coords to Address)
    """
    address = None
    zipcode = None
    city = None
    
    addr_res = requests.get(OSM_COORDS_TO_ADDRESS.format(lat=lat, lon=lon))
    applogger.debug(OSM_COORDS_TO_ADDRESS.format(lat=lat, lon=lon))
    if addr_res.status_code == 200:
        addr_json = json.loads(addr_res.content)
        if not "error" in addr_json:
            if "postcode" in addr_json['address']:
                address = "{} {}".format(addr_json['address'].get("road", ""),  addr_json['address'].get('house_number', "")).strip()
                zipcode = addr_json['address']['postcode']
                city = "{}".format(addr_json['address'].get('city', "")).strip()
            elif "display_name" in addr_json:
                address = addr_json['display_name']
    location = ImageLocation(lat=lat, lon=lon, zipcode=zipcode, city=city, address=address)
    return location
     

def googleplacessearch(limit=None, location=None, width=None):
    """
    Google Places API Search
    ===
    limit: maximum output results
    location: LoactionTuple instance
    """
    total = 0
    if not location:
        message='Not enoung data for Google location search'
        return make_response(jsonify(message=message), 400)
    addr = []
    if location.address:
        addr.append(location.address)
    if location.city:
        addr.append(location.city)
    if location.zipcode:
        addr.append(str(location.zipcode))
    addr = " ".join(addr)
    places_res = requests.get(GOOGLE_PLACES_API.format(key=GOOGLE_KEY, address=addr))
    if places_res.status_code == 200:
        places_json = json.loads(places_res.content)
        if places_json['status'] == 'OK':
            placeid = places_json['candidates'][0].get('place_id', None)
            if placeid:
                details_res = requests.get(GOOGLE_PLACES_DETAILS.format(key=GOOGLE_KEY, placeid=placeid))
                if details_res.status_code == 200:
                    details_json = json.loads(details_res.content)
                    if details_json['status'] == 'OK':
                        photos = details_json['result'].get('photos', None)
                        if photos:
                            if limit > len(photos):
                                limit = len(photos)
                            photos = photos[:limit]
                            applogger.debug(photos)
                            argslist = []
                            for p in photos:
                                if p['width'] >= width:
                                    photoid = p['photo_reference']
                                    applogger.debug(f"LIMIT {limit}")
                                    image_url = GOOGLE_PHOTOS_API.format(key=GOOGLE_KEY, photoid=photoid, width=width)
                                    argslist.append([image_url, 'google', None, None, None, location, None, None])
                            p = Pool(processes=NUMPROCESS)
                            data = p.starmap(processfile, argslist)
                            p.close()
                            total = data.count(True)

    # Get place ID based on location
    # Get place details
    # get place photos
    
    applogger.debug(f"Google Places API Search,  location: {location}")
    return total


def flickrsearch(limit, location=None):
    """
    Flickr API location-based images search
    ===
    location: ImageLocation instance
    limit: output results limit
    ===
    Returns total results found
    """
    totalcount = 0
    if not location:
        message='Not enoung data for Flickr location search'
        return make_response(jsonify(message=message), 400)
    total = 0
    apistr = FLICKR_API.format(FLICKR_KEY=FLICKR_KEY, lat=location.lat, lon=location.lon, radius=FLICKR_SEARCH_RADIUS, limit=limit)
    applogger.debug(f"Performing Flickr search, {apistr}, requested {limit} images")
    r = requests.get(apistr)
    if r.status_code == 200:
        applogger.debug(r.content.decode("utf-8"))
        res = json.loads(r.content.decode("utf-8"))
        stat = res['stat']
        try:
            total = int(res["photos"]["total"])
        except:
            pass
        if total > 0 and stat == 'ok':
            argslist = []
            for crd in res["photos"]["photo"]:
                # Transform response to image URL
                image_url = FLICKR_PHOTO_URL.format(farmid=crd["farm"],
                                                    serverid=crd["server"],
                                                    photoid=crd["id"],
                                                    secret=crd["secret"])
                applogger.debug(f"Fetching image {image_url}")
                author_id = crd['owner']
                # TODO Flickr author
                author_api_str = FLICKR_AUTHOR_API.format(FLICKR_KEY=FLICKR_KEY, USER_ID=author_id)
                author_res = requests.get(author_api_str)
                if author_res.status_code == 200:
                    author_json = json.loads(author_res.content.decode("utf-8"))
                    img_author = author_json['person']['username']['_content']
                else:
                    img_author = None

                argslist.append([image_url, 'flickr', None, None, None, location, image_url, None, img_author, True])
            p = Pool(processes=NUMPROCESS)
            data = p.starmap(processfile, argslist)
            p.close()
            #p.terminate()
            p.join()
            totalcount = data.count(True)
        applogger.debug(f"FLICKR GEO SEARCH FOUND: {totalcount}, requested: {limit}")
    return totalcount


def imagetagsearch(source=None, tags=None, limit=None, width=None, orientation=None):
    """
    Stock photo search from different sources
    If no explicit source provided, search is
    performed in the following order,
    pexels, pixabay, cloudinary, flickr tags, imgur, unsplash
    """
    found = 0
    applogger.debug(f"SEARCH LIMIT {limit}")
    # setting default value for limit
    if not limit or limit > MAX_LIMIT:
        limit = DEFAULT_LIMIT
    if not width:
        width = DEFAULT_WIDTH

    # Add new search functions here
    searchfunctions = {
        'unsplash': unsplashsearch,
        'pixabay': pixabaysearch,
        'pexels': pexelssearch,
        'flickrtags': flickrtagssearch,
        'imgur': imgursearch,
        'cloudinary': cloudinarysearch
    }
    
    if not source:
        applogger.debug("No source provided, searching all sources")
        src = list(searchfunctions)[0]
        # tagcounter
        applogger.debug("Recursive search started")
        recursive_search(tags, limit, width, orientation, searchfunctions, src)
    
    if source:
        searchf = searchfunctions.get(source, None)
        if searchf:
            searchf(tags, limit, width, orientation)
            
        
def translate_tag(source, lang, tag):
    transl = None
    if source == 'google':
        sl, tl = lang.split("-")
        req = GOOGLE.format(sl, tag, tl)
        resp = requests.get(req)
        if resp.status_code == 200:
            transl = json.loads(resp.content)
            if 'sentences' in transl:
                transl = transl['sentences'][-1]['trans']
    elif source == 'yandex':
        req = YANDEX.format(YKEY, tag, lang)
        resp = requests.get(req)
        if resp.status_code == 200:
            transl = json.loads(resp.content)
            if 'text' in transl:
                transl = transl['text'][-1]
    elif source == 'deepl':
        sl, tl = lang.split("-")
        req = DEEPL.format(sl, tl, tag, DPL_KEY)
        resp = requests.get(req)
        if resp.status_code == 200:
            transl = json.loads(resp.content)
            if "translations" in transl:
                transl = transl['translations'][-1]['text']
    return transl


def basic_image_search(filters=None, tags=None, limit=None, offset=None, source=None, output=None, show_srcset=False, lang=None, trsource=None, width=None, orientation=None, per_page=None, page=None, lat=None, lon=None, address=None, zipcode=None, city=None, match=None, publisher=None):
    cache_to_be_cleared = True
    images = db.session.query(Image).filter(
        Image.downloaded == 1).filter(Image.url is not None)
    # BLACKLISTS FILTER >>>
    blocked_authors = db.session.query(AuthorBlacklist.author)
    blocked_sources = db.session.query(SourceBlacklist.source)
    images = images.filter(Image.author.notin_(blocked_authors))
    images = images.filter(Image.source.notin_(blocked_sources))
        
    # CACHED IMAGES SEARCH 
    # TAGS SEARCH
    # Translate tags and perform tag filtering
    if tags:
        tags = [t.lower() for t in tags]
        applogger.debug("TAGS START")
        applogger.debug(['TAGS', tags])
        taglist = []
        fulltagslist = []
        
        for index, orig_tag in enumerate(tags):
            #orig_tag = orig_tag.lower()
            applogger.debug("INDEX {}".format(index))
            applogger.debug("TAGS 1")
            # SETTING DEFAULT TRANSLATION
            #if not all([lang, trsource]):
            #    lang = "de-en"
            #    trsource = "deepl"
            if lang and trsource:
                tag = translate_tag(trsource, lang, orig_tag)
                if not tag:
                    tag = orig_tag
            else:
                tag = orig_tag
            applogger.debug("TAGS 2")
                    
            tgs = set([tag, orig_tag])
                
            for tg in tgs:
                tagobj = db.session.query(Tag).filter(Tag.txt == tg).first()
                
                if tagobj:
                    taglist.append(tagobj)
                else:
                    newtag = Tag(txt=tg)
                    db.session.add(newtag)
                    db.session.commit()
                    taglist.append(newtag)
                    
            #tags[index] = tag
            fulltagslist.append(tag)
            if tag != orig_tag:
                fulltagslist.append(orig_tag)
            applogger.debug("TAGS {}".format(tags))
                
        tags = fulltagslist
        applogger.debug(["TAGS END", [t.txt for t in taglist]])

        applogger.debug("SEARCH DB TAGS {}".format(taglist))
        # Multiple AND
        # images_ = images.filter(*[Image.tags.contains(t) for t in taglist])
        images_ = images.join(img_tags).filter(img_tags.columns.tag_id.in_([t.id for t in taglist]))
        results_count = countresults(images_)
        applogger.debug(["TAG RESULTS COUNT", results_count])
        if match == 'exact':
            applogger.debug("EXACT TAG SEARCH")
        else:
            applogger.debug("ANY TAG SEARCH")

        if results_count == 0 and match == 'any':
            # Multiple OR
            images_ = images.filter(Image.tags.any(Tag.txt.in_(tags)))
        # tagsocount:
            
        images = images_
        # Image source filter
        if source:
            images = images.filter(Image.source == source)
    # GEO SEARCH HERE
    elif any([x is not None for x in [lat, lon, address, city, zipcode]]):
        # Either coordinate search or address search
        applogger.debug(f"GEO SEARCH lat: {lat}, lon: {lon}, address: {address}, city: {city}, zipcode: {zipcode}")
        if all([lat, lon]):
            if lat:
                images = images.filter(Image.location.has(Location.lat==str(lat)))
            if lon:
                images = images.filter(Image.location.has(Location.lon==str(lon)))
            applogger.debug("Found COORDS images {}".format(images.all()))
        else:
            address_filter = []
            if address:
                ilike_str = f"%{address}%"
                address_filter.append(Image.location.has(Location.addr.ilike(ilike_str)))
            if city:
                address_filter.append(Image.location.has(Location.city == city))
            if zipcode:
                address_filter.append(Image.location.has(Location.zipcode == zipcode))
            if any([address, city, zipcode]):
                applogger.debug("Address search")
                images = images.filter(and_(*address_filter))
                    

    if publisher:
        images = images.filter(Image.publisher == publisher)
                
    # GENERIC FILTERS: orientation, minwidth, limit
    # Image orientation filter
    if orientation:
        # Added for backward-compatibility
        if orientation == 'p':
            orientation = 'portrait'
        if orientation == 'l':
            orientation = 'landscape'
        images = images.filter(Image.orientation == orientation)
    # Image minimal width filter
    if width:
        images = images.filter(Image.width >= width)
    if not limit:
        limit = DEFAULT_LIMIT
    images = images.order_by(Image.score.desc()) 
    images_query = images
    images_query = images_query.limit(limit)
    if offset:
        images_query = images_query.offset(offset)            
    count_result = countresults(images)#.with_entities(func.count()).scalar()
    applogger.debug(f"COUNT {count_result}")
    images = images.limit(limit)#.all()
        
    # ONLINE SEARCH
    # Perform an online search if there's not enough cached results
    if count_result < limit:
        # clear_cache
        cache_to_be_cleared = True
        add_search_limit = limit - count_result#len(images)
        applogger.debug('Cached results: {}, images to download: {}'.format(count_result, add_search_limit))
        # perform an additional search and save results to the DB
        if tags:
            applogger.debug(f"TAGS additional search {tags}")
            # TODO add 'additional search = True' arg to the tags search
            # function to skip existing results
            imagetagsearch(source, tags, add_search_limit, width, orientation)
        # GEOSEARCH 
        elif all([lat is not None, lon is not None]):
            # perform coordinates search here
            # Flickr
            applogger.debug(f"COORDS additional search lat: {lat}, lon: {lon}")
            location = getlocation(lat=lat, lon=lon)
            totalfound = flickrsearch(limit, location=location)
                
            applogger.debug(f"FOUND: {totalfound}, requested: {limit}")
            if totalfound < limit:
                #applogger.debug(f"Additional google images location search")
                totalfound = googleplacessearch(limit=limit, location=location, width=width)

        elif any((address, zipcode, city)):
            # perform address search here
            # Google Places
            applogger.debug(f"ADDRESS additional search address: {address}, zipcode: {zipcode}, city: {city}")
            # Translate address to the coordinates before
            # performing location search
            location = getlocation(address=address, zipcode=zipcode, city=city)
            if location.lat and location.lon:
                totalfound = flickrsearch(limit, location=location)
                if totalfound == 0:
                    totalfound = googleplacessearch(limit=limit, location=location, width=width)
            # googleplacessearch(addr=address, zipcode=zipcode, city=city)
        # reprat the query after fetching additional images
        # to get results as a query set
        # to be passed to the prepare_results function
        db.session.commit()
        #db.session.refresh(images_query)
        
        images = images_query#.all()
        
    return images, cache_to_be_cleared


class TagsSchema(ma.ModelSchema):
    class Meta:
        model = Tag
        exclude=('images',)


class TagSchema(ma.ModelSchema):
    class Meta:
        model = Tag

        
class ArticlesSchema(ma.ModelSchema):
    class Meta:
        model = Article
        exclude=('images',)

        
class TagSchema(ma.ModelSchema):
    class Meta:
        model = Article

        
        
class LocationSchema(ma.ModelSchema):
    class Meta:
        model = Location

        
class AuthorBlacklistSchema(ma.ModelSchema):
    class Meta:
        model = AuthorBlacklist


class SourceBlacklistSchema(ma.ModelSchema):
    class Meta:
        model = SourceBlacklist
        

class ImageVersionSchema(ma.ModelSchema):
    class Meta:
        model = ImageVersion
    filter_url = ma.Function(lambda obj: request.host_url.replace("http://", "https://") + obj.filterpath)
    version_url = ma.Function(lambda obj: request.host_url.replace("http://", "https://") + obj.versionpath)

    
class ImageTagSchema(ma.ModelSchema):
    class Meta:
        model = Image
        exclude = ('path', 'status', 'tags', 'downloaded', 'imagecounter')
    tagstxt = ma.Function(lambda obj: [tag.txt for tag in obj.tags])
    hits = ma.Function(lambda obj: sum([cnt.counter if cnt.counter else 0 for cnt  in obj.imagecounter]))
    taghits = ma.Function(lambda obj: [{tag.txt: sum([cnt.counter if cnt.image_id==obj.id else 0 for cnt in tag.tagcounter ])} for tag in obj.tags])
    articleids = ma.Function(lambda obj: [article.storyid for article in obj.articles])
    imgcredits = ma.Function(lambda obj: obj.generated_credits)
    tags = ma.Nested('TagSchema', many=True, exclude=('images',))
    url = ma.Function(lambda obj: create_local_url(request.host, obj.path, request.path))
    orientation = EnumField(OrientationType)
    location = ma.Nested('LocationSchema', many=False, exclude=('images', "id"))
    versions = ma.Nested('ImageVersionSchema', many=True, exclude=('image', "filterpath", "versionpath"))


class AuthorBlacklistAPI(Resource):
    """
    Authors Blacklist API
    
    Allows to manipulate the authors blacklist
    """
    def __init__(self):
        self.authr_schema = AuthorBlacklistSchema()
        self.authrs_schema = AuthorBlacklistSchema(many=True)
        self.parser = reqparse.RequestParser()
        
    def get(self, id=None):
        """
        Get a list of blocked authors
        ---
        tags: [Blacklists,]
        definitions:
          Authors:
            type: array
            description: Blocked Authors list
            items:
              type: object
              properties:
                id:
                  type: integer
                  description: Author Record ID
                author:
                  type: string
                  description: Author name 
        responses:
          200:
            description: Blocked Authors list
            schema:
               $ref: '#/definitions/Authors'
        """
        
        authors = db.session.query(AuthorBlacklist).all()
        res = self.authrs_schema.dump(authors)
        return res.data, 200

    @jwt_required
    @cross_origin()
    def post(self):
        """
        Create new blocked author record
        ---
        tags: [Blacklists,]
        parameters:
          - name: author_name
            in: body
            required: true
        definitions:
          BlockedAuthorRecord:
            type: object
            properties:
              id:
                type: integer
                description: Blocked Author ID
              author:
                type: string
                description: Blocked Author name
        responses:
          201:
            description: New record created, returns new Blocked Author Record object
            schema:
               $ref: '#/definitions/BlockedAuthorRecord'
          400:
            description: Bad request, meaning that the wrong values were passed for some parameters.
          409:
            description: Blocked Author Record already exists
        """

        author_name = request.json.get('author_name', None)
        if not author_name:
            message = "No author name provided"
            return make_response(jsonify(message=message), 400)
        existing = db.session.query(AuthorBlacklist).filter(AuthorBlacklist.author==author_name).first()
        if not existing:
            newrecord = AuthorBlacklist(author=author_name)
            db.session.add(newrecord)
            db.session.commit()
            res = self.authr_schema.dump(newrecord)
            applogger.debug(['RESP', res])
            return jsonify(res.data), 201
        else:
            message = "Author has already been added to the blacklist"
            return make_response(jsonify(message=message), 409)

    @jwt_required
    @cross_origin()
    def delete(self):
        """
        Delete an author from the blacklist
        ---
        tags: [Blacklists,]
        parameters:
          - name: id
            in: body
            type: integer
            required: false
          - name: author_name
            in: body
            type: string
            required: false
        responses:
          204:
            description: Blocked Author Record successfully deleted
          400:
            description: Bad request, meaning that the wrong values were passed for some parameters.
          404:
            description: No records found for provided ID or author name
        """
        
        author_id = request.json.get('id', None)
        author_name = request.json.get('author_name', None)
        
        if not any([author_id, author_name]):
            message = "No ID or Author Name provided"
            return make_response(jsonify(message=message), 400)
        
        if all([author_id, author_name]):
            message = "Too many parameters provided"
            return make_response(jsonify(message=message), 400)

        author = db.session.query(AuthorBlacklist)

        if author_id:
            try:
                author_id = int(author_id)
            except:
                message = "Author ID should be numeric"
                return make_response(jsonify(message=message), 400)
            author = author.filter(AuthorBlacklist.id == author_id)
        elif author_name:
            author = author.filter(AuthorBlacklist.author == author_name)

        author_rec = author.first()
        if not author_rec:
            message = "No Records found"
            return make_response(jsonify(message=message), 404)
        
        db.session.delete(author_rec)
        db.session.commit()

        response = make_response('', 204)
        response.mimetype = current_app.config['JSONIFY_MIMETYPE']

        return response


class SourceBlacklistAPI(Resource):
    """
    Sources Blacklist API
    
    Allows to manipulate the sources blacklist
    """
    def __init__(self):
        self.src_schema = SourceBlacklistSchema()
        self.srcs_schema = SourceBlacklistSchema(many=True)
        self.parser = reqparse.RequestParser()
        
    def get(self, id=None):
        """
        Get a list of blocked sources
        ---
        tags: [Blacklists,]
        definitions:
          Sources:
            type: array
            description: Blocked Sources list
            items:
              type: object
              properties:
                id:
                  type: integer
                  description: Source Record ID
                source:
                  type: string
                  description: Source name 
        responses:
          200:
            description: Blocked Sources list
            schema:
               $ref: '#/definitions/Sources'
        """
        
        sources = db.session.query(SourceBlacklist).all()
        res = self.srcs_schema.dump(sources)
        return res.data, 200

    @jwt_required
    @cross_origin()
    def post(self):
        """
        Create new blocked source record. Available values are: pexels, pixabay, cloudinary, flickrtags, imgur, unsplash
        ---
        tags: [Blacklists,]
        parameters:
          - name: source_name
            enum: [pexels, pixabay, cloudinary, flickrtags, imgur, unsplash]
            in: body
            required: true
        definitions:
          BlockedSourceRecord:
            type: object
            properties:
              id:
                type: integer
                description: Blocked Source ID
              author:
                type: string
                description: Blocked Source name
        responses:
          201:
            description: New record created, returns new Blocked Source Record object
            schema:
               $ref: '#/definitions/BlockedSourceRecord'
          400:
            description: Bad request, meaning that the wrong values were passed for some parameters.
          409:
            description: Blocked Source Record already exists
        """
        
        source_name = request.json.get('source_name', None)
        
        if not source_name:
            message = "No source name provided"
            return make_response(jsonify(message=message), 400)

        if source_name not in ["pexels", "pixabay", "cloudinary", "flickrtags", "imgur", "unsplash"]:
            message = "Wrong source name. Available values are: pexels, pixabay, cloudinary, flickrtags, imgur, unsplash"
            return make_response(jsonify(message=message), 400)
        
        existing = db.session.query(SourceBlacklist).filter(SourceBlacklist.source==source_name).first()
        if not existing:
            newrecord = SourceBlacklist(source=source_name)
            db.session.add(newrecord)
            db.session.commit()
            res = self.src_schema.dump(newrecord)
            return jsonify(res.data), 201
        else:
            message = "Source has already been added to the blacklist"
            return make_response(jsonify(message=message), 409)

    @jwt_required
    @cross_origin()
    def delete(self):
        """
        Delete a source from the blacklist
        ---
        tags: [Blacklists,]
        parameters:
          - name: id
            in: body
            type: integer
            required: false
          - name: source_name
            in: body
            type: string
            enum: [pexels, pixabay, cloudinary, flickrtags, imgur, unsplash]
            required: false
        responses:
          204:
            description: Blocked Source Record successfully deleted
          400:
            description: Bad request, meaning that the wrong values were passed for some parameters.
          404:
            description: No records found for provided ID or source name
        """
        
        source_id = request.json.get('id', None)
        source_name = request.json.get('source_name', None)
        
        if not any([source_id, source_name]):
            message = "No ID or Source Name provided"
            return make_response(jsonify(message=message), 400)
        
        if all([source_id, source_name]):
            message = "Too many parameters provided"
            return make_response(jsonify(message=message), 400)

        source = db.session.query(SourceBlacklist)

        if source_id:
            try:
                source_id = int(source_id)
            except:
                message = "Source ID should be numeric"
                return make_response(jsonify(message=message), 400)
            source = source.filter(SourceBlacklist.id == source_id)
        elif source_name:
            source = source.filter(SourceBlacklist.source == source_name)

        source_rec = source.first()
        if not source_rec:
            message = "No Records found"
            return make_response(jsonify(message=message), 404)
        
        db.session.delete(source_rec)
        db.session.commit()

        response = make_response('', 204)
        response.mimetype = current_app.config['JSONIFY_MIMETYPE']

        return response

    
class TagsBaseAPI(Resource):
    """
    Image Tags API
    
    Allows to search for existing tags & get related images
    """
    def __init__(self):
        self.tagschema = TagSchema()
        self.tagsschema = TagsSchema(many=True)
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('search', type=str)
        self.parser.add_argument('page', type=int, required=False,
                                 default=1, help="Results page parameter not provided")
        self.parser.add_argument('per_page', type=int, required=False,
                                 default=5, help="Results per page parameter not provided")
        
    def prepare_results(self, results, page=1, per_page=PER_PAGE, url=None):
        """
        Prepare results before return, default implementation
        """
        applogger.debug("PREP RES DEF")
        return self.tagsschema.dump(results).data
        

class TagsGETAPI(TagsBaseAPI):
    #@cache.cached(timeout=60, key_prefix=cache_key)
    def get(self, id=None):
        """
        Get image tags. Returns a list of available tags 
        ---
        tags: [Tags,]
        parameters:
         - in: query
           name: search
           type: string
           required: true
           description: tags to search
        definitions:
          TagsList:
            type: array
            description: Tags list
            items:
              type: object
              properties:
                id:
                  type: integer
                  description: Tag ID
                txt:
                  type: string
                  description: Tag text
        responses:
          200:
            description: Tag json data
            schema:
               $ref: '#/definitions/TagsList'
          400:
            description: Bad request, meaning that the wrong values were passed for some parameters.
        """
        if not id:
            try:
                args = request.args#self.parser.parse_args()
            except:
                args = {'page': 1, 'per_page': PER_PAGE}

            try:
                search = args.get('search', None)
            except:
                search = None
            if not search:
                message = "No search param provided"
                return make_response(jsonify(message=message), 400)

            per_page = args.get('per_page', None)
            page = args.get('page', None)
            # try:
                
            if not per_page:
                per_page = PER_PAGE
            if not page:
                page = 1
            if not search:
                message = 'No search parameters provided'
                return make_response(jsonify(message=message), 400)
            if page:
                try:
                    page = int(page)
                except:
                    message = f"page should be numeric"
                    return make_response(jsonify(message=message), 400)
            if per_page:
                try:
                    per_page = int(per_page)
                except:
                    message = f"per_page should be numeric"
                    return make_response(jsonify(message=message), 400)
                
            searchstr = f"{search}%"
            tags = db.session.query(Tag).filter(Tag.txt.ilike(searchstr))
            #if tags.count() > 0:
            res = self.prepare_results(tags, page=page, per_page=per_page, url=request.url)
            return res, 200
            #else:
            #    return [], 200
        else:
            #if not str(id).isnumeric():
            #
            try:
                id = int(id)
            #if not isinstance(id, (int, float, complex)):
            except:
                message = f"Tag ID should be numeric"
                return make_response(jsonify(message=message), 400)
                
            tag = db.session.query(Tag).filter(Tag.id == id).first()
            
            if tag:
                res = self.tagschema.dump(tag)
                return res.data, 200
            else:
                message = f'No results found for tag id {id}'
                return make_response(jsonify(message=message), 404)

            
# Single tag V1
class SingleTagV1API(TagsGETAPI):
        
    def _get(self, id):
        return TagsGETAPI.get(self, id)

    def get(self, id):
        """
        Get a single tag by ID
        ---
        tags: [Tags,]
        parameters:
         - in: path
           name: id
           type: integer
           required: true
           description: Tag ID
        definitions:
          SingleTag:
            type: object
            properties:
              id:
                type: integer
                description: Tag ID
              txt:
                type: string
                description: Tag text
              images:
                type: array
                description: A list of related image IDs for a single tag
                items:
                  type: integer
                  description: Image ID
        responses:
          200:
            description: Tag json data
            schema:
               $ref: '#/definitions/SingleTag'
          400:
            description: Bad request, meaning that the wrong values were passed for some parameters.
          404:
            description: No results found
        """
        return self._get(id)
    

         
class TagsV1API(TagsGETAPI):

    def prepare_results(self, results, page=1, per_page=PER_PAGE, url=None):
        applogger.debug("PREP RES V1")
        return self.tagsschema.dump(results).data

        
class TagsV2API(TagsGETAPI):
        
    def _get(self):
        return TagsGETAPI.get(self)

    def get(self):
        """
        Get image tags. Returns a list of available tags
        ---
        tags: [Tags,]
        parameters:
         - in: query
           name: search
           type: string
           required: true
           description: tags to search
         - in: query
           name: page
           type: integer
           required: false
           description: results page
         - in: query
           name: per_page
           type: integer
           required: false
           description: results per page
        definitions:
          TagsData:
            type: object
            properties:
              total:
                type: integer
                description: Total results number
              per_page:
                type: integer
                description: Number of results per page
              current_page:
                type: integer
                description: Current page number
              last_page:
                type: integer
                description: Last page number
              next_page_url:
                type: string
                description: Next page URL
              prev_page_url:
                type: string
                description: Previous page URL
              data:
                type: array
                description: Results list
                items:
                  type: object
                  properties:
                    id:
                      type: integer
                      description: Tag ID
                    txt:
                      type: string
                      description: Tag text
        responses:
          200:
            description: Tag json data
            schema:
               $ref: '#/definitions/TagsData'
          400:
            description: Bad request, meaning that the wrong values were passed for some parameters.
        """
        return self._get()

    def prepare_results(self, results, page=1, per_page=PER_PAGE, url=None):
        """
        API v2 results formatting
        """
        applogger.debug("PREP RES 2")
        prev_url = None
        next_url = None
        paginated = results.from_self().paginate(page, per_page, error_out=False)
        tags_on_page = paginated.items
        total = paginated.total
        last_page = paginated.pages
        next_page = paginated.next_num
        prev_page = paginated.prev_num
        data = self.tagsschema.dump(tags_on_page).data
        if url:
            parsed = urlparse(url)
            urlpath = parsed.path
            base_url = urljoin(request.host_url, urlpath) + "?"
            base_url = base_url.replace("http://", "https://")
            params = parse_qs(parsed.query)
            if next_page:
                params['page'] = next_page
                next_url =  base_url + urlencode(params, doseq=True)
            if prev_page:
                params['page'] = prev_page
                prev_url = base_url + urlencode(params, doseq=True)

        results = {'data': data,
                   'total': total,
                   'per_page': per_page,
                   'current_page': page,
                   'last_page': last_page,
                   'next_page_url': next_url,
                   'prev_page_url': prev_url
        }
        return results

    
class SingleTagV2API(TagsV2API):
    def _get(self, id):
        return TagsGETAPI.get(self, id)

    def get(self, id):
        """
        Get a single tag by ID
        ---
        tags: [Tags,]
        parameters:
         - in: path
           name: id
           type: integer
           required: true
           description: Tag ID
        definitions:
          Tag:
            type: object
            properties:
              id:
                type: integer
                description: Tag ID
              txt:
                type: string
                description: Tag text
              images:
                type: array
                description: A list of related image IDs for a single tag
                items:
                  type: integer
                  description: Image ID
        responses:
          200:
            description: Tag json data
            schema:
               $ref: '#/definitions/Tag'
          400:
            description: Bad request, meaning that the wrong values were passed for some parameters.
          404:
            description: No results found
        """
        return self._get(id)

    
class TagsPostAPI(TagsBaseAPI):
    
    def post(self):
        """
        Create new tag
        ---
        tags: [Tags,]
        parameters:
          - name: txt
            in: body
            required: true
        definitions:
          NewTag:
            type: object
            properties:
              id:
                type: integer
                description: Tag ID
              txt:
                type: string
                description: Tag text
        responses:
          201:
            description: Tag created, returns new Tag object
            schema:
               $ref: '#/definitions/NewTag'
          400:
            description: Bad request, meaning that the wrong values were passed for some parameters.
          409:
            description: Tag already exists
        """
        txt = request.json.get('txt', None)
        
        if not txt:
            message = "No tag text provided"
            return make_response(jsonify(message=message), 400)

        existing = db.session.query(Tag).filter(Tag.txt==txt).first()
        if not existing:
            newtag = Tag(txt=txt)
            db.session.add(newtag)
            db.session.commit()
            # clear_cache
            clear_cache("tags")
            res = self.tagschema.dump(newtag)
            return res.data, 201
        else:
            message = "Tag already exists"
            return make_response(jsonify(message=message), 409)


class ImageDeleteAPI(Resource):
    """
    Image Delete API
    """
    
    @jwt_required
    @cross_origin()
    def delete(self):
        """
        ImageDelete API

        Allows an authorized user to delete an image by it's URL, source URL or ID
        ---
        tags: [Images,]
        parameters:
          - name: id
            in: body
            type: integer
            required: false
          - name: url
            in: body
            type: string
            required: false
          - name: source
            in: body
            type: string
            required: false
        responses:
          204:
            description: Image successfully deleted
          400:
            description: Bad request, meaning that the wrong values were passed for some parameters.
          404:
            description: No image found for provided URL or source
        """
        
        image_url = request.json.get('url', None)
        image_source = request.json.get('source', None)
        image_id = request.json.get('id', None)
        minioClient = create_minio_client()
        
        applogger.debug(["json", image_url, image_source])
        if all([image_url, image_source, image_id]):
            message = "Too many parameters specified, provide one of the parameters, Image URL, Image Source, Image ID"
            return make_response(jsonify(message=message), 400)
        
        if not any([image_url, image_source, image_id]):
            message = "No Image URL, Image Source or Image ID provided"
            return make_response(jsonify(message=message), 400)

        db_image = db.session.query(Image)
        
        if image_url:
           image_source = None
           # Extract iamge path from the url
           parsedurl = urlparse(image_url)
           image_path = parsedurl.path
           VERSION = find_version(image_path)
           apistr = f"/api/{VERSION}/"
           image_path = image_path.replace(apistr, "")
           db_image = db_image.filter(Image.path == image_path)
        elif image_source:
            applogger.debug(["Image Source", image_source])
            db_image = db_image.filter(Image.source_url == image_source)
        elif image_id:
            applogger.debug(["Image ID", image_source])
            db_image = db_image.filter(Image.id == image_id)
            
        db_image = db_image.first()
        if not db_image:
            message = "No Image found"
            return make_response(jsonify(message=message), 404)
            
        try:
            resp = minioClient.stat_object(MINIO_BUCKET_NAME, db_image.path)
            minioClient.remove_object(MINIO_BUCKET_NAME, db_image.path)
            applogger.debug(["Minio Image deleted", resp])
        except:# NoSuchKey:
            applogger.debug(["Minio Image",  "not found"])
        
        applogger.debug(["Image to delete", db_image])

        if db_image.versions:
            for v in db_image.versions:
                try:
                    applogger.debug(["Image version", v.versionpath])
                    resp = minioClient.stat_object(MINIO_BUCKET_NAME, os.path.join(IMGPATH, v.filterpath))
                    minioClient.remove_object(MINIO_BUCKET_NAME, os.path.join(IMGPATH, v.filterpath))
                    minioClient.remove_object(MINIO_BUCKET_NAME, os.path.join(IMGPATH, v.versionpath))
                    applogger.debug(["Minio Version Image deleted"])
                except:# NoSuchKey:
                    applogger.debug(["Minio Version Image",  "not found"])
                db.session.delete(v)
                
        cache.delete("/api/v2/{}?".format(db_image.path))
        cache.delete("/api/v1/{}?".format(db_image.path))
        
        clear_cache(db_image.path, exact_match=True)
        db.session.delete(db_image)
        db.session.commit()

        response = make_response('', 204)
        response.mimetype = current_app.config['JSONIFY_MIMETYPE']
        applogger.debug(["Image deleted", "ImageDeleteAPI"])
        return response

    
class ImageUploadAPI(Resource):
    """
    Image Upload API
    """
    def __init__(self):
        self.imageschema = ImageTagSchema()
        self.imagesschema = ImageTagSchema(many=True)

    
    def upload_image(self, method=None):
        if method == "POST":
            auth_headers = request.headers.get('Authorization', '').split()
            token = auth_headers[1]
            current_user = get_jwt_identity()
            
        elif method == "GET":
            token = request.values.get('token', False)
            #try:
            token_data = decode_token(token, allow_expired=True)
            applogger.debug(token_data)
            current_user = token_data['identity']#get_jwt_identity()
            #data = jwt.decode(token, current_app.config['SECRET_KEY'], options={'verify_exp': True})
            #except jwt.exceptions.InvalidSignatureError:
            #    fabort(401, "Not authorized")
            #except jwt.exceptions.ExpiredSignatureError:
            #    fabort(401, "Not authorized")

            #if not token:
            #    fabort(401, "Not authorized")
                
        #data = jwt.decode(token, current_app.config['SECRET_KEY'], options={'verify_exp': False})

        applogger.debug(current_user)
        if current_user:
            user = User.query.filter_by(login=current_user).first()
        else:
            fabort(401, "Not authorized")
        source = f"{user.login}"

        img_src = None
        imgwidth = None
        imgorientation = None
        facetags = None
        face_file = None
        applogger.debug(request.values)
        thumb_width = request.values.get('width', None)
        thumb_height = request.values.get('height', None)
        return_data = request.values.get('data', 'json')
        fltrstring = None
        descr = request.values.get('descr', None)
        license = request.values.get('license', None)
        publisher = request.values.get('publisher', None)
        lat = request.values.get('lat', None)
        lon = request.values.get('lon', None)
        detect_faces = request.values.get('detect_faces', False)
        filter_type = request.values.get('ftype', 'resize')
        image_url = request.values.get('image_url', False)
        color = request.values.get('color', 'FFFFFF')
        
        if detect_faces and detect_faces == 'false':
            detect_faces = False
            
        if any((lat, lon)):
            if not all((lat, lon)):
                message='Not enoung arguments for location search. Please provide lat & lon parameters'
                return make_response(jsonify(message=message), 400)
            
            if not -180 <= float(lat) <= 180 or not -180 <= float(lon) <= 180:
                message='Coordinates values should be between -180 and 180'
                return make_response(jsonify(message=message), 400)
            location = getlocation(lat=lat, lon=lon)
        else:
            location = None
            
        if location:
            location_query = db.session.query(Location)
            if location.lat:
                location_query = location_query.filter(Location.lat == str(location.lat))
            if location.lon:
                location_query = location_query.filter(Location.lon == str(location.lon))
            if location.address:
                location_query = location_query.filter(Location.addr == location.address)
            if location.zipcode:
                location_query = location_query.filter(Location.zipcode == location.zipcode)
            if location.city:
                location_query = location_query.filter(Location.city == location.city)
            db_location = location_query.first()

            if not db_location:
                db_location = Location(lat=str(location.lat),
                                       lon=str(location.lon),
                                       addr=location.address,
                                       zipcode=location.zipcode,
                                       city=location.city
                )
            db.session.add(db_location)
            db.session.commit()
            applogger.debug("Adding new location {}".format(db_location))
            location_id = db_location.id
        else:
            location_id = None    
                        
        applogger.debug(["THUMB DIMS", thumb_width, thumb_height])
        
        if any([thumb_width, thumb_height]) and not filter_type == 'padding':
            if thumb_width and thumb_height:
                fltrstring = f"f_scale-w_{thumb_width}-h_{thumb_height}"
            elif thumb_width and not thumb_height:
                fltrstring = f"f_scale-w_{thumb_width}"
            elif thumb_height and not thumb_width:
                fltrstring = f"f_scale-h_{thumb_height}"
        elif filter_type == 'padding':
            fltrstring = f"f_padding-w_{thumb_width}-h_{thumb_height}-c_{color}"
            color = get_color(fltrstring)
        applogger.debug(["FILTER", fltrstring])

        tags = request.values.get('tags', None)
        applogger.debug(['TAGS', tags])
        
        # Auto face tags
        # get the uploaded file and send it to the FD API with returndata = match
        #
        if not image_url and request.files:
            face_file = [request.files.get(f) for f in request.files]
            
            if not face_file:
                return make_response(jsonify(message="No file provided"), 400)
            else:
                face_file = face_file[-1]
            if detect_faces:    
                with tempfile.TemporaryDirectory() as tmpdname:
                    tmp_upl_file = os.path.join(tmpdname, face_file.filename)
                    buf = BytesIO(face_file.read())
                    face_file.seek(0)
                    face_file.save(tmp_upl_file)
                    # Move to background
                    buf_str = base64.b64encode(buf.read()).decode('ascii')
                    #print(["BUF_str", buf_str])
                    #facetags = face_detect.run(None, buf_str,  "tags")
                    facetags = face_detect.run(None, buf_str,  "tags")
                    
                    #facetags = face_detect(tmp_upl_file, "tags")
                    #if facetags:
                    #    facetags = facetags.capitalize()
                    
                    face_file.stream.seek(0)
        
            
        dbtags = []
        if tags:
           tags = tags.split(",")
           tags = [t.strip() for t in tags]
           tags = list(set(tags))
           # auto face tags
           if facetags:
               tags.append(facetags)
        else:
            if facetags:
               tags = [facetags,]
               
        if tags:
           for tag in tags:
              prevtag = db.session.query(Tag).filter(Tag.txt == tag).first()
              if not prevtag:
                 newtag = Tag(txt=tag)
                 db.session.add(newtag)
                 db.session.commit()
                 dbtags.append(newtag.id)
              else:
                 dbtags.append(prevtag.id)

        location = None
        source_url = None
        imgcredits = None
        if not request.files and not image_url:
            message='No file or image URL provided'
            return make_response(jsonify(message=message), 400)
        # If uploading a file: 
        if face_file and not image_url:
            uploaded_file = face_file
            flpath, fileext = os.path.splitext(uploaded_file.filename)
        
            out_queue = queue.Queue()
            with tempfile.TemporaryDirectory() as tmpdirname:
                if fileext.upper() == ".PDF":
                    tmp_pdf_file = os.path.join(tmpdirname, flpath+".pdf")
                    tmp_img_file = os.path.join(tmpdirname, flpath+".png")
                    
                    uploaded_file.save(tmp_pdf_file)
                    # PDF Thumbnail only for the first page
                    pdfimage = WImage(filename=tmp_pdf_file+"[0]")
                    pdfimage.convert('png')
                    if any([thumb_width, thumb_height]):
                        dim = get_dimensions(fltrstring, getfloat=True)
                        pdfimage = fast_resize(pdfimage, dim)
                    pdfimage.strip()
                    pdfimage.save(filename=tmp_img_file)
                else:
                    tmp_img_file = os.path.join(tmpdirname, uploaded_file.filename)
                    uploaded_file.save(tmp_img_file)
                    uploaded_file = open(tmp_img_file, 'rb')
                    uploaded_file.filename = tmp_img_file
                    if any([thumb_width, thumb_height]):
                        dim = get_dimensions(fltrstring, getfloat=True, short=False)
                        applogger.debug(["DIMS", dim])
                        wimage = Img.open(tmp_img_file) #WImage(filename=tmp_img_file)
                        if filter_type == 'resize':
                            wimage = apply_resize(wimage, dim)
                        elif filter_type == 'crop':
                            if detect_faces:
                                buf = BytesIO(uploaded_file.read())
                                # Move to background
                                buf_str = base64.b64encode(buf.read()).decode('ascii')
                                #print(["BUF_STR", buf_str])
                                faces = face_detect.run(None, buf_str,  "faces")
                                uploaded_file.seek(0)
                                #faces = face_detect(tmp_img_file, "faces")
                                wimage = apply_crop(wimage, dim, gravity="faces", faces=faces)
                            else:
                                wimage = apply_crop(wimage, dim, gravity="center")
                        elif filter_type == 'padding':
                            wimage = apply_padding(wimage, dim, color)
                        wimage.save(tmp_img_file)#wimage.save(filename=tmp_img_file)

                uploaded_file = open(tmp_img_file, 'rb')
                uploaded_file.filename = tmp_img_file
                
                image_id, existing = savefile(dbtags, source, uploaded_file, detect_faces, location_id, out_queue, license, publisher, descr)
            
        else:
            applogger.debug(["IMAGE URL", image_url])
            out_queue = queue.Queue()
            r = requests.get(image_url, stream=True)
            applogger.debug(["STATUS CODE", r.status_code])
            if r.status_code == 200:
                with tempfile.NamedTemporaryFile() as f:
                    for chunk in r.iter_content(1024):
                        f.write(chunk)
                    img_mime_type = magic.from_file(f.name, mime=True)
                    applogger.debug(["IMAGE MIME", img_mime_type])
                    f.seek(0)
                    if any([thumb_width, thumb_height]):
                        dim = get_dimensions(fltrstring, getfloat=True, short=False)
                    else:
                        dim = None
                    pil_img = Img.open(f.name)
                    if filter_type == 'resize' and dim:
                        pil_img = apply_resize(pil_img, dim)
                    elif filter_type == 'crop':
                        if detect_faces:
                            buf = BytesIO(f.read())
                            # Move to background
                            buf_str = base64.b64encode(buf.read()).decode('ascii')
                            #print(["BUF_STR", buf_str])
                            faces = face_detect.run(None, buf_str,  "faces")
                            f.seek(0)
                            #faces = face_detect(f.name, "faces")
                            pil_img = apply_crop(pil_img, dim, gravity="faces", faces=faces)
                        else:
                            pil_img = apply_crop(pil_img, dim, gravity="center")
                    elif filter_type == 'padding':
                        pil_img = apply_padding(pil_img, dim, color)
                        
                    extension = img_mime_type.split("/")[-1]
                    newfname = f"{f.name}.{extension}"
                    pil_img.save(newfname)#wimage.save(filename=tmp_img_file)
                        
                    uploaded_file = open(newfname, 'rb')
                    uploaded_file.filename = newfname
                    image_id, existing = savefile(dbtags, source, uploaded_file, detect_faces, location_id, out_queue, license, publisher, descr, source_url=image_url)
                    applogger.debug(["OUT IMAGE ID", image_id])
            #image_id, existing = saveurl(dbtags, source, image_url, detect_faces, location_id, license, publisher, descr, fltrstring)

        db.session.commit()
        image = db.session.query(Image).filter(Image.id == image_id).first()
            
        if return_data == 'raw':
            if request.files:
                img_mime_type = mimetypes.guess_type(uploaded_file.filename)[0]
                return send_file(uploaded_file, mimetype=img_mime_type)
            else:
                if image.source == 'minio':
                    applogger.debug(["SERVE URL", "minio"])
                    redirect_url = create_minio_url(image.path, imgsource=source)
                else:
                    redirect_url = create_minio_url(image.path)
                applogger.debug(["SERVE URL", redirect_url, source])
                return serve_url(redirect_url)

        #elif return_data == 'json':
            #st.join()
        #    image_id, existing = out_queue.get()
        
        applogger.debug(["IMAGE ID", image_id])        
        applogger.debug(["IMAGE ", image.path, image.url, image.tagstxt])
        applogger.debug(["IMAGE EXISTING", existing])        
        
            
        res = self.imageschema.dump(image).data
        
        if not existing:
           # clear_cache
           applogger.debug(["CLEAR CACHE", request.full_path])
           #clear_cache("images")
           return jsonify(res), 201
        else:
           message='File is already uploaded'
           return make_response(jsonify(message=message, data=res), 409)


    #@jwt_required
    @cross_origin()
    def get(self):
        """
        Upload custom images with tags. Supports URLs upload only
        ---
        tags: [Images,]
        parameters:
         - in: path
           name: tags
           type: string
           required: false
           description: A list of comma-separated image tags to link with the image
         - in: path
           name: width
           type: integer
           required: false
           description: Image width
         - in: path
           name: height
           type: integer
           required: false
           description: Image height
         - in: path
           name: data
           type: string
           enum: ['raw', 'json']
           required: false
           default: "json"
           description: Output data type. To get the raw image the "raw" value should be passed, to get the json representation the "json" value should be provided 
         - in: path
           name: detect_faces
           type: string
           required: false
           description: Should the API detect the faces on the image
         - in: path
           name: ftype
           type: string
           enum: ['crop', 'resize']
           required: false
           default: "resize"
           description: Which filter to apply, crop or resize
         - in: path
           name: license
           type: string
           required: false
           description: Image license
         - in: path
           name: publisher
           type: string
           required: false
           description: Image publisher
         - in: path
           name: descr
           type: string
           required: false
           description: Image description
         - in: path
           name: lat
           type: float
           required: false
           description: location lattitude
         - in: path
           name: lon
           type: float
           required: false
           description: location longitude
         - in: path
           name: image_url
           type: string
           required: false
           description: Image URL to upload.
        definitions:
          DuplicateImage:
            type: object
            properties:
              data:
                type: object
                properties:
                  id:
                    type: integer
                    description: Image ID
                  orientation:
                    type: string
                    description: Image orientation,\
            portrait or landscape
                  url:
                    type: string
                    description: Image URL
                  width:
                    type: integer
                    description: Image width
                  height:
                    type: integer
                    description: Image height
                  score:
                    type: integer
                    description: Image score for custom ordering
                  source:
                    type: string
                    description: Image source
                  license:
                    type: string
                    description: Image license
                  publisher:
                    type: string
                    description: Image publisher
                  descr:
                    type: string
                    description: Image description
                  tagstxt:
                    type: array
                    description: A list of related tags
                    items:
                      type: string
                      description: A single tag
                  versions:
                    type: array
                    description: A list of image versions
                    items:
                    type: object
                    properties:
                      version:
                        type: integer
                        description: Version number
                      filter_url:
                        type: string
                        description: Filter-based image URL
                      version_url:
                        type: string
                        description: Version-based image URL
                  location:
                    type: object
                    properties:
                      addr:
                        type: string
                        description: Location address
                      city:
                        type: string
                        description: Location city
                      zipcode:
                        type: string
                        description: Location zipcode
                      lat:
                        type: string
                        description: Location lattitude
                      lon:
                        type: string
                        description: Location longitude
              message:
                type: string
                description: Error message
          JsonImage:
            type: object
            properties:
              id:
                type: integer
                description: Image ID
              orientation:
                type: string
                description: Image orientation,\
        portrait or landscape
              url:
                type: string
                description: Image URL
              width:
                type: integer
                description: Image width
              height:
                type: integer
                description: Image height
              score:
                type: integer
                description: Image score for custom ordering
              source:
                type: string
                description: Image source
              tagstxt:
                type: array
                description: A list of related tags
                items:
                  type: string
                  description: A single tag
              versions:
                type: array
                description: A list of image versions
                items:
                type: object
                properties:
                  version:
                    type: integer
                    description: Version number
                  filter_url:
                    type: string
                    description: Filter-based image URL
                  version_url:
                    type: string
                    description: Version-based image URL
              location:
                type: object
                properties:
                  addr:
                    type: string
                    description: Location address
                  city:
                    type: string
                    description: Location city
                  zipcode:
                    type: string
                    description: Location zipcode
                  lat:
                    type: string
                    description: Location lattitude
                  lon:
                    type: string
                    description: Location longitude
        responses:
         201:
           description: New image uploaded
           schema:
              $ref: '#/definitions/JsonImage'
         400:
           description: Bad request, meaning that the wrong values were passed for some parameters.
         409:
           description: File has been already uploaded
           schema:
              $ref: '#/definitions/DuplicateImage'
        """
        
        resp = self.upload_image(method="GET")
        return resp
        
       

    @jwt_required
    @cross_origin()
    def post(self):
        """
        Upload custom images with tags. Supports both files & URLs
        
        ---
        tags: [Images,]
        parameters:
         - in: formData
           name: file
           type: file
           required: false
           description: Image file to upload. Required if no URL provided
         - in: formData
           name: tags
           type: string
           required: false
           description: A list of comma-separated image tags to link with the image
         - in: formData
           name: width
           type: integer
           required: false
           description: Image width
         - in: formData
           name: height
           type: integer
           required: false
           description: Image height
         - in: formData
           name: data
           type: string
           enum: ['raw', 'json']
           required: false
           default: "json"
           description: Output data type. To get the raw image the "raw" value should be passed, to get the json representation the "json" value should be provided 
         - in: formData
           name: detect_faces
           type: string
           required: false
           description: Should the API detect the faces on the image
         - in: formData
           name: ftype
           type: string
           enum: ['crop', 'resize']
           required: false
           default: "resize"
           description: Which filter to apply, crop or resize
         - in: formData
           name: license
           type: string
           required: false
           description: Image license
         - in: formData
           name: publisher
           type: string
           required: false
           description: Image publisher
         - in: formData
           name: descr
           type: string
           required: false
           description: Image description
         - in: formData
           name: lat
           type: float
           required: false
           description: location lattitude
         - in: formData
           name: lon
           type: float
           required: false
           description: location longitude
         - in: formData
           name: image_url
           type: string
           required: false
           description: Image Direct URL. If URL is provided, filename is not required
        definitions:
          DuplicateImage:
            type: object
            properties:
              data:
                type: object
                properties:
                  id:
                    type: integer
                    description: Image ID
                  orientation:
                    type: string
                    description: Image orientation,\
            portrait or landscape
                  url:
                    type: string
                    description: Image URL
                  width:
                    type: integer
                    description: Image width
                  height:
                    type: integer
                    description: Image height
                  score:
                    type: integer
                    description: Image score for custom ordering
                  source:
                    type: string
                    description: Image source
                  license:
                    type: string
                    description: Image license
                  publisher:
                    type: string
                    description: Image publisher
                  descr:
                    type: string
                    description: Image description
                  tagstxt:
                    type: array
                    description: A list of related tags
                    items:
                      type: string
                      description: A single tag
                  versions:
                    type: array
                    description: A list of image versions
                    items:
                    type: object
                    properties:
                      version:
                        type: integer
                        description: Version number
                      filter_url:
                        type: string
                        description: Filter-based image URL
                      version_url:
                        type: string
                        description: Version-based image URL
                  location:
                    type: object
                    properties:
                      addr:
                        type: string
                        description: Location address
                      city:
                        type: string
                        description: Location city
                      zipcode:
                        type: string
                        description: Location zipcode
                      lat:
                        type: string
                        description: Location lattitude
                      lon:
                        type: string
                        description: Location longitude
              message:
                type: string
                description: Error message
          JsonImage:
            type: object
            properties:
              id:
                type: integer
                description: Image ID
              orientation:
                type: string
                description: Image orientation,\
        portrait or landscape
              url:
                type: string
                description: Image URL
              width:
                type: integer
                description: Image width
              height:
                type: integer
                description: Image height
              score:
                type: integer
                description: Image score for custom ordering
              source:
                type: string
                description: Image source
              tagstxt:
                type: array
                description: A list of related tags
                items:
                  type: string
                  description: A single tag
              versions:
                type: array
                description: A list of image versions
                items:
                type: object
                properties:
                  version:
                    type: integer
                    description: Version number
                  filter_url:
                    type: string
                    description: Filter-based image URL
                  version_url:
                    type: string
                    description: Version-based image URL
              location:
                type: object
                properties:
                  addr:
                    type: string
                    description: Location address
                  city:
                    type: string
                    description: Location city
                  zipcode:
                    type: string
                    description: Location zipcode
                  lat:
                    type: string
                    description: Location lattitude
                  lon:
                    type: string
                    description: Location longitude
        responses:
         201:
           description: New image uploaded
           schema:
              $ref: '#/definitions/JsonImage'
         400:
           description: Bad request, meaning that the wrong values were passed for some parameters.
         409:
           description: File has been already uploaded
           schema:
              $ref: '#/definitions/DuplicateImage'
        """
        
        resp = self.upload_image(method="POST")
        return resp

        
class ImageVoteAPI(Resource):
    """
    Image Vote API
    """
    def __init__(self):
        self.imageschema = ImageTagSchema()
        self.imagesschema = ImageTagSchema(many=True)

    @jwt_required
    @cross_origin()
    def patch(self, id):
        """
        Update image data. Update image data. One of parameters, vote or tags, should be provided.

        ---
        tags: [Images,]
        parameters:
         - in: path
           name: id
           type: integer
           required: true
           description: Image ID
         - in: body
           name: vote
           type: string
           enum: [up, down]
           required: false
           description: Image upvote/downvote endpoint for custom sorting of search results. 
         - in: body
           name: tags
           type: array
           required: false
           description: A list of image tags to link with the image
           items:
             type: string
             description: Tag text
        definitions:
          JsonImage:
            type: object
            properties:
              id:
                type: integer
                description: Image ID
              orientation:
                type: string
                description: Image orientation,\
        portrait or landscape
              url:
                type: string
                description: Image URL
              width:
                type: integer
                description: Image width
              height:
                type: integer
                description: Image height
              score:
                type: integer
                description: Image score for custom ordering
              source:
                type: string
                description: Image source
              tagstxt:
                type: array
                description: A list of related tags
                items:
                  type: string
                  description: A single tag
              versions:
                type: array
                description: A list of image versions
                items:
                type: object
                properties:
                  version:
                    type: integer
                    description: Version number
                  filter_url:
                    type: string
                    description: Filter-based image URL
                  version_url:
                    type: string
                    description: Version-based image URL
              location:
                type: object
                properties:
                  addr:
                    type: string
                    description: Location address
                  city:
                    type: string
                    description: Location city
                  zipcode:
                    type: string
                    description: Location zipcode
                  lat:
                    type: string
                    description: Location lattitude
                  lon:
                    type: string
                    description: Location longitude
        responses:
         200:
           description: Image json data
           schema:
              $ref: '#/definitions/JsonImage'
         400:
           description: Bad request, meaning that the wrong values were passed for some parameters.
        """
        if not id:
            message = "No Image ID provided"
            return make_response(jsonify(message=message), 400)
        if not id.isnumeric():
            message = "Image ID should be numeric"
            return make_response(jsonify(message=message), 400)
        id = int(id)
        img = db.session.query(Image).filter(Image.id == id).first()
        if not img:
            message = "No Image found for id {}".format(id)
            return make_response(jsonify(message=message), 404)
        try:
            request.json
        except:
            message = "No data, provide vote or tags values"
            return make_response(jsonify(message=message), 400)
            
        #vote = request.args.get('vote', None)
        if request.json:
           tags = request.json.get('tags', None)
           vote = request.json.get('vote', None)
        else:
           tags = None
           vote = None
           
        #applogger.debug("TAGS {}".format(tags))
        if vote is None and tags is None:
            message = "Missing parameters, vote or tags sould be provided"
            return make_response(jsonify(message=message), 400)
        if vote:
            if vote not in ['up', 'down']:
                message = 'Wrong value for vote, avaliable values are "up" and "down"'
                return make_response(jsonify(message=message), 400)
            if vote == 'up':
                img.score += 1
            elif vote == "down":
                img.score -= 1
            
        if tags is not None:
            # Check for existing tags
            image_tags = db.session.query(Tag).filter(Tag.txt.in_(tags)).all()
            tags_not_found = list(set(tags) - set([t.txt for t in image_tags]))
            applogger.debug(tags_not_found)
            if len(tags_not_found) > 0:
                for new_tag_txt in tags_not_found:
                    newtag = Tag(txt=new_tag_txt)
                    db.session.add(newtag)
                    db.session.commit()
                    image_tags.append(newtag)
            #if image_tags:
            img.tags = image_tags
        db.session.add(img)
        db.session.commit()
        res = self.imageschema.dump(img).data
        # clear_cache
        applogger.debug(["CLEAR CACHE", request.full_path])
        clear_cache("images")
        return jsonify(res), 200


class ImageBaseAPI(Resource):
    """
    Image Search API
    
    The search algorythm is: 
    - Select only cached images first
    - Apply specific filters (tags search at the moment, source, geolocation, etc)
    - Apply generic filters (image minimum width, orientation, etc)
    - If there's no cached results, perform an online search on all avaliable sources
      if no specific source is requested
    - Return an array of structured data, a widget or image file
    """
    def __init__(self):
        self.imageschema = ImageTagSchema()
        self.imagesschema = ImageTagSchema(many=True)
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('tags', action='append', required=False,
                                 help="No tags provided")
        self.parser.add_argument('limit', type=int, required=False,
                                 help="No limit provided")
        self.parser.add_argument('offset', type=int, required=False,
                                 help="No limit provided")
        self.parser.add_argument('source', choices=('pexels', 'pixabay', 'cloudinary', 'flickrtags', 'imgur', 'unsplash'), help='{error_msg}. Available values are: pexels, pixabay, cloudinary, flickrtags, imgur, unsplash')
        self.parser.add_argument('output', choices=('widget', 'json', 'image'), help='{error_msg}. Avaliable values are  widget, json, image')
        self.parser.add_argument('lang')
        self.parser.add_argument('descr', required=False)
        self.parser.add_argument('license', required=False)
        self.parser.add_argument('publisher', required=False)
                
        self.parser.add_argument('show_img_srcset', type=non_empty_string, required=False, nullable=False)
        self.parser.add_argument('trsource', choices=('google', 'yandex', 'deepl'), help='{error_msg}. Available values are google, yandex, deepl')
        
        self.parser.add_argument('minwidth', type=int, default=DEFAULT_WIDTH)
        self.parser.add_argument('orientation', choices=('portrait', 'landscape', 'p', 'l'), help='{error_msg}. Avaliable values are portrait, landscape, p, l')
        
        self.parser.add_argument('page', type=int, required=False,
                                 default=1, help="Results page parameter not provided")
        self.parser.add_argument('per_page', type=int, required=False,
                                 default=5, help="Results per page parameter not provided")
        # coordinates
        self.parser.add_argument('lat', type=float)
        self.parser.add_argument('lon', type=float)
        # address
        self.parser.add_argument('address')
        self.parser.add_argument('zipcode', type=str)
        self.parser.add_argument('city')
        self.parser.add_argument('match', choices=('any', 'exact'), help='{error_msg}. Avaliable values are any, exact', default="any")
        # image filters string
        self.parser.add_argument('filters', action="append")
        # story_id to link image with article
        # self.parser.add_argument('story_id', type=int)
        
    def prepare_results(self, results, page=1, per_page=PER_PAGE):
        """
        Prepare results before return, default implementation
        """
        applogger.debug("Base")
        return self.imagesschema.dump(results).data
    
    #@cache.cached(timeout=300, key_prefix=cache_key, forced_update=update_tag_counter)
    def get(self):
        """
        Get images based on tags, source, resolution
        and other filters. A sample query can look like 
        /api/v1/images?tags=Hotel&tags=photo&tags=night&limit=1&source=pixabay&minwidth=1300&orientation=landscape

        At least one of tags or location should be provided

        An example of location-based query
        /api/v1/images?address=Sunset boulevard 21&city=Los Angeles

        For location search lat and lon or any of address, city, zipcode parameters should be passed. If all location parameters are passed, only coordinates search would be performed. 

        Additional image filters could be applied, like f_grayscale, f_blackwhite, f_scale and others.
        ---
        tags: [Images,]
        parameters:
         - in: query
           name: tags
           type: array
           collectionFormat: multi
           items:
             type: string
             example: flore
           required: false
           description: tags to search
         - in: query
           name: limit
           type: integer
           required: false
           example: 5
           description: output results limit
         - in: query
           name: show_img_srcset
           type: boolean
           required: false
           example: 1
           description: provide srcset attribute for each image
         - in: query
           name: offset
           type: integer
           required: false
           example: 5
           description: output results offset in case of output set to widget
         - in: query
           name: match
           type: string
           enum: [any, exact]
           required: false
           description: Tags match. If not specified, any tag search would be performed.  For exact match the result for all tags would be returned
         - in: query
           name: source
           type: string
           enum: [pexels, pixabay, cloudinary, flickrtags, imgur, unsplash]
           required: false
           description: Image source, for example pixabay, pexels, etc. If no source \
        provided, the search would be performed in the following order,\
        pexels, pixabay, cloudinary, flickrtags, imgur, unsplash
         - in: query
           name: filters
           type: string
           required: false
           description: Image filters to be applied to the original image.\
        Filters can have optional parameters. <br />Avaliable output formats (f_outformat) \
        are bmp, jpg, png, gif, webp, j2k<br /> \
        Avaliable filters are <br /> \
        f_sepia<br /> f_grayscale<br /> f_blackwhite<br /> \
        f_blur-r_[blur radius, FLOAT or INT > 0]<br />f_scale-w_[width, INT or FLOAT, > 0]-h_[height, INT or FLOAT, > 0]<br />f_contrast-c_[contrast, ±INT]<br />\
        f_pureblackwhite-t_[threshold, INT>0]<br />f_outformat-[FORMAT]<br />f_layer-t_[text to be printed, TEXT]-x_[horizontal position, INT]-y_[vertical position, INT]\
        -r_[rotation, INT]-o_[opacity, INT]-c_[text color, HEXCOLOR or auto]-fs_[font size, INT]<br />f_padding-w_[width, INT]-h_[height, INT]-c_[background color, HEXCOLOR or auto]<br /> f_fit-w_[width, INT]-h_[height, INT]<br />f_crop-h_[INT]-w_[INT]-l_[INT]-t_[INT]-r_[INT]-b_[INT]-g_[north_east, north, north_west, west, south_west, south, south_east, east, center, face, faces]. Provide w(idth) and h(eight) or l(eft), r(ight), t(op) and b(ottom) values for precise cropping. The gravity value is optional, the default value is 'center'<br />f_fill-h_[INT]-w_[INT]-g_[north_east, north, north_west, west, south_west, south, south_east, east, center] <br /> f_show_source_service <br>\
        Each filter can be followed by a -nocache option to overwrite the previous cached image
         - in: query
           name: lang
           type: string
           required: false
           description: tags language, es-en\
        to translate from Spanish to English
           example: es-en
         - in: query
           name: trsource
           type: string
           enum: [google, yandex, deepl]
           required: false
           description: translation source
         - in: query
           name: publisher
           type: string
           required: false
           description: An option to filter dpa pictures
         - in: query
           name: minwidth
           type: integer
           required: false
           description: minimum width for returned images
         - in: query
           name: orientation
           type: string
           enum: [portrait, landscape, p, l]
           required: false
           description: output images orientation, portrait or landscape
         - in: query
           name: output
           type: string
           enum: [widget, json, image]
           required: false
           description: results output, json, widget (image link), image file
         - in: query
           name: lat
           type: float
           required: false
           description: location lattitude
         - in: query
           name: lon
           type: float
           required: false
           description: location longitude
         - in: query
           name: address
           type: string
           required: false
           description: location address
         - in: query
           name: zipcode
           type: string
           required: false
           description: location zipcode
         - in: query
           name: city
           type: string
           required: false
           description: location city
        definitions:
          JsonImage:
            type: object
            properties:
              id:
                type: integer
                description: Image ID
              orientation:
                type: string
                description: Image orientation,\
        portrait or landscape
              url:
                type: string
                description: Image URL
              width:
                type: integer
                description: Image width
              height:
                type: integer
                description: Image height
              score:
                type: integer
                description: Image score for custom ordering
              source:
                type: string
                description: Image source
              license:
                type: string
                description: Image license
              publisher:
                type: string
                description: Image publisher
              descr:
                type: string
                description: Image description
              tagstxt:
                type: array
                description: A list of related tags
                items:
                  type: string
                  description: A single tag
              versions:
                type: array
                description: A list of image versions
                items:
                type: object
                properties:
                  version:
                    type: integer
                    description: Version number
                  filter_url:
                    type: string
                    description: Filter-based image URL
                  version_url:
                    type: string
                    description: Version-based image URL
              location:
                type: object
                properties:
                  addr:
                    type: string
                    description: Location address
                  city:
                    type: string
                    description: Location city
                  zipcode:
                    type: string
                    description: Location zipcode
                  lat:
                    type: string
                    description: Location lattitude
                  lon:
                    type: string
                    description: Location longitude
          WidgetImage:
            type: string
            description: URL of an image for a widget output
        responses:
         200:
           description: Search results. Json for output=json\
         or no output parameter, single URL for output=widget
           schema:
             oneOf:
               - $ref: '#/definitions/JsonImage'
               - $ref: '#/definitions/WidgetImage'
         400:
           description: Bad request, meaning that the wrong values were passed for some parameters.
        """
        args = self.parser.parse_args()
        filters = args['filters']
        tags = args['tags']
        if tags:
            for i, t in enumerate(tags):
                if "," in t:
                    fixed_tags = [tg.strip() for tg in t.split(",")]
                    tags.extend(fixed_tags)
                    tags.pop(i)
        
        applogger.debug(["TAGS", tags])
        limit = args['limit']
        offset = args['offset']
        source = args['source']
        output = args['output']
        show_srcset = args['show_img_srcset']
        #applogger.debug(args)
        if output:
            if output in ['widget', 'image']:
                limit = 1
        lang = args['lang']
        trsource = args['trsource']
        width = args['minwidth']
        orientation = args['orientation']
        # pagination params
        per_page = args['per_page']
        page = args['page']
        if not per_page:
            per_page = PER_PAGE
        if not page:
            page = 1
        # coordinates
        lat = args['lat']
        lon = args['lon']
        # woeid = args['woeid']
        address = args['address']
        zipcode = args['zipcode']
        city = args['city']
        match = args['match']
        publisher = args['publisher']
        
        cache_to_be_cleared=False

        ## Removed tags or locations requirement
        
        #if not tags and not any((lat, lon, address, zipcode, city)):
        #    message='Not enough parameters. Provide tags or location'
        #    #fabort(400, message)
        #    return make_response(jsonify(message=message), 400)
        
        # geosearch args validation
        if any((lat, lon)):
            if not all((lat, lon)):
                message='Not enoung arguments for location search. Please provide lat & lon parameters'
                return make_response(jsonify(message=message), 400)
                
            if not -180 <= lat <= 180 or not -180 <= lon <= 180:
                message='Coordinates values should be between -180 and 180'
                return make_response(jsonify(message=message), 400)

        # address, zipcode & city params
        if any((address, zipcode, city)):
            if address:
                if len(address) < 1:
                    message='Address value too short'
                    return make_response(jsonify(message=message), 400)
        if tags:
            if len(tags) == 1 and tags[-1] == '':
                message='No tags provided'
                return make_response(jsonify(message=message), 400)
                
        ### Move to a separate function
        ### From here >>>>
        # Select only cached images first
        ##images = db.session.query(Image).filter(
        ##    Image.downloaded == 1).filter(Image.url is not None)
        ## BLACKLISTS FILTER >>>
        ##blocked_authors = db.session.query(AuthorBlacklist.author)
        ##blocked_sources = db.session.query(SourceBlacklist.source)
        ##images = images.filter(Image.author.notin_(blocked_authors))
        ##images = images.filter(Image.source.notin_(blocked_sources))
        
        ### CACHED IMAGES SEARCH 
        ### TAGS SEARCH
        ### Translate tags and perform tag filtering
        ##if tags:
        ##    tags = [t.lower() for t in tags]
        ##    applogger.debug("TAGS START")
        ##    applogger.debug(['TAGS', tags])
        ##    taglist = []
        ##    fulltagslist = []
        ##    
        ##    for index, orig_tag in enumerate(tags):
        ##        #orig_tag = orig_tag.lower()
        ##        applogger.debug("INDEX {}".format(index))
        ##        applogger.debug("TAGS 1")
        ##        # SETTING DEFAULT TRANSLATION
        ##        #if not all([lang, trsource]):
        ##        #    lang = "de-en"
        ##        #    trsource = "deepl"
        ##        if lang and trsource:
        ##            tag = translate_tag(trsource, lang, orig_tag)
        ##            if not tag:
        ##                tag = orig_tag
        ##        else:
        ##            tag = orig_tag
        ##        applogger.debug("TAGS 2")
        ##            
        ##        tgs = set([tag, orig_tag])
        ##        
        ##        for tg in tgs:
        ##            tagobj = db.session.query(Tag).filter(Tag.txt == tg).first()
        ##        
        ##            if tagobj:
        ##                taglist.append(tagobj)
        ##            else:
        ##                newtag = Tag(txt=tg)
        ##                db.session.add(newtag)
        ##                db.session.commit()
        ##                taglist.append(newtag)

        ##        #tags[index] = tag
        ##        fulltagslist.append(tag)
        ##        if tag != orig_tag:
        ##            fulltagslist.append(orig_tag)
        ##        applogger.debug("TAGS {}".format(tags))
        ##        
        ##    tags = fulltagslist
        ##    applogger.debug(["TAGS END", [t.txt for t in taglist]])

        ##    applogger.debug("SEARCH DB TAGS {}".format(taglist))
        ##    # Multiple AND
        ##    # images_ = images.filter(*[Image.tags.contains(t) for t in taglist])
        ##    images_ = images.join(img_tags).filter(img_tags.columns.tag_id.in_([t.id for t in taglist]))
        ##    results_count = countresults(images_)
        ##    applogger.debug(["TAG RESULTS COUNT", results_count])
        ##    if match == 'exact':
        ##        applogger.debug("EXACT TAG SEARCH")
        ##    else:
        ##        applogger.debug("ANY TAG SEARCH")

        ##    if results_count == 0 and match == 'any':
        ##        # Multiple OR
        ##        images_ = images.filter(Image.tags.any(Tag.txt.in_(tags)))
        ##    # tagsocount:
            
        ##    images = images_
        ##    # Image source filter
        ##    if source:
        ##        images = images.filter(Image.source == source)
        ### GEO SEARCH HERE
        ##elif any([x is not None for x in [lat, lon, address, city, zipcode]]):
        ##    # Either coordinate search or address search
        ##    applogger.debug(f"GEO SEARCH lat: {lat}, lon: {lon}, address: {address}, city: {city}, zipcode: {zipcode}")
        ##    if all([lat, lon]):
        ##        if lat:
        ##            images = images.filter(Image.location.has(Location.lat==str(lat)))
        ##        if lon:
        ##            images = images.filter(Image.location.has(Location.lon==str(lon)))
        ##        applogger.debug("Found COORDS images {}".format(images.all()))
        ##    else:
        ##        address_filter = []
        ##        if address:
        ##            ilike_str = f"%{address}%"
        ##            address_filter.append(Image.location.has(Location.addr.ilike(ilike_str)))
        ##        if city:
        ##            address_filter.append(Image.location.has(Location.city == city))
        ##        if zipcode:
        ##            address_filter.append(Image.location.has(Location.zipcode == zipcode))
        ##        if any([address, city, zipcode]):
        ##            applogger.debug("Address search")
        ##            images = images.filter(and_(*address_filter))
                    

        ### GENERIC FILTERS: orientation, minwidth, limit
        ### Image orientation filter
        ##if orientation:
        ##    # Added for backward-compatibility
        ##    if orientation == 'p':
        ##        orientation = 'portrait'
        ##    if orientation == 'l':
        ##        orientation = 'landscape'
        ##    images = images.filter(Image.orientation == orientation)
        ### Image minimal width filter
        ##if width:
        ##    images = images.filter(Image.width >= width)
        ##if not limit:
        ##    limit = DEFAULT_LIMIT
        ##images = images.order_by(Image.score.desc()) 
        ##images_query = images
        ##images_query = images_query.limit(limit)
        ##if offset:
        ##    images_query = images_query.offset(offset)            
        ##count_result = countresults(images)#.with_entities(func.count()).scalar()
        ##applogger.debug(f"COUNT {count_result}")
        ##images = images.limit(limit).all()
        
        ### ONLINE SEARCH
        ### Perform an online search if there's not enough cached results
        ##if len(images) < limit:
        ##    # clear_cache
        ##    cache_to_be_cleared = True
        ##    add_search_limit = limit - len(images)
        ##    applogger.debug('Cached results: {}, images to download: {}'.format(len(images), add_search_limit))
        ##    # perform an additional search and save results to the DB
        ##    if tags:
        ##        applogger.debug(f"TAGS additional search {tags}")
        ##        # TODO add 'additional search = True' arg to the tags search
        ##        # function to skip existing results
        ##        imagetagsearch(source, tags, add_search_limit, width, orientation)
        ##    # GEOSEARCH 
        ##    elif all([lat is not None, lon is not None]):
        ##        # perform coordinates search here
        ##        # Flickr
        ##        applogger.debug(f"COORDS additional search lat: {lat}, lon: {lon}")
        ##        location = getlocation(lat=lat, lon=lon)
        ##        totalfound = flickrsearch(limit, location=location)
        ##        
        ##        applogger.debug(f"FOUND: {totalfound}, requested: {limit}")
        ##        if totalfound < limit:
        ##            #applogger.debug(f"Additional google images location search")
        ##            totalfound = googleplacessearch(limit=limit, location=location, width=width)

        ##    elif any((address, zipcode, city)):
        ##        # perform address search here
        ##        # Google Places
        ##        applogger.debug(f"ADDRESS additional search address: {address}, zipcode: {zipcode}, city: {city}")
        ##        # Translate address to the coordinates before
        ##        # performing location search
        ##        location = getlocation(address=address, zipcode=zipcode, city=city)
        ##        if location.lat and location.lon:
        ##            totalfound = flickrsearch(limit, location=location)
        ##            if totalfound == 0:
        ##                totalfound = googleplacessearch(limit=limit, location=location, width=width)
                # googleplacessearch(addr=address, zipcode=zipcode, city=city)
        ##    # reprat the query after fetching additional images
        ##    # to get results as a query set
        ##    # to be passed to the prepare_results function
        ##    db.session.commit()
        ##    #db.session.refresh(images_query)
        ##    
        ##    images = images_query.all()
        ### Move to a separate function
        ### To here <<<<
        
        images_query, cache_to_be_cleared = basic_image_search(filters=filters, tags=tags, limit=limit, offset=offset, source=source, output=output, show_srcset=show_srcset, lang=lang, trsource=trsource, width=width, orientation=orientation, per_page=per_page, page=page, lat=lat, lon=lon, address=address, zipcode=zipcode, city=city, match=match, publisher=publisher)
        
        if cache_to_be_cleared:
            applogger.debug(["CLEAR CACHE", request.full_path])
            clear_cache(request.full_path, exact_match=True)

        res = self.prepare_results(images_query, output=output, page=page, per_page=per_page, url=request.url, filters=filters, show_srcset=show_srcset)
        return res, 200

        
class ImageV1API(ImageBaseAPI):
    def __init__(self, *args, **kwargs):
        super(ImageV1API, self).__init__(*args, **kwargs)

    def prepare_results(self, images, output='all', page=1, per_page=PER_PAGE, url=None, filters=None, show_srcset=False):
        """
        API v1 results formatting
        """
        applogger.debug("V1")
        images = images.all()
        if filters:
            # Validate filters parameters
            for fltr in filters:
                apply_single_chain(fltr)

        if output not in ['widget', 'image']:
            data = self.imagesschema.dump(images).data
            if filters:
                fltrstr = "/".join(filters)
                for img in data:
                    newurl = add_filters_to_url(img['url'], fltrstr)
                    img['url'] = newurl

            if not show_srcset:
                pass
            else:
                if show_srcset in ['1', 'True', 'true']:
                    srcsizes = [f"(max-width: {s}px) {s}px" for s in BREAKPOINTS[:-1]]
                    srcsizes.append("{}px".format(BREAKPOINTS[-1]))
                    srcsizes = ", ".join(srcsizes)
                    for img in data:
                        newsrcset = add_srcset_to_url(srcsizes, img['url'])
                        img['srcset'] = newsrcset
                elif show_srcset in ['0', "false", "False"]:
                    pass
                else:
                    message = 'show_img_srcset accepts boolean values like 0, 1, True, False, true, false'
                    return make_response(jsonify(message=message), 400)
            results = data
        else:
            applogger.debug("OUTPUT WIDGET OR IMAGE")
            if len(images) == 0:
                results = "https://via.placeholder.com/150"
            else:
                host_url = request.host_url.replace('http://', 'https://', 1)
                newurl = host_url + images[0].path
                if filters:
                    fltrstr = "/".join(filters)
                    newurl = add_filters_to_url(newurl, fltrstr)
                if output == 'widget':
                    results = newurl
                elif output == "image":
                    applogger.debug("OUTPUT IMAGE")
                    #serve_url(newurl)
                    raise RequestRedirect(newurl)
                
        return results

        
class ImageV2API(ImageBaseAPI):
    def _get(self):
        return ImageBaseAPI.get(self)
    
    def get(self):
        """
        Get images based on tags, source, resolution
        and other filters. A sample query can look like 
        /api/v2/images?tags=Hotel&tags=photo&tags=night&limit=1&source=pixabay&minwidth=1300&orientation=landscape

        At least one of tags or location should be provided

        An example of location based query:
        /api/v2/images?address=Sunset boulevard 21&city=Los Angeles

        For location search lat and lon or any of address, city, zipcode parameters should be passed. If all location parameters are passed, only coordinates search would be performed.

        Additional image filters could be applied, like f_grayscale, f_blackwhite, f_scale and others.
        ---
        tags: [Images,]
        parameters:
         - in: query
           name: tags
           type: array
           collectionFormat: multi
           items:
             type: string
             example: flore
           required: false
           description: tags to search
         - in: query
           name: limit
           type: integer
           required: false
           example: 5
           description: output results limit
         - in: query
           name: offset
           type: integer
           required: false
           example: 5
           description: output results offset in case of output set to widget
         - in: query
           name: match
           type: string
           enum: [any, exact]
           required: false
           description: Tags match. If not specified, any tag search would be performed. For exact match the result for all tags would be returned
         - in: query
           name: show_img_srcset
           type: boolean
           required: false
           example: 1
           description: provide srcset attribute for each image
         - in: query
           name: vote
           type: string
           enum: [up, down]
           required: false
           description: Image score for custom sorting of tag/location search results. Used only in PATCH requests.
         - in: query
           name: source
           type: string
           enum: [pexels, pixabay, cloudinary, flickrtags, imgur, unsplash]
           required: false
           description: Image source, for example pixabay, pexels, etc. If no source \
        provided, the search would be performed in the following order,\
        pexels, pixabay, flickrtags, imgur, cloudinary, unsplash
         - in: query
           name: filters
           type: string
           required: false
           description: Image filters to be applied to the original image.\
        Filters can have optional parameters. Avaliable output formats (f_outformat) \
        are bmp, jpg, png, gif, webp, j2k<br /> \
        Avaliable filters are<br /> \
        f_sepia<br />f_grayscale<br />f_blackwhite<br /> \
        f_blur-r_[blur radius, FLOAT or INT > 0]<br />f_scale-w_[width, INT or FLOAT, > 0]-h_[height, INT or FLOAT, > 0]<br />f_contrast-c_[contrast, ±INT]<br />\
        f_pureblackwhite-t_[threshold, INT>0]<br />f_outformat-[FORMAT]<br />f_layer-t_[text to be printed, TEXT]-x_[horizontal position, INT]-y_[vertical position, INT]-r_[rotation, INT]-o_[opacity, INT]-c_[text color, HEXCOLOR]-fs_[font size, INT]<br />\
        f_padding-w_[width, INT]-h_[height, INT]-c_[background color, HEXCOLOR or auto]<br />f_fit-w_[width, INT]-h_[height, INT]<br />f_crop-h_[INT]-w_[INT]-l_[INT]-t_[INT]-r_[INT]-b_[INT]-g_[north_east, north, north_west, west, south_west, south, south_east, east, center, face, faces]<br />f_fill-h_[INT]-w_[INT]-g_[north_east, north, north_west, west, south_west, south, south_east, east, center] <br /> f_show_source_service <br> \
        Each filter can be followed by a -nocache option to overwrite the previous cached image
         - in: query
           name: lang
           type: string
           required: false
           description: tags language, es-en\
        to translate from Spanish to English
           example: es-en
         - in: query
           name: trsource
           type: string
           enum: [google, yandex, deepl]
           required: false
           description: translation source
         - in: query
           name: minwidth
           type: integer
           required: false
           description: minimum width for returned images
         - in: query
           name: orientation
           type: string
           enum: [portrait, landscape, p, l]
           required: false
           description: output images orientation, portrait or landscape
         - in: query
           name: output
           type: string
           enum: [widget, json, image]
           required: false
           description: results output, json, widget (image link), image file
         - in: query
           name: page
           type: integer
           required: false
           description: results page
         - in: query
           name: per_page
           type: integer
           required: false
           description: results per page
         - in: query
           name: lat
           type: float
           required: false
           description: location lattitude
         - in: query
           name: lon
           type: float
           required: false
           description: location longitude
         - in: query
           name: woeid
           type: int
           required: false
           description: location WOEID (Where On Earth IDentifier)
         - in: query
           name: address
           type: string
           required: false
           description: location address
         - in: query
           name: zipcode
           type: string
           required: false
           description: location zipcode
         - in: query
           name: city
           type: string
           required: false
           description: location city
        definitions:
          JsonImage:
            type: object
            properties:
              id:
                type: integer
                description: Image ID
              orientation:
                type: string
                description: Image orientation,\
        portrait or landscape
              url:
                type: string
                description: Image URL
              width:
                type: integer
                description: Image width
              height:
                type: integer
                description: Image height
              score:
                type: integer
                description: Image score for custom ordering
              source:
                type: string
                description: Image source
              tagstxt:
                type: array
                description: A list of related tags
                items:
                  type: string
                  description: A single tag
              versions:
                type: array
                description: A list of image versions
                items:
                type: object
                properties:
                  version:
                    type: integer
                    description: Version number
                  filter_url:
                    type: string
                    description: Filter-based image URL
                  version_url:
                    type: string
                    description: Version-based image URL
              location:
                type: object
                properties:
                  addr:
                    type: string
                    description: Location address
                  city:
                    type: string
                    description: Location city
                  zipcode:
                    type: string
                    description: Location zipcode
                  lat:
                    type: string
                    description: Location lattitude
                  lon:
                    type: string
                    description: Location longitude
          WidgetImage:
            type: string
            description: URL of an image for a widget output
        responses:
         200:
           description: Search results. Json for output=json\
         or no output parameter, single URL for output=widget
           schema:
             oneOf:
               - $ref: '#/definitions/JsonImage'
               - $ref: '#/definitions/WidgetImage'
         400:
           description: Bad request, meaning that the wrong values were passed for some parameters.

        """
        return self._get()
    
    def prepare_results(self, images, output='all', page=1, per_page=PER_PAGE, url=None, filters=None, show_srcset=False):
        """
        API v2 results formatting
        """
        applogger.debug("V2")
        applogger.debug(images)
        applogger.debug(output)
        if filters:
            # Validate filters parameters
            for fltr in filters:
                apply_single_chain(fltr)
            applogger.debug("Filters checked") 
        if output not in [ 'widget', 'image']:
            prev_url = None
            next_url = None
            paginated = images.from_self().paginate(page, per_page, error_out=False)
            images_on_page = paginated.items
            total = paginated.total
            last_page = paginated.pages
            next_page = paginated.next_num
            prev_page = paginated.prev_num
            if url:
                parsed = urlparse(url)
                urlpath = parsed.path
                base_url = urljoin(request.host_url, urlpath) + "?"
                base_url = base_url.replace("http://", "https://")
                params = parse_qs(parsed.query)
                if next_page:
                    params['page'] = next_page
                    next_url =  base_url + urlencode(params, doseq=True)
                if prev_page:
                    params['page'] = prev_page
                    prev_url = base_url + urlencode(params, doseq=True)
            #modify results url if filters are passed
            data = self.imagesschema.dump(images_on_page).data
            if filters:
                fltrstr = "/".join(filters)
                for img in data:
                    newurl = add_filters_to_url(img['url'], fltrstr)
                    img['url'] = newurl
            if not show_srcset:
                pass
            else:
                if show_srcset in ['1', 'True', 'true']:
                    srcsizes = [f"(max-width: {s}px) {s}px" for s in BREAKPOINTS[:-1]]
                    srcsizes.append("{}px".format(BREAKPOINTS[-1]))
                    srcsizes = ", ".join(srcsizes)
                    for img in data:
                        newsrcset = add_srcset_to_url(srcsizes, img['url'])
                        img['srcset'] = newsrcset
                elif show_srcset in ['0', "false", "False"]:
                    pass
                else:
                    message = 'show_img_srcset accepts boolean values like 0, 1, True, False, true, false'
                    return make_response(jsonify(message=message), 400)
            applogger.debug("RETURN RESULTS")
            results = {'data': data,
                       'total': total,
                       'per_page': per_page,
                       'current_page': page,
                       'last_page': last_page,
                       'next_page_url': next_url,
                       'prev_page_url': prev_url
            }
        else:
            applogger.debug("OUTPUT IMAGE")
            images_count = countresults(images)
            if images_count == 0:#.with_entities(func.count()).scalar() == 0:
                results = "https://via.placeholder.com/150"
            else:
                host_url = request.host_url.replace('http://', 'https://', 1)
                newurl = host_url + images[0].path
                
                if filters:
                    fltrstr = "/".join(filters)
                    newurl = add_filters_to_url(newurl, fltrstr)
                if output == 'widget':
                    results = newurl
                elif output == "image":
                    applogger.debug("OUTPUT IMAGE")
                    #serve_url(newurl)
                    applogger.debug(newurl)
                    raise RequestRedirect(newurl)

                results = newurl

        applogger.debug(["FINAL RETURN", len(results), results])

        return results

    
def make_linear_ramp(white):
    ramp = []
    r, g, b = white
    for i in range(255):
        ramp.extend((int(r*i/255), int(g*i/255), int(b*i/255)))
    return ramp

def get_color(fltrstr):
    # TODO add standart
    # black by default
    rgb = (0,0,0)
    if len(re.findall('c_auto', fltrstr)) > 0:
        return rgb
    
    colres = re.findall('c_[0-9a-fA-F]+', fltrstr)
    if len(colres) > 0:
        hexcolor = colres[0].lstrip('c_')
        rgb = tuple(int(hexcolor[i:i+2], 16) for i in (0, 2, 4))
    else:
        message = "Color value should be in HEX format, example: c_0000FF, or auto"
        #abort(make_response(jsonify(message=message), 400))
        fabort(400, message)
    return rgb

def make_error(status_code, sub_code, message, action):
    response = jsonify({
        'status': status_code,
        'sub_code': sub_code,
        'message': message,
        'action': action
    })
    response.status_code = status_code
    return response

@app.errorhandler(400)
def custom400(error):
    response = jsonify({'message': error.description})
    return response

def validate_numeric(parname, fltrstr, allow_none=False):
    v = re.findall(f'({parname}_-?[^-/]+)', fltrstr)
    if len(v) > 0:
        v = v[0]
        v = v.replace(f'{parname}_', '')
        try:
            float(v)
        except:
            return make_response(jsonify(message=f"Wrong value for {parname}, should be numeric"), 400)
    else:
        if not allow_none:
            return make_response(jsonify(message=f"Wrong value for {parname}"), 400)
        
def get_dimensions(fltrstr, getfloat=False, allow_none=False, short=True):
    if short:
        dimensions = {'w': None, 'h': None}
    else:
        dimensions = {'w': None, 'h': None, 'l': None, 't':None, 'r': None, 'b': None}
    for dim in dimensions:
        validate_numeric(dim, fltrstr, allow_none=True)
        if getfloat:
            res = re.findall(f'({dim}_-?\d+(?:\.\d+)?)', fltrstr)
        else:
            res = re.findall(f'{dim}_-?[0-9]+', fltrstr)
        if len(res) > 0:
            if getfloat:
                res = float(res[0].lstrip(dim+"_"))
            else:
                res = int(res[0].lstrip(dim+"_"))
            dimensions[dim] = res
    if not any(dimensions.values()):
        if not allow_none:
           message = "Wrong w or h value"
           fabort(400, message)
           #abort(make_response(jsonify(), 400))
    if any([d <= 0 for d in dimensions.values() if d]):
        if not allow_none:
           message = "w and h should be positive"
           fabort(400, message)
           #abort(make_response(jsonify(message="w and h should be positive"), 400))
    return dimensions

def get_resize_factor(dim, img):
    factor = [0, 0]
    
    if dim['w'] and not dim['h']:
        if dim['w'] and dim['w'] > 1:
            factor = [dim['w'] / img.width for i in range(2)]
        elif dim['w'] <= 1:
            factor = [ dim["w"] for i in range(2)]
            
    elif dim['h'] and not dim['w']:
        if dim['h'] and dim['h'] > 1:
            factor = [dim['h'] / img.height for i in range(2)]
        elif dim['h'] <= 1:
            factor = [dim["h"] for i in range(2)]
    elif dim['h'] and dim['w']:
        if dim['w'] and dim['w'] > 1:
            factor[0] = dim['w'] / img.width
        elif dim['w'] <= 1:
            factor[0] = dim["w"]
        if dim['h'] and dim['h'] > 1:
            factor[1] = dim['h'] / img.height
        elif dim['h'] <= 1:
            factor[1] = dim["h"]
        
            
    return factor

def apply_showsource(img=None):
    if img:
        applogger.debug(img)
        #path = img.filename.replace(IMGPATH, "")[1:]
        dbimage = db.session.query(Image).filter(Image.id == img.parent_id).first()
        if not dbimage:
            message = "No source found for the given URL"
            applogger.debug(message)
            return make_response(jsonify(message=message), 404)
        src = dbimage.source
        source = f"© www.{dbimage.source}.com"
        if "flickr" in src:
            source = "© www.flickr.com"
        # img, text, position, rotation, color, opacity, font, fontsize, fill=FILLCOLOR
        # calculate position of the right bottom corner:
        # and font size based on image width
        w, h = img.size
    
        if w >= h:
            position = (w-int(w*35/100), h-int(h*8/100))
            fontsize = int(w/30)
        else:
            position = (w-int(w*50/100), h-int(h*8/100))
            fontsize = int(h/25)

        opacity = 255
        #color = (255, 255, 255, opacity)
        rotation = 0
        color = (204, 210, 219, 255)
        newimg = apply_addlayer(img, source, position, rotation, color, opacity, TEXT_FONT, fontsize, shadow=True, shadowcolor=(79,59,58))
        return newimg
    
def apply_padding(img=None, dim=None, bgcolor=None):
    """
    Padding image to desired shape and keep its aspect ratio
    """
    if img:
        x, y = img.size
        ratio = x / y
        desired_ratio = dim['w']/dim['h']
        w = max(dim['w'], x)
        h = int(w / desired_ratio)
        if h < y:
            h = y
            w = int(h * desired_ratio)

        applogger.debug(bgcolor)
        
        if bgcolor == 'auto':
            newimg = img.filter(ImageFilter.GaussianBlur(radius=50))
            newimg = newimg.resize((int(w), int(h)))
        else:
            newimg = Img.new('RGB', (int(w), int(h)), bgcolor)
        newimg.paste(img, ((int(w) - int(x)) // 2, (int(h) - int(y)) // 2))
        newimg = newimg.resize((int(dim['w']), int(dim['h'])))

        return newimg

def apply_addlayer(img=None, text="", position=[0,0], rotation=0, color=[255,255,255,0], opacity=1, font=None, fontsize=16, shadow=False, shadowcolor=(79,59,58)):
    """
    Add text layer to the image
    Has the following parameters: 
    text: Any text incl spaces
    position: integer
    rotation: integer
    color: Any HEX color
    opacity: integer 0-100
    font: Now only one
    fontsize: integer
    """
    if img:
        img = img.convert("RGBA")
        txt = Img.new('RGBA', img.size, (255,255,255,0))
        
        draw = ImageDraw.Draw(txt)
        font = ImageFont.truetype(font, size=fontsize)
        tw, th = font.getsize(text)
        if shadow:
            draw.text((position[0] + 2, position[1] + 2), text, font=font, fill=(79,59,58))
        applogger.debug(["COLOR", color])
        draw.text(position, text, fill=color, font=font)
        txt = txt.rotate(rotation, center=(tw/2, th/2))
        img = Img.alpha_composite(img, txt)
        img = img.convert("RGB")
        return img


def apply_sepia(img=None):
    """
    Apply sepia filter
    """
    if img:
        if img.mode != "L":
            newimg = img.convert("L")
        else:
            newimg = img
        sepia = make_linear_ramp((255, 240, 192))
        newimg = ImageOps.autocontrast(newimg)
        newimg.putpalette(sepia)
        newimg = newimg.convert("RGB")
        return newimg

def apply_grayscale(img=None):
    """
    Apply grayscale filter
    """
    if img:
        newimg = img.convert('LA')
        newimg = newimg.convert("RGB")
        newimg = ImageOps.autocontrast(newimg)
        return newimg


def apply_crop(img=None, dim=None, gravity='center', faces=None, scale_factor=None):
    """
    Image crop function. 
    Extract a region of the given width and height 
    out of the original image. The original proportions
    are retained and so is the size of the graphics. 
    You can specify the gravity parameter to 
    select which part of the image to extract, 
    or use fixed coordinates cropping.
    The filter string can be as follows:
    f_crop-h_480-w_600-g_north_west
    The "-" separator can be any character except ',', 
    The comma is reserved as a filter separator.
    """

    # default behaviour:
    # if image.faces_json is not empty,
    # don't crop faces 

    # No cropping of faces, auto detect faces
    # But if explicitly wanted the cropping will crop faces

    # if faces present, crop the faces area and resize it to
    # the crop area
    applogger.debug(["FILTER CROP", [d for d in dim.values()]])
    applogger.debug(["FILTER DIMS", dim])
    shortdim = dict((k, dim[k]) for k in ('w', 'h'))
    longdim = dict((k, dim[k]) for k in ('l', 't', 'r', 'b'))
    applogger.debug(shortdim)
    applogger.debug(longdim)
    if not all([d is not None for d in longdim.values()]):
        if not all([d is not None for d in shortdim.values()]):
            message = "Both width and height values are required"
            fabort(400, message)
        #return make_response(jsonify(message=message), 400)
    if all([d is not None for d in shortdim.values()]):
        for d in shortdim.keys():
            if not dim[d].is_integer():
                message = f"{d} value should be integer"
                return make_response(jsonify(message=message), 400)

    if gravity not in GRAVITYVALUES:
        message = "The available values for gravity are {}".format(", ".join(GRAVITYVALUES))
        return make_response(jsonify(message=message), 400)
    if img:
        if not all([d is not None for d in shortdim.values()]):
            if all([d is not None for d in longdim.values()]):
                box = list(map(int, [longdim['l'], longdim['t'], longdim['r'], longdim['b']]))
                newimg = img.crop(box)
                return newimg
            else:
                message = "Left, top, right and bottom values are required"
                fabort(400, message)
               
               
        width, height = img.size
        # default box position (center)
        left = (width - dim['w'])/2
        top = (height - dim['h'])/2
        right = (width + dim['w'])/2
        bottom = (height + dim['h'])/2
        
        if gravity == "west":
            left = left - dim['w']
            if left <= 0:
                left = 0
            right = left + dim['w']
                
        elif gravity == 'north':
            top = top - dim['h']
            if top <= 0:
                top = 0
            bottom = top + dim['h'] - 1
            
        elif gravity == 'east':
            left = left + dim['w']
            right = left + dim['w']
            if right >= width:
                right = width
                left = right - dim['w']
        elif gravity == 'south':
            top = top + dim['h']
            bottom = top + dim['h']
            if bottom >= height:
                bottom = height
                top = bottom - dim['h'] + 1
                
        elif gravity == "north_east":
            top = top - dim['h']
            if top <= 0:
                top = 0
            bottom = top + dim['h'] - 1 

            right = left + dim['w']
            if right >= width:
                right = width
            left = right - dim['w']
            
        elif gravity == "north_west":
            top = top - dim['h']
            if top <= 0:
                top = 0
            bottom = top + dim['h'] - 1
            left = left - dim['w']
            if left <= 0:
                left = 0
            right = left + dim['w']
            
        elif gravity == "south_east":
            top = top + dim['h']
            bottom = top + dim['h']
            if bottom >= height:
                bottom = height
                top = bottom - dim['h'] + 1
            left = left + dim['w']
            right = left + dim['w']
            if right >= width:
                right = width
                left = right - dim['w']
                
        elif gravity == "south_west":
            top = top + dim['h']
            bottom = top + dim['h']
            if bottom >= height:
                bottom = height
                top = bottom - dim['h'] + 1

            left = left - dim['w']
            if left <= 0:
                left = 0
            right = left + dim['w']
            
        elif gravity in ['face', 'faces']:
            applogger.debug(["CROP FACE", faces])
            if not isinstance(faces, list):
                if faces is None:
                    dbimage = db.session.query(Image).filter(Image.id==img.parent_id).first()
                    if dbimage:
                        if dbimage.faces_json:
                            faces = json.loads(dbimage.faces_json)
                            applogger.debug(["USING DB FACES RECORD", faces])
                            # x, y, w, h
                            # scale_factor [w, h]
                            #if scale_factor is not None:
                            #    for i, f in enumerate(faces):
                            #        scaled_faces = [round(f[0]*scale_factor[0]),
                            #                        round(f[1]*scale_factor[1]),
                            #                        round(f[2]*scale_factor[0]),
                            #                        round(f[3]*scale_factor[1])]
                            #        
                            #        faces[i] = scaled_faces
                            

            if len(faces) > 0:
                applogger.debug(['FACES', faces])
                if scale_factor is not None:
                    scale_x = scale_factor[0]
                    scale_y = scale_factor[1]

                else:
                    scale_x = 1
                    scale_y = 1
                    
                if gravity == "face":
                    x, y, w, h = faces[-1]
                    center_x = (x*scale_x + (x*scale_x+w*scale_x))/2
                    center_y = (y*scale_y + (y*scale_y+w*scale_y))/2
                    
                elif gravity == "faces":
                    center_x = sum([(el[0]*scale_x + (el[0]*scale_x+el[2]*scale_x))/2 for el in faces])/len(faces)
                    center_y = sum([(el[1]*scale_y + (el[1]*scale_y+el[3]*scale_y))/2 for el in faces])/len(faces)


                left = center_x - dim['w']/2
                top = center_y - dim['h']/2
                right = center_x + dim['w']/2
                bottom = center_y + dim['h']/2
                    
        box = list(map(int, [left, top, right, bottom]))
        applogger.debug(["FINAL CROP", img.size, dim, box])
        newimg = img.crop(box)
    
        return newimg

        
def apply_fit(img=None, dim=None):
    """
    Image fit function. The image is resized so that it takes up
    as much space as possible within a bounding box 
    defined by the given width and height parameters. 
    The original aspect ratio is retained and all 
    of the original image is visible. It can take at least one parameter:
    w or h, for width and height respectively.
    The filter string can be as follows:
    f_fit-h_480-w_600
    The "-" separator can be any character except ',', 
    The comma is reserved as a filter separator.
    """    

    newimg = None
    too_large = False
    too_small = False
    if not all([d for d in dim.values()]):
        message = "Both width and height values are required"
        fabort(400, message)
    if not all([d.is_integer() for d in dim.values()]):
        message = f"Dimensions accept only integer values"
        fabort(400, message)
    if any([v >= MAX_RESIZE for v in dim.values() if v is not None]):
        too_large = True
        
    if any([v < 0.001 for v in dim.values() if v is not None]):
        too_small = True
    if img:
        if not too_large and not too_small:
            img.thumbnail(tuple([dim['w'], dim['h']]), Img.ANTIALIAS)
            background = Img.new('RGBA', tuple([int(d) for d in dim.values()]), (255, 255, 255, 0))
            background.paste(img, (int((dim['w'] - img.size[0]) / 2), int((dim['h'] - img.size[1]) / 2))
)
            newimg = background
            newimg = newimg.convert("RGB")
            #newimg = ImageOps.fit(img, (int(dim['w']), int(dim['h'])), Img.ANTIALIAS)
        else:
            newimg = img
        return newimg

    
def fast_resize(img=None, dim=None):
    # Only for MagickWand Objects
    factor = get_resize_factor(dim, img)
    if all(dim.values()):
        img.resize(int(img.width * factor[0]), int(img.height * factor[1]))
    elif any(dim.values()):
        img.resize(int(img.width * factor[0]), int(img.height * factor[0]))
    return img

    
def apply_resize(img=None, dim=None):
    """
    Resize function. It can take at least one parameter:
    w or h, for width and height respectively.
    If w or h is a float and is less than 1, then
    the result is scaled by the given value. 
    w_0.25 is scaled to a width of 25%.
    The filter string can be as follows:
    f_scale-h_480-w_600
    The "-" separator can be any character except ',', 
    The comma is reserved as a filter separator.
    """
    factor = None
    newimg = None
    too_large = False
    too_small = False
    if img:
        factor = get_resize_factor(dim, img)
        if all(dim.values()):
            newimg = img.resize((int(img.width * factor[0]), int(img.height * factor[1])), Img.ANTIALIAS)
        elif any(dim.values()):
            newimg = img.resize((int(img.width * factor[0]), int(img.height * factor[0])), Img.ANTIALIAS)
                
    return newimg


def apply_blur(img=None, radius=0):
    """
    Gaussian blur filter with
    radius parameter.
    Example: f_blur-r_20
    If no radius provided, the default r_0 value is used
    """
    if img:
        newimg = img.filter(ImageFilter.GaussianBlur(radius=radius))
        return newimg

def apply_contrast(img=None, scale=1):
    """
    Contrast filter with scale parameter
    Example: f_contrast-c_35
    If no scale value provided, the default c_100 is used
    """
    if img:
        newimg = ImageEnhance.Contrast(img).enhance(scale)
        return newimg

def apply_blackwhite(img=None):
    if img:
        newimg = img.convert('1')
        newimg = newimg.convert("RGB")
        return newimg

def apply_pureblackwhite(img=None, thresh=200):
    if img:
        fn = lambda x : 255 if x > thresh else 0
        newimg = img.convert('L').point(fn, mode='1')
        newimg = newimg.convert("RGB")
        return newimg

def apply_filter(fltrstr, img=None):
    """
    Filter string parser
    Finds an appropriate filter, parses it's parameters and passes it to 
    the respcetive filter function.
    """
    applogger.debug(f'parsing & applying a single filter {fltrstr} {img}')
    if hasattr(img, 'parent_id'):
        parent_id = img.parent_id
    else:
        parent_id = None
        
    applogger.debug(["PARENTID", parent_id])
        
    newimg = None
    if 'f_sepia' in fltrstr:
        newimg = apply_sepia(img)
    elif 'f_grayscale' in fltrstr:
        newimg = apply_grayscale(img)
    elif 'f_scale' in fltrstr:
        dimensions = get_dimensions(fltrstr, getfloat=True)
        newimg = apply_resize(img, dimensions)

    elif 'f_fit' in fltrstr:
        dimensions = get_dimensions(fltrstr, getfloat=True)
        newimg = apply_fit(img, dimensions)
    elif 'f_fill' in fltrstr:
        dimensions = get_dimensions(fltrstr, getfloat=True, short=False)
        gravity = re.findall('g_-?[a-z_]+', fltrstr)

        if len(gravity) > 0:
            gravity = gravity[0][2:]
            if gravity not in GRAVITYVALUES:
                message = "The available values for gravity are {}".format(", ".join(GRAVITYVALUES))
                abort(make_response(jsonify(message=message), 400))
                #fabort(400, message)
                
        applogger.debug(["DIM", dimensions])
        for d in dimensions.keys():
            if dimensions[d]:
                if not dimensions[d].is_integer():
                    message = f"{d} value should be integer"
                    abort(make_response(jsonify(message=message), 400))
                    #fabort(400, message)

        if img:
            w, h = img.size
            scaleto = "land" if dimensions['w']/dimensions['h'] >= 1 else "portr"
            scalefrom = "land" if w/h >= 1 else "portr"
            conv = f"{scalefrom}-{scaleto}"
            scaledim = copy.deepcopy(dimensions)

            if conv in ["land-land", 'land-portr']:
                scaledim['h'] = None
            elif conv in ["portr-portr", "portr-land"]:
                scaledim['w'] = None
            #parent_id = img.parent_id
            newimg = apply_resize(img, scaledim)
            newimg.parent_id = parent_id
            nw, nh = newimg.size
            if nw < dimensions['w']:
                scaledim['w'] = dimensions['w']
                scaledim['h'] = None
                newimg = apply_resize(img, scaledim)
            elif nh < dimensions['h']:
                scaledim['w'] = None
                scaledim['h'] = dimensions['h']
                newimg = apply_resize(img, scaledim)
                
            resize_factor = get_resize_factor(scaledim, img)
            applogger.debug(["SCALEF", resize_factor])
            
            applogger.debug(["APPLYING CROP", newimg, gravity, scaledim, dimensions])
            newimg.parent_id = parent_id
            
            if gravity:
                newimg = apply_crop(newimg, dimensions, gravity, scale_factor=resize_factor)
            else:
                newimg = apply_crop(newimg, dimensions)
                
    elif 'f_crop' in fltrstr:
        dimensions = get_dimensions(fltrstr, getfloat=True, short=False)
        gravity = re.findall('g_-?[a-z_]+', fltrstr)
        if len(gravity) > 0:
            gravity = gravity[0][2:]
            newimg = apply_crop(img, dimensions, gravity)
        else:
            newimg = apply_crop(img, dimensions)
            
    elif "f_show_source_service" in fltrstr:
        applogger.debug("SHOWSOURCE")
        newimg = apply_showsource(img)
            
    elif 'f_blur' in fltrstr:
        rad = 0
        validate_numeric("r", fltrstr, allow_none=True)
        radius = re.findall('(r_-?\d+(?:\.\d+)?)', fltrstr)
        if len(radius) > 0:
            rad = float(radius[0].lstrip('r_'))
        
        if rad < 0:
            message = "Blur radius value should be positive"
            abort(make_response(jsonify(message=message), 400))
            #fabort(400, message)
        if not rad:
            newimg = apply_blur(img)
        else:
            newimg = apply_blur(img, rad)
    elif 'f_contrast' in fltrstr:
        scl = None
        validate_numeric('c', fltrstr, allow_none=True)
        scale = re.findall('(c_-?\d+(?:\.\d+)?)', fltrstr)
        if "." in fltrstr:
            message = "Contrast scale value should be integer"
            abort(make_response(jsonify(message=message), 400))
            #fabort(400, message)
        if len(scale) > 0:
            scl = float(scale[0].lstrip('c_'))
            if scl != 0 and not scl.is_integer():
                message = "Contrast scale value should be integer"
                abort(make_response(jsonify(message=message), 400))
                #fabort(400, message)
            scl = scl / 100
            if scl == 0:
                scl = 0.0
        if scl is None:
            newimg = img
        else:
            newimg = apply_contrast(img, scl)
    elif 'f_blackwhite' in fltrstr:
        newimg = apply_blackwhite(img)
    elif 'f_pureblackwhite' in fltrstr:
        thr = None
        validate_numeric('t', fltrstr, allow_none=True)
        thresh = re.findall('(t_-?\d+(?:\.\d+)?)', fltrstr)
        if "." in fltrstr:
            message = "Threshold value should be integer"
            applogger.debug(message)
            abort(make_response(jsonify(message=message), 400))
            #fabort(400, message)
            
        if len(thresh) > 0:
            thr = float(thresh[0].lstrip('t_'))
            if thr <= 0:
                message = "Threshold value should be positive"
                abort(make_response(jsonify(message=message), 400))
                #fabort(400, message)
        if not thr:
            newimg = img
        else:
            newimg = apply_pureblackwhite(img, thr)
    elif 'f_layer' in fltrstr:
        text = ""
        position = [0,0]
        rotation = 0
        opacity = 255
        color = (255, 255, 255, opacity)
        if img:
            fontsize = round(img.size[1]*0.03)
        else:
            fontsize = 16
        textres = re.findall('t_[0-9\w\s]+', fltrstr)
        if len(textres) > 0:
            text = textres[0][2:]
        else:
            message = "No text provided"
            abort(make_response(jsonify(message=message), 400))
            #fabort(400, message)

        if len(text) >= MAX_TEXT:
            message = f"Text too long, max text length - {MAX_TEXT}"
            abort(make_response(jsonify(message=message), 400))
            #fabort(400, message)
        for v in ['r', 'x', 'y', 'o', 'fs']:
            validate_numeric(v, fltrstr, allow_none=True)
            
        xres = re.findall('(x_-?\d+(?:\.\d+)?)', fltrstr)
        if "." in xres:
            message = "X position value should be integer"
            abort(make_response(jsonify(message=message), 400))
            #fabort(400, message)
        if len(xres) > 0:
            position[0] = int(xres[0].lstrip('x_'))
            
        yres = re.findall('(y_-?\d+(?:\.\d+)?)', fltrstr)
        if "." in yres:
            message = "Y position value should be integer"
            abort(make_response(jsonify(message=message), 400))
            #fabort(400, message)
        if len(yres) > 0:
            position[1] = int(yres[0].lstrip('y_'))
            
        rotres = re.findall('(r_-?\d+(?:\.\d+)?)', fltrstr)
        if "." in rotres:
            message = "Rotation value should be integer"
            abort(make_response(jsonify(message=message), 400))
            #fabort(400, message)
        if len(rotres) > 0:
            rotation = int(rotres[0].lstrip('r_'))
        opres = re.findall('(o_-?\d+(?:\.\d+)?)', fltrstr)
        if "." in opres:
            message = "Opacity value should be integer"
            abort(make_response(jsonify(message=message), 400))
            #fabort(400, message)
        if len(opres) > 0:
            opacity = int(255 * (int(opres[0].lstrip('o_'))/100))
        color = get_color(fltrstr)
        applogger.debug(color)
        color = (color[0], color[1], color[2], opacity)
        fsizeres = re.findall('(fs_-?\d+(?:\.\d+)?)', fltrstr)
        if "." in fsizeres:
            message = "Font size value should be integer"
            abort(make_response(jsonify(message=message), 400))
            #fabort(400, message)
        if len(fsizeres) > 0:
            fontsize = int(fsizeres[0].lstrip('fs_'))
            if fontsize < 0:
                message = "Font size should be positive"
                abort(make_response(jsonify(message=message), 400))
                #fabort(400, message)
        
        font = TEXT_FONT
        
        newimg = apply_addlayer(img,\
                                text,\
                                position,\
                                rotation,\
                                color,\
                                opacity,\
                                font,\
                                fontsize, shadow=True)
    if "f_padding" in fltrstr:
        dimensions = get_dimensions(fltrstr, getfloat=True)
        for d in dimensions.keys():
            if not dimensions[d].is_integer():
                message = f"{d} value should be integer"
                abort(make_response(jsonify(message=message), 400))
                #fabort(400, message)
        color = get_color(fltrstr)
        newimg = apply_padding(img, dimensions, color)
        
    if 'f_outformat' in fltrstr:
        outformat = fltrstr.split("-")[-1]
        if outformat not in SUPPORTED_FORMATS:
           message="Supported formats are {}".format(", ".join(SUPPORTED_FORMATS))
           abort(make_response(jsonify(message=message), 400))
           #fabort(400, message)
    # If there's still no image generated and img is passed  
    if img and not newimg:
       if not 'f_outformat' in fltrstr:
          applogger.debug([fltrstr])
          message = f'Filter {fltrstr} cannot be found'
          abort(make_response(jsonify(message=message), 404))
          #fabort(400, message)#make_response(jsonify(message=message), 400))
       else:
          newimg = img
    if parent_id:
        newimg.parent_id = parent_id
    return newimg

def apply_single_chain(chainstr, img=None):
    # if no image instance passed, just validate filter parameters and throw errors
    if img:
        applogger.debug(f'Applying single chain {chainstr} {img}')
    else:
        applogger.debug(f'Validating filter parameters')
        
    applogger.debug(["APPLY SINGLE CHAIN", img])
    fltrs = chainstr.split(',')
    applogger.debug(["FILTERS TO APPLY", fltrs])

    for fltr in fltrs:
        if len(fltr) > 0:
            img = apply_filter(fltr, img)
    return img

def apply_chains(chains, img):
    """
    A function to recursively apply 
    filter chains to the image file
    """
    chain = chains.pop(0)
    processed_image = apply_single_chain(chain, img)
    if len(chains) > 0:
        processed_image = apply_chains(chains, processed_image)
    return processed_image


@app.route('/api/v1/screenshots/<path:filterstring>')
@app.route('/api/v2/screenshots/<path:filterstring>')
#@cache.cached(timeout=300, key_prefix=cache_key)
def webscreenshots(filterstring, imgfilters=None):
    """Web Screenshots API
    ---
    tags: [Web Screenshots,]
    parameters:
      - name: filterstring
        in: path
        type: string
        required: true
        description: Parameters to be passed to the Webscreenshots API, w_[width, INT]-h_[height, INT]-url_[TEXT]-scr_[scroll to, INT or FLOAT, 0-100]-nocache. <br />\
    -nocache option forces the app to rewrite the previous cached screenshot with the new one<br>\
    -scr_[scroll to, INT or FLOAT, 0-100] Page scroll down parameter. -scr_10 scrolls down 10 percents of page width <br> The required parameters are url, w and h. The \
    scr parameter is optional
    definitions:
      ImageObject:
        type: object
        description: Screenshot file
    responses:
      200:
        description: Image object to be used as a src for the HTMLimg tag
        schema:
          $ref: '#/definitions/ImageObject'
      400:
        description: Incorrect parameter value
      404:
        description: Missing filterstring parameters
    """

    if len(filterstring) == 0:
       message = "Page not found."
       abort(make_response(jsonify(message=message), 404))
       
    scrollto = False
    origfltrstr = filterstring
    filterslist = []
    spliturl(filterstring, filterslist)
    if len(filterslist) > 0:
        imgfilters = "/".join(filterslist)
        filterstring = filterstring.replace(imgfilters, '')
        filterstring = filterstring[1:]
    if filterstring.endswith("/"):
        filterstring = filterstring[:-1]

        
    pth = request.full_path.replace("-nocache", '')
    # fix single slash un URL
    bad_url = re.findall('https?:/[^/]', pth)
    if len(bad_url) > 0:
        pth = pth.replace("http:/", 'http://')
        pth = pth.replace("https:/", 'https://')
    newurl = request.host_url+pth[1:]
    imgsource = "screenshots"
    applogger.debug(["Original request", request.host_url, ])
    # validation
    if not "url_" in pth:
       message="No URL found"
       abort(make_response(jsonify(message=message), 404))
       
    url = re.findall('url_.+', pth)
    dim = get_dimensions(filterstring, getfloat=True, allow_none=True)
    if not all(dim.values()):
        message = "w_ or h_ parameters not found"
        abort(make_response(jsonify(message=message), 404))
       
    if not all([d.is_integer() for d in dim.values()]):
        message = f"Dimensions accept only integer values"
        abort(make_response(jsonify(message=message), 400))

    if len(url) > 0:
       url = url[0]
       url = url.lstrip('url_')
    else:
       message="No URL found"
       abort(make_response(jsonify(message=message), 404))
    
    scroll = re.findall('scr_.+?-', pth)
    if len(scroll) > 0:
        scroll = scroll[0][:-1]
        validate_numeric('scr', scroll, allow_none=True)
        scroll = scroll.lstrip("scr_")
        scrollto = float(scroll)
        if scrollto < 0:
            message = 'Scroll value should be positive'
            abort(make_response(jsonify(message=message), 400))
        if scrollto > 100:
            message = 'Scroll value should be between 0 and 100'
            abort(make_response(jsonify(message=message), 400))

    dbimage = db.session.query(Image).filter(Image.url==newurl).filter(Image.source==imgsource).first()
    applogger.debug(["DBIMAGE WSCR", dbimage, imgsource, newurl])
    if dbimage:
        applogger.debug(["DBIMAGE WSCR", dbimage.path])
        if not "-nocache" in request.path:
            if imgfilters:
                filename = dbimage.path.replace(imgsource,'')
                commonpath = "/".join([imgsource, imgfilters, filename])
                return process_path(commonpath)
            redirect_url = create_minio_url(dbimage.path)
            applogger.debug(["DBIMAGE WSCR", redirect_url])
            return serve_url(redirect_url)
            #return redirect(redirect_url, code=302)
    else:
        applogger.debug(f'redirecting to {newurl}')
        dbimage = Image(url=newurl, source=imgsource)
        
    newfname = str(uuid.uuid4())+".png"
    img_mime_type = mimetypes.guess_type(newfname)[0]
    newpath = os.path.join(IMGPATH, imgsource, newfname)
    dbimage.path = os.path.join(imgsource, newfname)
    
    bdir = os.path.dirname(newpath)
    driver = create_chrome_instance()
    applogger.debug(["DRIVER", driver])
    driver.set_window_size(int(dim['w']), int(dim['h']))
    
    try:
       applogger.debug("Check URL availability")
       session = requests.Session()
       session.mount('https://', TLSAdapter())
       session.head(url)
    except Exception as e:
       if hasattr(e, 'message'):
          applogger.debug(e.message)
       else:
          applogger.debug(e)
       message = "Incorrect URL"
       abort(make_response(jsonify(message=message), 400))
       
    applogger.debug("GET URL chrome")
    driver.get(url)
    #    #return make_response(jsonify(message="Page not found"), 404)
    
    time.sleep(1)
    if scrollto:
        applogger.debug("SCROLL")
        pageheight = driver.execute_script("return document.body.scrollHeight")
        scrollvalue = (pageheight * scrollto)/100
        driver.execute_script(f"window.scrollTo(0, {scrollvalue})")

    link_buttons = driver.find_elements_by_xpath("//a[contains(text(), 'Cookies') or contains(text(), 'Ok')]")
    
    for l in link_buttons:
        applogger.debug([l.text, l.tag_name])
        try:
            l.click()
        except:
            pass
    tf = tempfile.NamedTemporaryFile()
    applogger.debug(["SAVING AS", tf.name])
    driver.save_screenshot(tf.name)
    applogger.debug(["Closing webdriver", driver.service.process.pid])
    driver.close()
    driver.quit()
    tf.seek(0)
    
    filehash = xxhash.xxh64()
    with open(tf.name, 'rb') as nf:
        filehash.update(nf.read())
    hashstr = filehash.hexdigest()
    dbimage.hashstr = hashstr
    buf = BytesIO(tf.read())
    tf.seek(0)
    wbpath = pth.replace("/api/v2/", "")
    wbpath = wbpath.replace("/api/v1/", "")
    # Move to background
    buf_str = base64.b64encode(buf.read()).decode('ascii')
    minio_upload.delay(newpath, buf_str, os.stat(tf.name).st_size, img_mime_type, wbpath)
    
    applogger.debug("NEWDBIMAGE: {} {}".format(dbimage.url, dbimage.source))
    db.session.add(dbimage)
    db.session.commit()
    applogger.debug(f'redirecting to {newurl}')
    applogger.debug("WEBSCREENSHOTS {} {} {} {} {}".format( pth, dim, url, newpath, newurl))
    
    if imgfilters:
        applogger.debug(["FILTERS", newfname, imgsource, imgfilters])
        filename = newfname
        filename = filename.replace("-nocache", '')
        commonpath = "/".join([imgsource, imgfilters, filename])
        applogger.debug(["COMMONPATH", commonpath])
        return process_path(commonpath)
    
    return send_file(tf, mimetype=img_mime_type)


@app.route('/<path:commonpath>', methods=['GET'])
#@cache.cached(timeout=300, key_prefix=cache_key, forced_update=update_img_counter)
def process_path(commonpath):
    """ImageFilters API
    ---
    tags: [Image Filters,]
    parameters:
      - name: imgsource
        in: path
        type: string
        enum: ['cloudinary', 'flickr', 'flickrtags', 'google', 'imgur', 'unsplash', 'pexels', 'pixabay', 'api/v1/screenshots', 'api/v2/screenshots']
        required: true
        description: Image source
      - name: output
        in: query
        type: string
        required: false
        enum: ['raw', 'json']
        default: "raw"
        description: Image output. If outout value is "raw", the image would be returned. In case of output=json the response would contain the json data for the particular image
      - name: filename
        in: path
        type: string
        required: true
        description: Image file name
      - name: filterstring
        in: path
        type: string
        required: false
        description: Image filters to be applied to the original image.\
        Filters can have optional parameters.<br />Avaliable output formats (f_outformat) \
        are bmp, jpg, png, gif, webp, j2k <br />\
        Avaliable filters are <br />\
        f_sepia<br />f_grayscale<br />f_blackwhite<br />\
        f_blur-r_[blur radius, FLOAT or INT > 0]<br />f_scale-w_[width, INT or FLOAT, > 0]-h_[height, INT or FLOAT, > 0]<br />f_contrast-c_[contrast, ±INT]<br />f_pureblackwhite-t_[threshold, INT>0]<br />f_outformat-[FORMAT]<br />f_layer-t_[text to be printed, TEXT]-x_[horizontal position, INT]-y_[vertical position, INT]-r_[rotation, INT]-o_[opacity, INT]-c_[text color, HEXCOLOR]-fs_[font size, INT]<br />\
        f_padding-w_[width, INT]-h_[height, INT]-c_[background color, HEXCOLOR]<br />f_fit-w_[width, INT]-h_[height, INT]<br />f_crop-h_[INT]-w_[INT]-g_[north_east, north, north_west, west, south_west, south, south_east, east, center, face, faces]. Provide w(idth) and h(eight) or l(eft), r(ight), t(op) and b(ottom) values for precise cropping. The gravity value is optional, the default value is 'center'<br />f_fill-h_[INT]-w_[INT]-g_[north_east, north, north_west, west, south_west, south, south_east, east, center] <br /> f_show_source_service <br> \
        Each filter can be followed by a -nocache option to overwrite the previous cached image
    definitions:
      WidgetImage:
       type: string
       description: URL of an image for a widget output
    responses:
      200:
        description: Image URL
        schema:
          $ref: '#/definitions/WidgetImage'
      400:
        description: Bad request, in case of a incorrect value passed to an existing filter.
      404:
        description: Not found. Case of incorrect or missing  URL parts like image source, filter name or file name
    """
    minioClient = create_minio_client()
    output_format = request.args.get('output', 'raw')
    version_regex = r"/v\d/"
    VERSION = re.findall(version_regex, request.path)
    if VERSION:
        VERSION = VERSION[-1].replace("/", '')

    commonpath = commonpath.replace(f"api/{VERSION}/", "")
    nocache = False
    
    if "/" in commonpath:
        imgsource, otherpath = commonpath.split("/", 1)
    else:
        imgsource = ""
        otherpath = ""
    
    filterlist = []
    spliturl(otherpath, filterlist)
    filterstring = "/".join(filterlist)
    filename = otherpath.replace(filterstring, '')
    filename = filename.lstrip("/")
    
    if "-nocache" in filterstring:
        nocache = True
        filterstring = filterstring.replace("-nocache", '')

    path = "/".join([imgsource, filterstring, filename])
    chains = fname = fullname = subdir = None
    pth = path.split('/')
    if len(pth) > 2:
        subdir, chains, fname = (imgsource, filterstring.split('/'), filename)
        chainscopy = chains.copy()
    elif len(pth) == 2:
        subdir, fname = (imgsource, filename)
    if fname:
        fullname = os.path.join(IMGPATH, subdir, fname)
    applogger.debug(["COMMONPATH", commonpath, subdir, fname])
    applogger.debug(["REQUEST_PARAMS", request.args, output_format])
    applogger.debug(["PATH", os.path.join(subdir, fname)])
    parentimage = db.session.query(Image).filter(Image.path == os.path.join(subdir, fname)).first()
    if output_format == "json":
        if parentimage:
            res = ImageTagSchema().dump(parentimage).data
            return jsonify(res), 200
        else:
            message = "No Image found"
            return make_response(jsonbify(message=message), 404)
    
    if not parentimage:
        return make_response(jsonify(message="Page not found.."), 404)


    # Story ID
    #story_id = request.args.get('story_id', None)
    #if story_id:
    #    applogger.debug(["STORY ID", story_id])
    #    story = db.session.query(Article).filter(Article.storyid == story_id).first()
    #    if not story:
    #        story = Article(storyid=story_id)
    #        db.session.add(story)
    #        db.session.commit()
    #    if story not in parentimage.articles:
    #        parentimage.articles.append(story)
    #    db.session.add(parentimage)
    #    db.session.commit()
        
    if FACE_DETECT_UPDATE == 'update':
        applogger.debug(["PPATH", parentimage.path])
        minioimage = minioClient.get_object(MINIO_BUCKET_NAME, os.path.join(IMGPATH, parentimage.path))
        with tempfile.NamedTemporaryFile() as f:
            for d in minioimage.stream(32*1024):
                f.write(d)
            applogger.debug("UPDATING FACE DETECT RESULTS")
            buf = BytesIO(f.read())
            # Move to background
            buf_str = base64.b64encode(buf.read()).decode('ascii')
            #print(["BUF_STR", buf_str])
            faces = face_detect.run(None, buf_str,  "faces")

            #faces = face_detect(f.name, "faces")
            parentimage.faces_json  = json.dumps(faces)
            db.session.add(parentimage)
            db.session.commit()
        
    if len(chains) == 1 and "" in chains:
        if parentimage:
            applogger.debug("RETURN ORIGINAL FILE")
            
            if parentimage.source == 'minio':
                redirect_url = create_minio_url(parentimage.path, imgsource=parentimage.source)
            else:
                redirect_url = create_minio_url(parentimage.path)
                
            applogger.debug(["MINIO URL", redirect_url])
            
            return serve_url(redirect_url, path=parentimage.path)
            #return redirect(redirect_url, code=302)
        
    elif all([parentimage, len(chains) > 0, len(chains[0]) > 0]):
        applogger.debug("PROCESSING FILTERS")
        try:
            if parentimage.source == 'minio':
                objname = parentimage.path
            else:
                objname = "{}/{}".format(IMGPATH, parentimage.path)
            applogger.debug(["MINIO Obj name", objname])    
            data = minioClient.get_object(MINIO_BUCKET_NAME, objname)
            
        except:# NoSuchKey:
            #if "/api/v2" in request.path:
            #    data = cache.get("/api/v2/{}?".format(parentimage.path))
            #elif "/api/v1" in request.path:
            resp = cache.get(request.full_path+"?")

            # Trying to return cached data
            # If neither Minio key found and not cached data found
            # return 404
            if not resp:
                # delete parent image record if no minio object found
                applogger.debug(["No Minio, No cache, deleting db rec", parentimage.source, parentimage.path])    
                db.session.delete(parentimage)
                db.session.commit()
                return make_response(jsonify(message="Page not found..."), 404)
            
            data = HTTPResponse(body=BytesIO(resp.data), preload_content=False, status=resp.status_code, headers=resp.headers)
            applogger.debug(["CACHED DATA", data, parentimage.path, request.path])
        with tempfile.NamedTemporaryFile() as f:
            for d in data.stream(32*1024):
                f.write(d)
            f.seek(0)
            with Img.open(f.name) as img:
                # generate new filepath & new file URL
                # check for output format filters
                # and create the right output filename
                outformat = [c for c in chains if 'f_outformat' in c]
                if outformat:
                    outformat = outformat[0]
                    outformat = outformat.split("-")
                    if len(outformat) > 1:
                        fbasename, _ = os.path.splitext(fname)
                        outext = outformat[-1]
                        if outext in SUPPORTED_FORMATS:
                            fname = f"{fbasename}.{outext}"

                newpath = os.path.join(IMGPATH, subdir, os.sep.join(chains))
                prevversionnum = chains[-1]
                newversionnum = len(parentimage.versions) + 1
                newfilterpath = os.path.join(subdir, os.sep.join(chains), fname)
                prevversionpath = os.path.join(subdir, prevversionnum, fname)
                newversionpath = os.path.join(subdir, f"v{newversionnum}", fname)
                newversionfullpath = os.path.join(IMGPATH, subdir, f"v{newversionnum}")
                newversionfullfilepath = os.path.join(newversionfullpath, fname)
                
                newurl = '/'.join([IMGPATH, subdir, os.sep.join(chains), fname])
                newversionurl = '/'.join([IMGPATH, subdir, f"v{newversionnum}", fname])
                newfname = os.path.join(newpath, fname)
                applogger.debug(["chains", chains])
                prevfilterversion = db.session.query(ImageVersion).filter(ImageVersion.image_id == parentimage.id).filter(ImageVersion.filterpath == newfilterpath).first()
                if prevfilterversion:
                    applogger.debug("PREV FILTERS REDIRECT")
                    newurl = create_minio_url(prevfilterversion.filterpath)
                    return serve_url(newurl)
                    #return requests.get(newurl).content
                    #return redirect(newurl, code=302)
                    
                prevnumversion = db.session.query(ImageVersion).filter(ImageVersion.image_id == parentimage.id).filter(ImageVersion.versionpath == prevversionpath).first()
                if prevnumversion:
                    applogger.debug("PREVNUM FILTERS REDIRECT")
                    newurl = create_minio_url(prevnumversion.versionpath)
                    return serve_url(newurl)
                    #return requests.get(newurl).content
                    #return redirect(newurl, code=302)
                
                # apply chains
                # Adding new attr to the img,
                # as long as now we don't store it locally
                img.parent_id = parentimage.id
                applogger.debug(["APPLY CHAINS", img.filename])
                #applogger.debug(["IMG", dir(img)])
                final_img = apply_chains(chains, img)
                
                # save file
                # check extension
                bd, extns = os.path.splitext(newfname)
                #if len(extns) == 0:
                #    final_img.save(newfname, 'PNG')
                #    final_img.save(newversionfullfilepath, 'PNG')
                #else:
                extns = extns.lstrip('.')
                #if extns not in SUPPORTED_FORMATS:
                #    # replace with minio
                #    final_img.save(newfname, 'PNG')
                #    final_img.save(newversionfullfilepath, 'PNG')
                #else:
                    # replace with minio
                if extns == "jpg":
                    frmt = "JPEG"
                elif extns == "webp":
                    frmt = "WebP"
                else:
                    frmt = extns.upper()
                            
                applogger.debug(["EXTNS", extns])
                
                if final_img.mode != "RGB":
                    final_img = final_img.convert("RGB")
                final_img.save(f.name, frmt)
                img_mime_type = mimetypes.guess_type(fname)[0]
                
                buf = BytesIO(f.read())
                        
                #final_img.save(newversionfullfilepath)
                applogger.debug('redirecting')
                newfilterpath = newfilterpath.replace("//", "/")
                newversionpath = newversionpath.replace("//", "/")
                newurl = newurl.replace("//", '/')
                # Versions
                applogger.debug(["Minio upload", newurl, newversionurl])
                newversion = ImageVersion(image_id = parentimage.id, version=newversionnum, versionpath = newversionpath, filterpath=newfilterpath)
                # Move to background
                buf_str = base64.b64encode(buf.read()).decode('ascii')
                minio_upload.delay(newurl, buf_str, os.stat(f.name).st_size, img_mime_type, newversion.filterpath)
                buf.seek(0)
                # Move to background
                
                minio_upload.delay(newversionurl, buf_str, os.stat(f.name).st_size, img_mime_type, newversion.versionpath)
                db.session.add(newversion)
                db.session.commit()
                applogger.debug(["New image version saved", newversion.filterpath])
                # redirect to the saved file
                applogger.debug(["FILTERS REDIRECT", newurl])
                #tf = tempfile.NamedTemporaryFile()
                
                return Response(buf.getvalue(), mimetype=img_mime_type)
                #newurl = create_minio_url(newversion.filterpath)
                #return serve_url(newurl)
                #return redirect(newurl, code=302)
    else:
        return make_response(jsonify(message="Page not found...."), 404)
    return f'filters: {chainscopy}, filename:{fullname}, subdir: {subdir}'


@app.route('/api/v1/updatearticle', methods=['POST'])
@app.route('/api/v2/updatearticle', methods=['POST'])
@cross_origin(supports_credentials=True)
def update_article_info():
    """UpdateArticle API
    ---
    tags: [Articles,]
    parameters:
      - in: body
        name: tags
        type: array
        required: true
        description: A list of image tags to link with the image
        example: ["tag1", "tag2", "tag3"]
        items:
          type: string
          description: Tag text
      - name: story_id
        in: body
        type: integer
        required: true
        description: Story ID
        example: 1
      - name: limit
        in: body
        type: integer
        required: false
        default: 1
        example: 1
        description: Results limit
      - in: body
        name: filters
        type: string
        example: f_sepia,f_grayscale
        required: false
        description: Image filters to be applied to the original image. 
    responses:
      200:
        description: Input data accepted, background task started
      400:
        description: Bad request, in case of a incorrect value passed to an existing filter.
    """
    
    applogger.debug(["UPDATE ARTICLE", request.json])
    tags = request.json.get('tags', None)
    story_id = request.json.get('story_id', None)
    limit = request.json.get('limit', None)
    filters = request.json.get('filters', None)
    if not limit:
        request.json['limit'] = 1
    # get results in background
    # link to the article id
    if not all([tags, story_id]):
        message='Not enough parameters. Provide tags and story_id'
        return make_response(jsonify(message=message), 400)
    if len(tags) == 0:
        message='Not enough tags'
        return make_response(jsonify(message=message), 400)


    # Replaced with celery
    article_update.delay(story_id, tags, filters, request.host, request.path)
    #@copy_current_request_context
    #def background_search():
        # FIX for missing request.host
    #    request.url = request.url.replace("http://", "https://")
    #    request.host_url = request.url
    #    p = urlparse(request.url)
    #    request.host_url = "{}://{}/".format(p.scheme, p.netloc)
    #    request.host = p.netloc
        
    #    applogger.debug(["URL1", request.url])
    #    #applogger.debug(["URL1", dir(request)])
    #    applogger.debug(["URL1", request.host_url])
    #    applogger.debug(["URL1", request.host])
    #    applogger.debug(["URL1", request.json.get("story_id")])
    #    story_id = request.json.get("story_id")
    #    resp = ImageV2API().get()
    #    applogger.debug(["UPD RESP", resp])
    #    if len(resp) > 0:
    #        if len(resp[0]['data']) > 0:
    #            image_url = resp[0]['data'][0]['url']
    #            applogger.debug(["URL2", image_url])
    #            applogger.debug(["URL2", ARTICLES_API.format(story_id), ARTICLES_USERNAME, ARTICLES_PASSWORD])

    #            article_resp = requests.put(ARTICLES_API.format(story_id), auth=(ARTICLES_USERNAME, ARTICLES_PASSWORD), json={"thumb":image_url})
    #            applogger.debug(["ARTICLE API RESP", article_resp.status_code, article_resp.text])
    #    return resp
        
    #th = threading.Thread(target=background_search)
    #th.daemon = True
    #th.start()
    return make_response(jsonify(message="Data Accepted"), 200)


# Images
api.add_resource(ImageV1API, '/api/v1/images',  endpoint='images1')
api.add_resource(ImageV2API, '/api/v2/images', endpoint='images2')
api.add_resource(ImageVoteAPI, '/api/v1/images/<id>', '/api/v2/images/<id>', endpoint='imagespatch')
api.add_resource(ImageUploadAPI, '/api/v1/imageupload', '/api/v2/imageupload', endpoint='imagesupload')
api.add_resource(ImageDeleteAPI, '/api/v1/images', '/api/v2/images', endpoint='imagesdelete')

# Tags
api.add_resource(TagsV2API, '/api/v2/tags', endpoint='tagsv2')
api.add_resource(SingleTagV2API, '/api/v2/tags/<id>', endpoint='singletagv2')
api.add_resource(TagsPostAPI, '/api/v2/tags', endpoint='tags_post_v2')
api.add_resource(TagsV1API, '/api/v1/tags', endpoint='tagsv1')
api.add_resource(SingleTagV1API, '/api/v1/tags/<id>', endpoint='singletagv1')
api.add_resource(TagsPostAPI, '/api/v1/tags', endpoint='tags_post_v1')

# Blacklists
api.add_resource(AuthorBlacklistAPI, '/api/v2/blockedauthors', endpoint='blockedauthorsv2')
api.add_resource(SourceBlacklistAPI, '/api/v2/blockedsources', endpoint='blockedsourcesv2')

@app.cli.command()
@click.option('--login',  help='user@mail.com')
@click.option('--password',  help='password')
def adduser(login, password):
    """ Create new user"""
    newuser = User(login=login)
    newuser.hash_password(password)
    newuser.is_confirmed = True
    newuser.confirmed_on = datetime.datetime.today()
    db.session.add(newuser)
    db.session.commit()
    applogger.debug(["New user added", newuser])

    
if __name__ == '__main__':
    app.run()
