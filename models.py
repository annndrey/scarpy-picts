#!/usr/bin/python
# -*- coding: utf-8 -*-


from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import CheckConstraint, ForeignKey
from sqlalchemy.orm import backref, validates, relationship
from sqlalchemy.ext.hybrid import hybrid_property
from itsdangerous import URLSafeSerializer, BadSignature, SignatureExpired
from passlib.apps import custom_app_context as pwd_context
import datetime
import enum
import jwt
from flask import current_app, jsonify

db = SQLAlchemy()

img_tags = db.Table('img_tags', db.Model.metadata,
                    db.Column('image_id', db.Integer, ForeignKey('image.id')),
                    db.Column('tag_id', db.Integer, ForeignKey('tag.id')),
                    db.PrimaryKeyConstraint('image_id', 'tag_id'))

img_articles = db.Table('img_articles', db.Model.metadata,
                    db.Column('image_id', db.Integer, ForeignKey('image.id')),
                    db.Column('article_id', db.Integer, ForeignKey('article.id')),
                    db.PrimaryKeyConstraint('image_id', 'article_id'))


class OrientationType(enum.Enum):
    # portrait
    portrait = "portrait"
    # landscape
    landscape = "landscape"
    

class TagTranslation(db.Model):
    __tablename__ = "tag_translation"
    id = db.Column(db.Integer, primary_key=True)
    tag = relationship('Tag', backref='translation')
    tag_id = db.Column(db.Integer, ForeignKey('tag.id'), nullable=False)
    translation = db.Column(db.String(600))


class Counter(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tag = relationship('Tag', backref='counter')
    image_id = db.Column(db.Integer, ForeignKey('image.id'), nullable=True)
    tag_id = db.Column(db.Integer, ForeignKey('tag.id'), nullable=True)
    counter = db.Column(db.Integer, default=0)
    
    
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(400))
    password_hash = db.Column(db.String(400))
    is_confirmed = db.Column(db.Boolean, default=False)
    registered_on = db.Column(db.DateTime, default=datetime.datetime.today)

    @validates('login')
    def validate_login(self, key, login):
        if len(login) > 1:
            assert '@' in login, 'Invalid email'
        return login
    
    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    #def generate_auth_token(self):
    #    token = jwt.encode({
    #    'sub': self.login,
    #    'iat': datetime.datetime.utcnow(),
    #    'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=10)},
    #    current_app.config['SECRET_KEY'])
    #    return token

    
class Image(db.Model):
    __tablename__ = 'image'
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.Text())
    source_url = db.Column(db.Text())
    path = db.Column(db.String(200), unique=True)
    versions = relationship("ImageVersion", backref="image")
    source = db.Column(db.String(200))
    tags = relationship('Tag', backref='images', secondary=img_tags)
    downloaded =db.Column(db.Boolean, default=False)
    status = db.Column(db.String(200))
    width = db.Column(db.Integer)
    height = db.Column(db.Integer)
    orientation = db.Column(db.Enum(OrientationType))
    location_id = db.Column(db.Integer, ForeignKey('location.id'))
    hashstr = db.Column(db.String(200))
    imgcredits = db.Column(db.String(600))
    author = db.Column(db.Text())
    score = db.Column(db.Integer, default=0)
    faces_json = db.Column(db.Text())
    publisher = db.Column(db.String(200))
    license = db.Column(db.Text())
    descr = db.Column(db.Text())
    articles = relationship('Article', backref='images', secondary=img_articles)
    imagecounter = relationship('Counter', backref='imagecounter')
    
    @hybrid_property
    def tagstxt(self):
        return [t.txt for t in self.tags]

    @hybrid_property
    def articleids(self):
        return [a.storyid for t in self.articles]
    
    @hybrid_property
    def generated_credits(self):
        if not self.imgcredits:
            src = self.source
            if src.startswith("flickr"):
                src = "flickr"
            return "Photo by {}, {}".format(self.author, src.capitalize())
        else:
           return self.imgcredits

       
class ImageVersion(db.Model):
    __tablename__ = 'imageversion'
    id = db.Column(db.Integer, primary_key=True)
    version = db.Column(db.Integer)
    versionpath = db.Column(db.String(255), unique=True)
    filterpath = db.Column(db.String(255), unique=True)
    image_id = db.Column(db.Integer, ForeignKey('image.id'))

    
class Tag(db.Model):
    __tablename__ = 'tag'
    id =  db.Column(db.Integer, primary_key=True)
    txt = db.Column(db.String(255), unique=True)
    tagcounter = relationship('Counter', backref='tagcounter')

    
class Article(db.Model):
    __tablename__ = 'article'
    id = db.Column(db.Integer, primary_key=True)
    storyid = db.Column(db.Integer, unique=True)
    

class Location(db.Model):
    __tablename__ = 'location'
    id = db.Column(db.Integer, primary_key=True)
    lat = db.Column(db.String(200))
    lon = db.Column(db.String(200))
    addr = db.Column(db.String(200))
    zipcode = db.Column(db.String(200))
    city = db.Column(db.String(200))
    images = relationship("Image", backref="location")


class AuthorBlacklist(db.Model):
    __tablename__ = 'authorblacklist'
    id = db.Column(db.Integer, primary_key=True)
    author = db.Column(db.String(255), unique=True)


class SourceBlacklist(db.Model):
    __tablename__ = 'sourceblacklist'
    id = db.Column(db.Integer, primary_key=True)
    source = db.Column(db.String(255), unique=True)
    
