from filters import app

# gunicorn --bind localhost:8084 wsgi:app
if __name__ == "__main__":
    app.run()
    
