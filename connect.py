#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

dbstring = os.environ.get('DBCONF')
mediadir = os.environ.get('MEDIADIR')
# db connect
engine = create_engine(dbstring, echo=False)
Base.metadata.create_all(engine, checkfirst=True)
session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)
session = Session()

