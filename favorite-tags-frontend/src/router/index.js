import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/components/MainPage'
import UploadPage from '@/components/UploadPage'
import Login from '@/components/Login'
import Logout from '@/components/Logout'
Vue.use(Router)


export default new Router({
    mode: 'history',
    base: '/frontend',
    routes: [
	{
	    path: '/',
	    name: 'MainPage',
	    component: MainPage
	},
	{
	    path: '/upload',
	    name: 'UploadPage',
	    component: UploadPage
	},
	{
	    path: '/login',
	    name: 'Login',
	    component: Login
	},
	{
	    path: '/logout',
	    name: 'Logout',
	    component: Logout
	}
    ]
})
