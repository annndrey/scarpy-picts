const axiosConfig = {
    timeout: 30000,
    withCredentials: false
};

import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueFlashMessage from 'vue-flash-message';
import { VLazyImagePlugin } from "v-lazy-image";
import router from './router'
import store from './store'

var VueCookie = require('vue-cookie');
Vue.config.productionTip = false
Vue.prototype.$axios = axios.create(axiosConfig);
Vue.prototype.$axios.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem('token')}`;
//Vue.prototype.$backendhost = 'https://test.image-finder.inquence.com/api/v2/'
Vue.prototype.$backendhost = 'https://testapi.me:5467/api/v2/'
const moment = require('moment')
require('vue-flash-message/dist/vue-flash-message.min.css');
Vue.use(require('vue-moment'), {
    moment
})
Vue.use(VueFlashMessage, { timeout: 2000 });
Vue.use(VueCookie);
Vue.use(VLazyImagePlugin);

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
}).$mount('#app')

