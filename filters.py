#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import logging
from flask import Flask, abort, redirect, request
from PIL import Image, ImageOps, ImageFilter, ImageEnhance, ImageDraw, ImageFont


logging.basicConfig(format='%(levelname)s: %(asctime)s - %(message)s',
                    level=logging.DEBUG, datefmt='%d.%m.%Y %I:%M:%S %p')

app = Flask(__name__)
app.config.from_envvar('APPSETTINGS')
IMGPATH = app.config.get('IMGPATH')
MAX_RESIZE = app.config.get('MAX_RESIZE')
MAX_TEXT = app.config.get('MAX_TEXT')

SUPPORTED_FORMATS = ['bmp', 'jpg', 'png', 'gif', 'webp', 'j2k']

def make_linear_ramp(white):
    ramp = []
    r, g, b = white
    for i in range(255):
        ramp.extend((int(r*i/255), int(g*i/255), int(b*i/255)))
    return ramp

def get_color(fltrstr):
    # TODO add standart
    # black by default
    rgb = (0,0,0)
    colres = re.findall('c_[0-9a-fA-F]+', fltrstr)
    if len(colres) > 0:
        hexcolor = colres[0].lstrip('c_')
        rgb = tuple(int(hexcolor[i:i+2], 16) for i in (0, 2, 4))
    return rgb

def get_dimensions(fltrstr, getfloat=False):
    dimensions = {'w': None, 'h': None}
    for dim in dimensions:
        if getfloat:
            res = re.findall(f'({dim}_\d+(?:\.\d+)?)', fltrstr)
        else:
            res = re.findall(f'{dim}_[0-9]+', fltrstr)
        if len(res) > 0:
            if getfloat:
                res = float(res[0].lstrip(dim+"_"))
            else:
                res = int(res[0].lstrip(dim+"_"))
            dimensions[dim] = res
        
    return dimensions

def get_resize_factor(dim, img):
    factor = 0
    if dim['w'] and not dim['h']:
        if dim['w'].is_integer() and dim['w'] > 1:
            factor = dim['w'] / img.width
        elif dim['w'] <= 1:
            factor = dim["w"]
    elif dim['h'] and not dim['w']:
        if dim['h'].is_integer() and dim['h'] > 1:
            factor = dim['h'] / img.height
        elif dim['h'] > 1:
            factor = dim["h"]
    return factor

def apply_padding(img, dim, bgcolor):
    """
    Padding image to desired shape and keep its aspect ratio
    """
    logging.debug("PADDING FILTER")
    x, y = img.size
    ratio = x / y
    desired_ratio = dim['w']/dim['h']
    w = max(dim['w'], x)
    h = int(w / desired_ratio)
    if h < y:
        h = y
        w = int(h * desired_ratio)

    newimg = Image.new('RGB', (w, h), bgcolor)
    newimg.paste(img, ((w - x) // 2, (h - y) // 2))
    newimg = newimg.resize((dim['w'], dim['h']))

    return newimg

def apply_addlayer(img, text, position, rotation, color, opacity, font, fontsize):
    """
    Add text layer to the image
    Has the following parameters: 
    text: Any text incl spaces
    position: integer
    rotation: integer
    color: Any HEX color
    opacity: integer 0-100
    font: Now only one
    fontsize: integer
    """
    if len(text) >= MAX_TEXT:
        logging.debug("Text too long")
        return
    img = img.convert("RGBA")
    txt = Image.new('RGBA', img.size, (255,255,255,0))
    
    draw = ImageDraw.Draw(txt)
    font = ImageFont.truetype(font, size=fontsize)
    draw.text(position, text, fill=color, font=font)
    txt = txt.rotate(rotation)
    img = Image.alpha_composite(img, txt)
    img = img.convert("RGB")
    return img


def apply_sepia(img):
    """
    Apply sepia filter
    """
    if img.mode != "L":
        newimg = img.convert("L")
    else:
        newimg = img
    sepia = make_linear_ramp((255, 240, 192))
    newimg = ImageOps.autocontrast(newimg)
    newimg.putpalette(sepia)
    newimg = newimg.convert("RGB")
    return newimg

def apply_grayscale(img):
    """
    Apply grayscale filter
    """
    newimg = img.convert('LA')
    newimg = newimg.convert("RGB")
    newimg = ImageOps.autocontrast(newimg)
    return newimg

def apply_resize(img, dim):
    """
    Resize function. It can take at least one parameter:
    w or h, for width and height respectively.
    If w or h is a float and is less than 1, then
    the result is scaled by the given value. 
    w_0.25 is scaled to a width of 25%.
    The filter string can be as follows:
    f_scale-h480-w600
    The "-" separator can be any character except ',', 
    The comma is reserved as a filter separator.
    """
    factor = None
    newimg = None
    too_large = False
    too_small = False
    
    if any([v >= MAX_RESIZE for v in dim.values() if v is not None]):
        too_large = True
        
    if any([v < 0.001 for v in dim.values() if v is not None]):
        too_small = True
        
    if not too_large and not too_small:
        factor = get_resize_factor(dim, img)
        if all(dim.values()):
            newimg = img.resize((dim['w'], dim['h']))
        elif any(dim.values()):
            newimg = img.resize((int(img.width * factor), int(img.height * factor)))
    
        return newimg

def apply_blur(img, radius=5):
    """
    Gaussian blur filter with
    radius parameter.
    Example: f_blur-r20
    If no radius provided, the default r5 value is used
    """
    newimg = img.filter(ImageFilter.GaussianBlur(radius=radius))
    return newimg

def apply_contrast(img, scale=5):
    """
    Contrast filter with scale parameter
    Example: f_contrast-c30
    If no scale value provided, the default c5 is used
    """
    newimg = ImageEnhance.Contrast(img).enhance(scale)
    return newimg

def apply_blackwhite(img):
    newimg = img.convert('1')
    newimg = newimg.convert("RGB")
    return newimg

def apply_pureblackwhite(img, thresh=200):
    fn = lambda x : 255 if x > thresh else 0
    newimg = img.convert('L').point(fn, mode='1')
    newimg = newimg.convert("RGB")
    return newimg


def apply_filter(fltrstr, img):
    """
    Filter string parser
    Finds an appropriate filter, parses it's parameters and passes it to 
    the respcetive filter function.
    """
    logging.debug(f'parsing & applying a single filter {fltrstr} {img}')
    newimg = None
    if 'f_sepia' in fltrstr:
        newimg = apply_sepia(img)
    elif 'f_grayscale' in fltrstr:
        newimg = apply_grayscale(img)
    elif 'f_scale' in fltrstr:
        dimensions = get_dimensions(fltrstr, getfloat=True)
        newimg = apply_resize(img, dimensions)
    elif 'f_blur' in fltrstr:
        rad = None
        radius = re.findall('r_[0-9]+', fltrstr)
        if len(radius) > 0:
            rad = int(radius[0].lstrip('r_'))
        if not rad:
            newimg = apply_blur(img)
        else:
            newimg = apply_blur(img, rad)
    elif 'f_contrast' in fltrstr:
        scl = None
        scale = re.findall('c_-?[0-9]+', fltrstr)
        if len(scale) > 0:
            scl = int(scale[0].lstrip('c_'))
        if not scl:
            newimg = apply_contrast(img)
        else:
            newimg = apply_contrast(img, scl)
    elif 'f_blackwhite' in fltrstr:
        newimg = apply_blackwhite(img)
    elif 'f_pureblackwhite' in fltrstr:
        thr = None
        thresh = re.findall('t_[0-9]+', fltrstr)
        if len(thresh) > 0:
            thr = int(thresh[0].lstrip('t'))
        if not thr:
            newimg = apply_pureblackwhite(img)
        else:
            newimg = apply_pureblackwhite(img, thr)
    elif 'f_layer' in fltrstr:
        text = ""
        position = [0,0]
        rotation = 0
        opacity = 255
        color = (255, 255, 255, opacity)
        fontsize = 16
        
        textres = re.findall('t_[0-9\w\s]+', fltrstr)
        print(fltrstr)
        if len(textres) > 0:
            text = textres[0].lstrip('t_')
            
        xres = re.findall('x_-?[0-9]+', fltrstr)
        if len(xres) > 0:
            position[0] = int(xres[0].lstrip('x_'))
        yres = re.findall('y_-?[0-9]+', fltrstr)
        if len(yres) > 0:
            position[1] = int(yres[0].lstrip('y_'))
        rotres = re.findall('r_-?[0-9]+', fltrstr)
        if len(rotres) > 0:
            rotation = int(rotres[0].lstrip('r_'))
        opres = re.findall('o_?[0-9]+', fltrstr)
        if len(opres) > 0:
            opacity = int(255 * (int(opres[0].lstrip('o_'))/100))
        color = get_color(fltrstr)
        color = (color[0], color[1], color[2], opacity)
        fsizeres = re.findall('fs_?[0-9]+', fltrstr)
        if len(fsizeres) > 0:
            fontsize = int(fsizeres[0].lstrip('fs_'))
        
        font = "/usr/share/fonts/truetype/liberation/LiberationSans-Regular.ttf"
        
        newimg = apply_addlayer(img,\
                                text,\
                                position,\
                                rotation,\
                                color,\
                                opacity,\
                                font,\
                                fontsize)
        # TODO from file
    if "f_padding" in fltrstr:
        dimensions = get_dimensions(fltrstr, getfloat=False)
        color = get_color(fltrstr)
        newimg = apply_padding(img, dimensions, color)
        
    #elif 'f_outformat' in fltrstr:
    #    outformat = fltrstr.split("-")[-1]
    #    if outformat in SUPPORTED_FORMATS:
    #        newimg = apply_convertformat(img, outformat)
            
    if not newimg:
        newimg = img
    return newimg

def apply_single_chain(chainstr, img):
    logging.debug(f'Applying single chain {chainstr} {img}')
    fltrs = chainstr.split(',')
    for fltr in fltrs:
        img = apply_filter(fltr, img)
    return img

def apply_chains(chains, img):
    """
    A function to recursively apply 
    filter chains to the image file
    """
    chain = chains.pop(0)
    processed_image = apply_single_chain(chain, img)
    if len(chains) > 0:
        processed_image = apply_chains(chains, processed_image)
    return processed_image
        

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def process_path(path):
    """
    The main function to parse the route path
    and apply specified filters. 
    If no path exists, create new subdirectories
    and the processed file. 
    Returns the new URL for processed image.
    
    Filters can be chained together. For now
    the chain limit is 3. 

    The filters are applied in order of appearance.
    """
    chains = fname = fullname = subdir = None

    pth = path.split('/')
    print(pth)
    if len(pth) > 2:
        subdir, chains, fname = (pth[0], pth[1:-1], pth[-1])
        chainscopy = chains.copy()
    elif len(pth) == 2:
        subdir, fname = pth
    if fname:
        fullname = os.path.join(IMGPATH, subdir, fname)
    if fullname and os.path.exists(fullname):
        if os.stat(fullname).st_size == 0:
            os.remove(fullname)
            abort(404, 'Zero-sized file')
        # open original file
        with Image.open(fullname) as img:
            # generate new filepath & new file URL
            # check for output format filters
            # and create the right output filename
            outformat = [c for c in chains if 'f_outformat' in c]
            if outformat:
                outformat = outformat[0]
                outformat = outformat.split("-")
                if len(outformat) > 1:
                    fbasename, _ = os.path.splitext(fname)
                    outext = outformat[-1]
                    if outext in SUPPORTED_FORMATS:
                        fname = f"{fbasename}.{outext}"

            newpath = os.path.join(IMGPATH, subdir, os.sep.join(chains))
            newurl = '/'.join([subdir, os.sep.join(chains), fname])
            newurl = request.host_url+newurl
            if not os.path.exists(newpath):
                os.makedirs(newpath)
            newfname = os.path.join(newpath, fname)
            # apply chains
            final_img = apply_chains(chains, img)
            # save file
            final_img.save(newfname)
            logging.debug('redirecting')
            # redirect to the saved file
            print(newurl)
            return redirect(newurl, code=302)
    else:
        abort(404, 'File not found')
    return f'filters: {chainscopy}, filename:{fullname}, subdir: {subdir}'

if __name__ == '__main__':
    app.run()
