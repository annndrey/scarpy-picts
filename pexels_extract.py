#!/usr/bin/env python
# -*- coding: utf-8 -*-

#from pyvirtualdisplay import Display
#from selenium import webdriver

import time
import json
import os
import sys
import shutil
import requests
import functools

from concurrent.futures import ThreadPoolExecutor

from bs4 import BeautifulSoup
from urllib.parse import urlparse

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from models import Image, Tag
from connect import Session, session, mediadir
from PIL import Image as Img
from sqlalchemy.exc import IntegrityError

#URL = "https://www.pexels.com/new-photos?page={}"
URL = "https://www.pexels.com/popular-photos/all-time?page={}"
PROXY = os.environ.get('PROXYCONF')
IMGPATH = "/media/MEDIA/PICT_DB"

proxy = 'http://{}'.format(PROXY)
PROXIES = {'http': proxy, 'https': proxy}


def setup_profile():
    # selenium setup
    display = Display(visible=0, size=(1600, 900))
    display.start()
    options = Options()
    options.set_headless(headless=True)

    # add proxy conf here
    #webdriver.DesiredCapabilities.FIREFOX['proxy']={
    #    "httpProxy":PROXY,
    #    "ftpProxy":PROXY,
    #    "sslProxy":PROXY,
    #    "proxyType":"MANUAL"
    #}
    
    browser = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver', 
                                firefox_binary='../../../firefox/firefox-bin'
    )

    return browser

def update_image_status(arg1, future):
    response = future.result()
    print(response, arg1)
    if response:
        status, url = response.split()
    else:
        return
    sess = Session()
    image = sess.query(Image).filter(Image.id == arg1).first()
    if image:
        if status == '200':
            filename = urlparse(image.url).path.rsplit("/")[-1]
            image.path = os.path.join(image.source, filename)
            width, height = Img.open(os.path.join(IMGPATH, image.path)).size
            orientation = "l"
            if height >= width:
                orientation = 'p'
            image.width = width
            image.height = height
            image.orientation = orientation

            image.downloaded = True
            sess.add(image)
            sess.commit()
        elif status == '404':
            print(404, url)
            filename = urlparse(image.url).path.rsplit("/")[-1]
            #image.path = os.path.join(image.source, filename)
            image.status = 404
            sess.add(image)
            sess.commit()
        

def getfiles(numthreads=14, source=None):
    with ThreadPoolExecutor(max_workers=numthreads) as executor:
        # TODO remove source
        # or add as a parameter
        
        images = session.query(Image).filter(Image.downloaded == False).filter(Image.status == None)
        if source:
            images = images.filter(Image.source==source)
        images = images.limit(numthreads).all()
        for img in images:
            filename = urlparse(img.url).path.rsplit("/")[-1]
            if not os.path.exists(os.path.join(IMGPATH, img.source, filename)):
                future = executor.submit(download_file, img)
                future.add_done_callback(functools.partial(update_image_status, img.id))
                # print(future.result())
            else:
                img.downloaded = True
                session.add(img)
                session.commit()
        
def download_file(img):
    """
    threading files download
    """
    url = img.url
    filename = urlparse(url).path.rsplit("/")[-1]
    directory = os.path.join(IMGPATH, img.source)
    if not os.path.exists(directory):
        os.makedirs(directory)
        
    fpath = os.path.join(directory, filename)
    
    response = requests.get(url, stream=True)#, proxies=PROXIES)
    status = response.status_code
    if status == 200:
        if not os.path.exists(fpath):
            with open(fpath, 'wb') as f:
                for chunk in response.iter_content(1024):
                    f.write(chunk)
            #img.downloaded = True
            #img.path = os.path.join(img.source, filename)
            # try:
            #sess.add(img)
            #sess.commit()
            # except IntegrityError:
            #session.rollback()
            #print(f"downloaded {url}")
            #session.close()
            return f"{status} {url}"
        # else:
        #    img.downloaded = True
        #    session.add(img)
        #    session.commit()
        #    return f"file exists {url}"
    else:
        #if status == 404:
        #    img.status = status
        #    sess.add(img)
        #    sess.commit()
        #    #session.close()
        #print(f'{status} returned for {url}')
        return f'{status} {url}'
    
def get_images_data(frompage=1, topage=300):
    proxy = 'http://{}'.format(PROXY)
    proxies = {'http': proxy, 'https': proxy}
    # pr = requests.get("http://whatismyip.host/my-ip-address-details", proxies=proxies)
    # html = BeautifulSoup(pr.content, features="html.parser")
    # print(html.find("p", class_="ipaddress").string)
    # for p in range(frompage, topage+1):
    for pg in range(frompage, topage+1):
        print("Parsing page {}".format(pg))
        r = requests.get("https://www.pexels.com/new-photos?page={}".format(pg), proxies=proxies)
        c = r.content
        soup = BeautifulSoup(c, features="html.parser")    
        links = soup.find_all("a", class_="js-photo-link photo-item__link")
        # print("TOTAL IMAGES, {}".format(len(links)))
        for ln in links:
            # print("_"*80)

            href = ln.get('href')
            imglink = "https://www.pexels.com" + href
            pg = requests.get(imglink)
            imgcontent = pg.content
            sp = BeautifulSoup(imgcontent, features="html.parser")
            image = sp.find("img", class_="js-photo-page-image-img")
            img_src = image.get("src")
            url = urlparse(img_src)
            url = "https://images.pexels.com" + url.path
            #print("SOURCE", img_src, url)
            tags = [t.string for t in sp.find_all("a", {'data-track-label': 'tag'})]
            #print("TAGS", tags)
            imgtags = []
            for tag in tags:
                tag = tag.strip().lower()
                prevtag = session.query(Tag).filter(Tag.txt == tag).first()
                if not prevtag:
                    newtag = Tag(txt=tag)
                    session.add(newtag)
                    session.commit()
                    imgtags.append(newtag)
                else:
                    imgtags.append(prevtag)
                    
            previmg = session.query(Image).filter(Image.url == img_src).first()
            
            if not previmg:
                # print("SAVing image data")
                newimage = Image(url=img_src, source='pexels')
                #session.add(newimage)
                #session.commit()
                newimage.tags = imgtags
                session.add(newimage)
                try:
                    session.commit()
                except IntegrityError:
                    session.rollback()
        
    # find all links like class=js-photo-link photo-item__link
    # visit link to grab image and tags: attribute data-track-label, value=tag, text -> tag
    
    
def get_ip():
    browser = setup_profile()
    browser.get("http://myip.ru")
    print(browser.find_element_by_xpath('//body').text)
    
def get_images_urls(startpage=1):
    
    browser = setup_profile()

    for page in range(startpage, startpage+500):
        
        browser.get(URL.format(page))
        #all_cookies = browser.get_cookies()
        ## print(all_cookies)
        ## browser.execute_script("window.scrollTo(0, 1080)")
        ## browser.implicitly_wait(2)
        print('parsing page {}'.format(page))
        media = browser.find_element_by_xpath('//div[@class="media_list"]')
        for d in media.find_elements_by_xpath('//div[@class="item"]'):
            l =  d.find_elements_by_tag_name('a')[0]
            # print(l.get_attribute("href"))
            # print(l.get_attribute('innerHTML'))
            for img in l.find_elements_by_tag_name('img'):
                imgtags = []
                tags = img.get_attribute("alt").split(", ")
                # print('TAGS', tags)
                for tag in tags:
                    tagtxt = tag.strip().lower()
                    existing = session.query(Tag).filter(Tag.txt == tagtxt).first()
                    if not existing:
                        newtag = Tag(txt=tag.strip().lower())
                        session.add(newtag)
                        session.commit()
                        imgtags.append(newtag)
                        
                    else:
                        imgtags.append(existing)
                        
                imgsrc = img.get_attribute("src")
                if imgsrc.endswith('blank.gif'):
                    # not loaded
                    attr = 'data-lazy'
                else:
                    # loaded
                    attr = 'src'

                # print('ATTR', attr)
                src = img.get_attribute(attr).replace('__340.jpg', '_960_720.jpg')

                # DB INSERT
                existing = session.query(Image).filter(Image.url == src).first()
                if not existing:
                    newimage = Image(url=src, source='pixabay')
                    session.add(newimage)
                    session.commit()
                    for tg in imgtags:
                        #if tg not in newimage.tags:
                        newimage.tags.append(tg)
                        
                        session.add(newimage)
                        try:
                            session.commit()
                        except IntegrityError:
                            session.rollback()
        print("going to the next page")
        #browser.close()
    

if __name__ == '__main__':
    # USAGE:
    # ./script.py geturls pagefrom[int] pageto[int]
    # ./script.py getfiles numthreads[int] numruns[int] source[txt]

    if sys.argv[1] == 'geturls':
        get_images_data(frompage=int(sys.argv[2]), topage=int(sys.argv[3]))

    elif sys.argv[1] == 'getfiles':
        nthreads = int(sys.argv[2])
        numruns = int(sys.argv[3])
        try:
            source = sys.argv[4]
        except:
            source = None
        for i in range(0, numruns):
            getfiles(numthreads=nthreads, source=source)
            
    elif sys.argv[1] == "getdimensions":

        picts = session.query(Image).filter(Image.downloaded == True).filter(Image.orientation == None).filter(Image.path != None)
        for p in picts:
            print(p.url)
            path = os.path.join(mediadir, p.path)
            img = Img.open(path)
            width, height = img.size
            orientation = "l"
            if height >= width:
                orientation = 'p'
            print(width, height, orientation)
            p.width = width
            p.height = height
            p.orientation = orientation
            session.add(p)
            session.commit()
    #get_ip()
    #page = int(sys.argv[1])
    #get_images_urls(startpage=page)
