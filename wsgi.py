from werkzeug.contrib.profiler import ProfilerMiddleware
from api import app

# gunicorn --bind localhost:5468 wsgi:app
if __name__ == "__main__":
    app.config['PROFILE'] = True
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions = [100])
    app.run(debug = True, host="localhost", port=8999)
    
