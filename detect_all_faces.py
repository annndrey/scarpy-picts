#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import json

import aiohttp
import asyncio
import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Image, ImageVersion, Tag, OrientationType, Location, User
from api import face_detect


def load_config():
    config_file = os.environ['APPSETTINGS']
    exec(open(config_file).read()) 
    return locals()
    
def create_session(dbconfig):
    engine = create_engine(dbconfig)
    Session = sessionmaker(bind=engine)
    session = Session()
    return session

async def async_face_detect(session, url, fname):
    files = {'imagefile': open(fname[1],'rb')}
    faces = []
    async with session.post(url, data=files, timeout=0) as response:
        if response.status == 200:
            logging.debug(["FACE DETECT API RESPONSE"])
            resp = await response.text()
            resp_json = json.loads(resp)
            faces = resp_json.get('faces_locations')
            logging.debug(["FACE DETECT API RESPONSE", faces])
        else:
            logging.debug(["NO RESPONSE FROM FACE DETECT API", response.status])
            
        return {fname[0]: faces}
        
async def fetch_all(fpaths, loop, url):
    async with aiohttp.ClientSession(loop=loop) as session:
        results = await asyncio.gather(*[async_face_detect(session, url, fpath) for fpath in fpaths], return_exceptions=True)
        return results

        
if __name__ == "__main__":
    config = load_config()
    dbconfig = config['SQLALCHEMY_DATABASE_URI']
    IMGPATH = config['IMGPATH']
    FACE_DETECT_API = config['FACE_DETECT_API']
    session = create_session(dbconfig)
    images = session.query(Image).all()
    loop = asyncio.get_event_loop()
    fpaths = []
    
    for ind, i in enumerate(images):
        if not i.faces_json:
            fpath = os.path.join(IMGPATH, i.path)
            fpaths.append([ind, fpath])
    responses = loop.run_until_complete(fetch_all(fpaths, loop, FACE_DETECT_API))
    resp_dict = {k:v for dic in responses for k,v in dic.items()}
    for img_index, faces_list in resp_dict.items():
        img = images[img_index]
        img.faces_json = json.dumps(faces_list)
        session.add(img)
        session.commit()
        print(faces_list)
    logging.debug("All {} images processed".format(len(images)))
    
    
